<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('gender')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->string('status')->default(0);
        });

        // Insert some stuff
        DB::table('admins')->insert(
        array(
        'name' => 'Admin',
        'email' => 'grocery@admin.com',
        'password' => Hash::make('grocery@admin.123'),
        'created_at' =>now(),
        'updated_at' => now(),
        'status' => 1
        )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
