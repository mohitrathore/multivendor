<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondcategory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parentid');
            $table->string('name')->unique();
            $table->string('child')->nullable();
            $table->string('slug');
            $table->timestamps();
            $table->string('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secondcategory');
    }
}
