<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description');
            $table->longText('disclaimer');
            $table->string('coverimage');
            $table->string('images');
            $table->string('type');
            $table->string('criteria');
            $table->integer('discount');
            $table->string('productcode');
            $table->integer('store_id');
            $table->string('owner_email');
            $table->integer('maincategory')->nullable();
            $table->integer('secondcategory')->nullable();
            $table->integer('thirdcategory')->nullable();
            $table->integer('quantity');
            $table->integer('stoke');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
