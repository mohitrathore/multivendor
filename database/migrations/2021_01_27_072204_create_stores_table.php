<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('store_name');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('agent')->nullable();
            $table->string('opening_time');
            $table->string('closing_time');
            $table->string('commission');
            $table->string('description');
            $table->string('cover_photo');
            $table->string('aadharcard');
            $table->string('udhyog_gst');
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
            $table->string('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
