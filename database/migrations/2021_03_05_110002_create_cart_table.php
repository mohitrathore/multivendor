<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('cart', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('productid');
            $table->string('user_email');
            $table->integer('price_varient_id');
            $table->integer('quantity');
            $table->integer('price');
            $table->integer('total_sum');
            $table->integer('total_saving');
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
