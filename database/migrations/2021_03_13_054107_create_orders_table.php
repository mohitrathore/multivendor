<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('mobile');
            $table->integer('amount');
            $table->string('discount')->nullable();
            $table->string('date_time');
            $table->longText('address');
            $table->string('landmark');
            $table->integer('store_id');
            $table->integer('store_status')->default(false);
            $table->string('order_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->string('driver_email')->nullable();
            $table->integer('driver_status')->nullable();
            $table->boolean('payment_status')->default(false);
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
