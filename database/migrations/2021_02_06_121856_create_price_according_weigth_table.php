<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceAccordingWeigthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_according_weight', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('productid');
            $table->string('weight');
            $table->string('unit');
            $table->integer('price');
            $table->integer('saleprice');
            $table->timestamps();
            $table->string('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_according_weight');
    }
}
