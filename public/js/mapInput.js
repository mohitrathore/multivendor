
function initialize() {
   var latlng = new google.maps.LatLng(28.5355161,77.39102649999995);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 13
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: true,
      anchorPoint: new google.maps.Point(0, -29)
   });
    var input = document.getElementById('searchInput');
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var geocoder = new google.maps.Geocoder();
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    var infowindow = new google.maps.InfoWindow();   
    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
  
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
       
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);          
    

    /* this method get city and state when user search any location START*/
       for (var i=0; i<place.address_components.length; i++) {
        for (var b=0;b<place.address_components[i].types.length;b++) {

        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
        if (place.address_components[i].types[b] == "administrative_area_level_2") {
        //this is the object you are looking for
        cityplace= place.address_components[i];
     
        }
        if (place.address_components[i].types[b] == "administrative_area_level_1") {
        //this is the object you are looking for
        stateplace= place.address_components[i];
       
        }
        }
        }

        bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng(),cityplace.long_name,stateplace.long_name);
        infowindow.setContent(place.formatted_address);
        infowindow.open(map, marker);

/* this method get  city and state when user search any location END */
       
    });

    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {  
                  
        //find country name
        for (var i=0; i<results[1].address_components.length; i++) {
        for (var b=0;b<results[1].address_components[i].types.length;b++) {

        //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
        if (results[1].address_components[i].types[b] == "administrative_area_level_2") {
        //this is the object you are looking for
        city= results[1].address_components[i];
       
        }
        if (results[1].address_components[i].types[b] == "administrative_area_level_1") {
        //this is the object you are looking for
        state= results[1].address_components[i];
       
        }
        }
        }
        bindDataToForm(results[1].formatted_address,marker.getPosition().lat(),marker.getPosition().lng(),city.long_name,state.long_name);
              infowindow.setContent(results[1].formatted_address);
              infowindow.open(map, marker);
          }
        }
        });
    });
}
function bindDataToForm(address,lat,lng,city,state){
   document.getElementById('searchInput').value = address;
   document.getElementById('lat').value = lat;
   document.getElementById('lng').value = lng;
   document.getElementById('city').value = city;
   document.getElementById('state').value = state;
}
google.maps.event.addDomListener(window, 'load', initialize);