<?php
 
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Auth::routes(['verify' => true]);

/* User Routes*/
Route::group([
'prefix'=>'/',
'middleware'=>['verified'],

], function(){
Route::get('shop/{main}/{mainid}',[App\Http\Controllers\Frontend\ShopController::class, 'main'])->name('shop.main');
Route::get('shop/{main}/{mainid}/{second}/{secondid}',[App\Http\Controllers\Frontend\ShopController::class,'second'])->name('shop.second');
Route::get('shop/{main}/{mainid}/{second}/{secondid}/{third}/{thirdid}',[App\Http\Controllers\Frontend\ShopController::class,'third'])->name('shop.third');
Route::get('shop/pr/{product}/{productid}',[App\Http\Controllers\Frontend\ShopController::class,'singleproduct'])->name('shop.singleproduct');
Route::post('cartadd',[App\Http\Controllers\Frontend\CartController::class,'cartadd'])->name('shop.cartadd');
Route::get('cart',[App\Http\Controllers\Frontend\CartController::class,'cart'])->name('shop.cart');
Route::post('cartupdate',[App\Http\Controllers\Frontend\CartController::class,'cartupdate'])->name('shop.cartupdate');
Route::get('cartdelete/{pid}/{vid}',[App\Http\Controllers\Frontend\CartController::class,'cartdelete'])->name('shop.cartdelete');
Route::post('/couponcheck',[App\Http\Controllers\Frontend\CartController::class, 'couponcheck'])->name('couponcodecheck');
Route::get('/profile',[App\Http\Controllers\Frontend\ProfileController::class, 'index'])->name('profile.view');
Route::post('/profileedit',[App\Http\Controllers\Frontend\ProfileController::class, 'edit'])->name('profile.edit');

Route::get('/address',[App\Http\Controllers\Frontend\AddressController::class, 'index'])->name('address.view');
Route::get('/addressview',[App\Http\Controllers\Frontend\AddressController::class, 'view'])->name('address.addview');
Route::post('/addressadd',[App\Http\Controllers\Frontend\AddressController::class, 'add'])->name('address.add');

Route::get('/addresseditview/{id}',[App\Http\Controllers\Frontend\AddressController::class, 'editview'])->name('address.editview');
Route::post('/addressedit',[App\Http\Controllers\Frontend\AddressController::class, 'edit'])->name('address.edit');
Route::get('/addressdelete/{id}',[App\Http\Controllers\Frontend\AddressController::class, 'delete'])->name('address.delete');
Route::post('/gotocheckoutfromcart',[App\Http\Controllers\Frontend\CartController::class, 'gotocheckoutfromcart'])->name('gotocheckoutfromcart');

Route::get('/checkout',[App\Http\Controllers\Frontend\CheckoutController::class, 'index'])->name('checkout');
Route::get('/deliverhere/{id}',[App\Http\Controllers\Frontend\CheckoutController::class, 'deliverhere'])->name('deliverhere');

Route::post('/razorpay',[App\Http\Controllers\Frontend\CheckoutController::class, 'razorpay'])->name('razorpay');

Route::get('/success',[App\Http\Controllers\Frontend\CheckoutController::class, 'success']);
Route::get('/error' ,[App\Http\Controllers\Frontend\CheckoutController::class, 'error']);


});
 

Route::get('',[App\Http\Controllers\Frontend\IndexController::class, 'index'])->name('index.frontend');
Route::post('locationupdate',[App\Http\Controllers\Frontend\IndexController::class, 'locationupdate'])->name('index.location.update');
/* User Routes*/


/* login Otp Routes*/
Route::post('sendOtp', 'Auth\LoginController@sendOtp')->name('sendotp'); //Otp Send to user mobile number

/* login Otp Routes*/

/* Admin login Routes*/
Route::get('2IhjBxwzd8/login',[App\Http\Controllers\admin\auth\LoginController::class, 'loginshow'])->name('admin.login');
Route::post('2IhjBxwzd8/login',[App\Http\Controllers\admin\auth\LoginController::class, 'login'])->name('admin.login.submit');
/* Admin login Routes*/

/* Store Routes start*/
Route::group([
'prefix'=>'store',
'middleware'=>['store','verified'],

], function(){
Route::get('/', 'Store\StoreController@index')->name('store.index');
Route::get('/orders', 'Store\StoreController@index')->name('store.order');

Route::get('/stores', 'Store\ListController@index')->name('store.list');
Route::get('/storeaddview', 'Store\ListController@view')->name('store.addpage');
Route::post('/storeadd', 'Store\ListController@add')->name('store.add');
Route::get('/store_edit/{id}',[App\Http\Controllers\Store\ListController::class, 'edit'])->name('store.editview');
Route::post('/storeupdate', 'Store\ListController@update')->name('store.edit');
Route::get('/activestore/{id}', 'Store\ListController@active')->name('store.active');
Route::get('/inactivestore/{id}', 'Store\ListController@inactive')->name('store.inactive');
Route::get('/deletestore/{id}', 'Store\ListController@deletestore')->name('store.delete');

Route::get('/products', 'Store\ProductController@index')->name('product.index');
Route::get('/editproductview/{id}', 'Store\ProductController@vieweditproduct')->name('store.producteditview');
Route::post('/updateproduct', 'Store\ProductController@updateproduct')->name('store.editproduct');
Route::get('/addproductview', 'Store\ProductController@view')->name('store.productview');
Route::post('/addproduct', 'Store\ProductController@add')->name('store.addproduct');

Route::post('/productsecondcategory','Store\ProductController@selectsecondcategory')->name('storeproductsecond');
Route::post('/productthirdcategory','Store\ProductController@selectthirdcategory')->name('storeproductthird');
Route::get('/storeactiveproduct/{id}','Store\ProductController@activeproduct')->name('store.activeproduct');
Route::get('/storeinactiveproduct/{id}','Store\ProductController@inactiveproduct')->name('store.inactiveproduct');
Route::get('/storedeleteproduct/{id}','Store\ProductController@deleteproduct')->name('store.deleteproduct');
Route::get('/storeprofile','Store\ProfileController@index')->name('store.profile');

Route::post('/editprofile/{id}','Store\ProfileController@edit')->name('store.profileedit');


});
/* Store Routes end*/

/* Agent Routes start*/
Route::group([
'prefix'=>'agent',
'middleware'=>['agent','verified'],

], function(){
Route::get('/', 'Agent\AgentController@index')->name('agent.index');
Route::get('/referredstore', 'Agent\ReferredController@index')->name('agent.referredstore');

Route::get('/agentprofile','Agent\ProfileController@index')->name('agent.profile');

Route::post('/editprofile/{id}','Agent\ProfileController@edit')->name('agent.profileedit');
Route::post('/verification','Agent\AgentController@verification')->name('agent.verification');


});
/* Agent Routes end*/

/* Driver Routes start*/
Route::group([
'prefix'=>'driver',
'middleware'=>['driver','verified'],

], function(){
	Route::get('/', 'Driver\DriverController@index')->name('driver.index');
	Route::get('/driverprofile','Driver\ProfileController@index')->name('driver.profile');

Route::post('/editprofile/{id}','Driver\ProfileController@edit')->name('driver.profileedit');



});
/* Driver Routes end*/


/*Admin Routes Start*/
Route::group([
'prefix'=>'2IhjBxwzd8',
'middleware'=>['auth:admin'],

], function(){
Route::get('/dashboard',[App\Http\Controllers\admin\HomeController::class, 'index'])->name('admin.dashboard');
Route::get('/users',[App\Http\Controllers\admin\user\UsersController::class, 'view'])->name('admin.users');
Route::post('/logout',[App\Http\Controllers\admin\auth\LoginController::class, 'logout'])->name('admin.logout');

Route::get('/location',[App\Http\Controllers\admin\Location::class, 'view'])->name('admin.location');
Route::get('/activelocation/{id}',[App\Http\Controllers\admin\Location::class, 'active'])->name('activelocation');
Route::get('/inactivelocation/{id}',[App\Http\Controllers\admin\Location::class, 'inactive'])->name('inactivelocation');

Route::get('/viewlocation',[App\Http\Controllers\admin\Location::class, 'viewlocation'])->name('viewlocation');
Route::get('/editlocation/{id}',[App\Http\Controllers\admin\Location::class, 'editlocation']);
Route::get('/deletelocation/{id}',[App\Http\Controllers\admin\Location::class, 'deletelocation']);
Route::post('/updatelocation',[App\Http\Controllers\admin\Location::class, 'locationupdate'])->name('updatelocation');

Route::get('/store',[App\Http\Controllers\admin\store\StoreController::class, 'view']);
Route::get('/viewowner',[App\Http\Controllers\admin\store\StoreController::class, 'list'])->name('storeowner.list');
Route::get('/owneractive/{id}',[App\Http\Controllers\admin\store\StoreController::class, 'owneractive'])->name('storeowner.active');
Route::get('/ownerinactive/{id}',[App\Http\Controllers\admin\store\StoreController::class, 'ownerinactive'])->name('storeowner.inactive');
Route::get('/owner',[App\Http\Controllers\admin\store\StoreController::class, 'viewowner'])->name('store.addowner');
Route::post('/addowner',[App\Http\Controllers\admin\store\StoreController::class, 'addowner'])->name('addowner');
Route::post('/addrestrictions',[App\Http\Controllers\admin\store\StoreController::class, 'addrestrictions'])->name('addrestrictions');
Route::post('/addstore',[App\Http\Controllers\admin\store\StoreController::class, 'add'])->name('addstore');
Route::get('/viewstore',[App\Http\Controllers\admin\store\StoreController::class, 'viewstore'])->name('viewstore');
Route::get('/editstore/{id}',[App\Http\Controllers\admin\store\StoreController::class, 'editstore']);
Route::get('/deletestore/{id}',[App\Http\Controllers\admin\store\StoreController::class, 'deletestore']);
Route::post('/updatestore',[App\Http\Controllers\admin\store\StoreController::class, 'storeupdate'])->name('updatestore');
Route::get('/active/{id}',[App\Http\Controllers\admin\store\StoreController::class, 'active']);
Route::get('/inactive/{id}',[App\Http\Controllers\admin\store\StoreController::class, 'inactive']);

Route::get('/driver',[App\Http\Controllers\admin\driver\DriverController::class, 'driver']);
Route::get('/viewdriver',[App\Http\Controllers\admin\driver\DriverController::class, 'viewdriver'])->name('viewdriver');
Route::post('/adddriver',[App\Http\Controllers\admin\driver\DriverController::class,'adddriver'])->name('adddriver');
Route::get('/inactivedriver/{id}',[App\Http\Controllers\admin\driver\DriverController::class, 'inactive']);
Route::get('/activedriver/{id}',[App\Http\Controllers\admin\driver\DriverController::class, 'active']);
Route::get('/editdriver/{id}',[App\Http\Controllers\admin\driver\DriverController::class, 'editdriver']);
Route::get('/deletedriver/{id}/{email}',[App\Http\Controllers\admin\driver\DriverController::class, 'deletedriver']);
Route::post('/updatedriver',[App\Http\Controllers\admin\driver\DriverController::class, 'driverupdate'])->name('updatedriver');

Route::get('/agent',[App\Http\Controllers\admin\agent\AgentController::class, 'agent']);
Route::post('/agentverification',[App\Http\Controllers\admin\agent\AgentController::class, 'agentverification'])->name('admin.agentverification');
Route::get('/viewagent',[App\Http\Controllers\admin\agent\AgentController::class, 'viewagent'])->name('viewagent');
Route::post('/addagent',[App\Http\Controllers\admin\agent\AgentController::class,'addagent'])->name('addagent');
Route::get('/inactiveagent/{id}/{email}',[App\Http\Controllers\admin\agent\AgentController::class, 'inactive']);
Route::get('/activeagent/{id}/{email}',[App\Http\Controllers\admin\agent\AgentController::class, 'active']);
Route::get('/editagent/{id}',[App\Http\Controllers\admin\agent\AgentController::class, 'editagent']);
Route::get('/deleteagent/{id}/{email}',[App\Http\Controllers\admin\agent\AgentController::class, 'deleteagent']);
Route::post('/updateagent',[App\Http\Controllers\admin\agent\AgentController::class, 'agentupdate'])->name('updateagent');

Route::get('/viewmaincategory',[App\Http\Controllers\admin\category\CategoryController::class, 'viewmaincategory'])->name('viewmaincategory');
Route::get('/maincategory',[App\Http\Controllers\admin\category\CategoryController::class, 'maincategory']);
Route::post('/addmaincategory',[App\Http\Controllers\admin\category\CategoryController::class, 'addmaincategory'])->name('maincategory');
Route::post('/updatemaincategory',[App\Http\Controllers\admin\category\CategoryController::class, 'maincategoryupdate'])->name('maincategoryupdate');
Route::get('/inactivemaincategory/{id}',[App\Http\Controllers\admin\category\CategoryController::class, 'inactivemaincategory']);
Route::post('/assignmaincategory',[App\Http\Controllers\admin\category\CategoryController::class, 'assignmaincategory'])->name('assignmaincategory');

Route::get('/activemaincategory/{id}',[App\Http\Controllers\admin\category\CategoryController::class, 'activemaincategory']);

Route::get('/viewsecondcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'viewsecondcategory'])->name('viewsecondcategory');
Route::get('/secondcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'secondcategory']);
Route::post('/addsecondcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'addsecondcategory'])->name('addsecondcategory');
Route::post('/updatesecondcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'updatesecondcategory'])->name('updatesecondcategory');
Route::get('/activesecondcategory/{id}',[App\Http\Controllers\admin\category\CategoryController::class, 'activesecondcategory']);
Route::get('/inactivesecondcategory/{id}',[App\Http\Controllers\admin\category\CategoryController::class, 'inactivesecondcategory']);
Route::post('/assignsecondcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'assignsecondcategory'])->name('assignsecondcategory');


Route::get('/viewthirdcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'viewthirdcategory'])->name('viewthirdcategory');
Route::get('/thirdcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'thirdcategory']);
Route::post('/addthirdcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'addthirdcategory'])->name('addthirdcategory');
Route::post('/updatethirdcategory',[App\Http\Controllers\admin\category\CategoryController::class, 'updatethirdcategory'])->name('updatethirdcategory');
Route::get('/activethird/{id}',[App\Http\Controllers\admin\category\CategoryController::class, 'activethird']);
Route::get('/inactivethird/{id}',[App\Http\Controllers\admin\category\CategoryController::class, 'inactivethird']);

Route::get('/addproduct',[App\Http\Controllers\admin\product\ProductController::class, 'view_addproduct']);
Route::get('/products',[App\Http\Controllers\admin\product\ProductController::class, 'viewproducts'])->name('admin.viewproducts');
Route::get('/products/{id}',[App\Http\Controllers\admin\product\ProductController::class, 'productdetails']);
Route::post('/addproductdetails',[App\Http\Controllers\admin\product\ProductController::class, 'addproduct'])->name('addproduct');
Route::get('/editproduct/{id}',[App\Http\Controllers\admin\product\ProductController::class, 'editproductview']);
Route::post('/updateproduct',[App\Http\Controllers\admin\product\ProductController::class, 'updateproduct'])->name('updateproduct');
Route::post('/restrictcategory',[App\Http\Controllers\admin\product\ProductController::class, 'selectmaincategory'])->name('productrestrictcategory');
Route::post('/selectsecondcategory',[App\Http\Controllers\admin\product\ProductController::class, 'selectsecondcategory'])->name('productsecond');
Route::post('/selectthirdcategory',[App\Http\Controllers\admin\product\ProductController::class, 'selectthirdcategory'])->name('productthird');

Route::get('/activeproduct/{id}',[App\Http\Controllers\admin\product\ProductController::class, 'activeproduct']);
Route::get('/inactiveproduct/{id}',[App\Http\Controllers\admin\product\ProductController::class, 'inactiveproduct']);
Route::get('/deleteproduct/{id}',[App\Http\Controllers\admin\product\ProductController::class, 'deleteproduct']);

Route::get('/banners',[App\Http\Controllers\admin\banner\BannerController::class, 'index'])->name('banners');
Route::get('/editbanner/{id}',[App\Http\Controllers\admin\banner\BannerController::class, 'vieweditbanner']);
Route::get('/addbanner',[App\Http\Controllers\admin\banner\BannerController::class, 'viewaddbanner']);
Route::post('/add',[App\Http\Controllers\admin\banner\BannerController::class, 'add'])->name('addbanner');
 
Route::post('/updatebanner',[App\Http\Controllers\admin\banner\BannerController::class, 'update'])->name('updatebanner');

Route::get('/activebanner/{id}',[App\Http\Controllers\admin\banner\BannerController::class, 'activebanner']);
Route::get('/inactivebanner/{id}',[App\Http\Controllers\admin\banner\BannerController::class, 'inactivebanner']);
Route::get('/deletebanner/{id}',[App\Http\Controllers\admin\banner\BannerController::class, 'deletebanner']);

Route::get('/couponlist',[App\Http\Controllers\admin\couponcode\CouponController::class, 'index'])->name('couponlist');
Route::get('/couponcode',[App\Http\Controllers\admin\couponcode\CouponController::class, 'addview'])->name('add.viewcouponcode');
Route::post('/addcouponcode',[App\Http\Controllers\admin\couponcode\CouponController::class, 'add'])->name('add.couponcode');

Route::get('/activecoupon/{id}',[App\Http\Controllers\admin\couponcode\CouponController::class, 'activecoupon']);
Route::get('/inactivecoupon/{id}',[App\Http\Controllers\admin\couponcode\CouponController::class, 'inactivecoupon']);
Route::get('/deletecoupon/{id}',[App\Http\Controllers\admin\couponcode\CouponController::class, 'deletecoupon']);



});
/*Admin Routes End*/


 