@extends('admin.layouts.app')
 
@section('content') 
        <!-- Content Wrapper. Contains page content -->
        @if(session()->has('success'))
      <script>
      $( document ).ready(function() {
      toastr.success("{!! session()->get('success')!!}")
      });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
      $( document ).ready(function() {
      toastr.error("{!! session()->get('error')!!}")
      });
      </script>
      @endif 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <form method="post" enctype="multipart/form-data" action="{{ route('updatestore')}}">
              @csrf
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3"> </div>
          <div class="col-sm-12">
          <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header text-white"
                   style="background: url('../../dist/img/photo1.png') center center;">
                <h3 class="widget-user-username text-center">

                  <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/dashboard" class="text-white">Home</a></li>
              <li class="breadcrumb-item active ">Edit Store</li>
            </ol></h3>
              
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="{{ asset('dist/img/store.jpg')}}" alt="User Avatar">
              </div>
              
            </div>
          </div>
          
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-2"> </div>
        <div class="col-md-8">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Store Details</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <input type="hidden" name="cid" value="{{$editstoredata->id}}">
              
              <div class="form-group">
                <label for="inputName">Store Name</label>
                <input   class="form-control" placeholder="Store Name" name="store_name" type="text" value="{{$editstoredata->name}}">
                <span style="color:orange"> @error('store_name') {{ $message }}@enderror </span>
              </div> 
              <div class="form-group">
                <label for="inputDescription">Store Description</label>
                <textarea   class="form-control ckeditor" cols="5" placeholder="Store Description" name="description" rows="5">{{$editstoredata->description}}</textarea>
               <span style="color:orange"> @error('description') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Store Commission % </label>
                <input   class="form-control" placeholder="Store commission %" name="commission" type="number" value="{{$editstoredata->commission}}">
                <span style="color:orange"> @error('commission') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Opening Time</label>
                <input   class="form-control" placeholder="Opening Time" name="opening_time" type="time" value="{{$editstoredata->opening_time}}">
                <span style="color:orange"> @error('opening_time') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Closing Time</label>
                <input   class="form-control  " placeholder="Closing Time" name="closing_time" type="time" value="{{$editstoredata->closing_time}}">
                <span style="color:orange"> @error('closing_time') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Agent ( optional )</label>
                <input class="form-control" placeholder="Agent Code" name="agent" type="text" value="{{$editstoredata->agent}}" maxlength="6">
              
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Store Address</label>
                <input type="text" class="form-control" id="searchInput" placeholder="Address" name="address" class="form-control" autocomplete="nope" value="{{$editstoredata->address}}"> 
                <input type="hidden" name="latitude" id="lat" value="{{$editstoredata->latitude}}"/>
                <input type="hidden" name="longitude" id="lng" value="{{$editstoredata->longitude}}"/>
                <input type="hidden" name="city" id="city" value="{{$editstoredata->city}}"/>
                <input type="hidden" name="state" id="state" value="{{$editstoredata->state}}"/>
                <div id="address-map-container"  style="display:none;">
                <div style="width: 100%; height: 100%"  class="map" id="map" ></div>

                </div>
                <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Cover Photo</label> <br />

               <input type="file" class="form-group" name="cover_photo" id="coverimage"> 

               <input type="hidden" class="form-group" name="oldcover_photo" value="{{$editstoredata->cover_photo}}"> 
               <img id="coverimageshow" src="/store_images/{{$editstoredata->cover_photo}}" style="width:15%;">
               <span style="color:orange"> @error('cover_photo') {{ $message }}@enderror </span>
              </div>
              <div class="col-12">
              <a href="/2IhjBxwzd8/viewstore" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Update Store" class="btn btn-success float-right">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-2"> </div>
      </div>
     
  </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
//load cover image only
  function readURL(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#coverimageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }

  $("#coverimage").change(function() {
  readURL(this);
  });


  </script>
@endsection
   @section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>
    <script>

    </script>
    <script src="/js/mapInput.js"></script>

@endsection
