@extends('admin.layouts.app')
 
@section('content')
		 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2"> 
            <div class="col-sm-3"> </div>
            <div class="col-sm-12">
            <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header text-white"
            style="background: url('../dist/img/photo1.png') center center;">
            <h3 class="widget-user-username text-center">
            @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  
            

            </h3>
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
            <li class="breadcrumb-item active ">View Store</li>
            </ol>
            </div>
            <div class="widget-user-image">
            <img class="img-circle" src="{{ asset('dist/img/store.jpg')}}" alt="User Avatar">
            </div>

            </div>
            </div>

            </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-3"> <h3 class="card-title">Stores</h3> </div>
            <div class="col-sm-7"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
            <div class="col-sm-1"> </div>
             <div class="col-sm-1">
            <div class="card-tools">

            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects table-responsive">
              <thead>
                 <thead>
                  <tr> 
                    <th style="min-width:190px;">Photos</th>
                    <th>Store Name</th>
                    <th>Store Address</th>
                    <th>Email</th>
                    <th>Opening Time</th>
                    <th>Closing Time</th>
                    <th>Commission % </th>
                    <th>City</th>
                    <th>State</th>
                    <th>Agent Code</th>
                    <th>Status </th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody id="myTable">
                  @forelse ($stores as $store)
                  <tr>
                    <td>
                      <ul class="list-inline">
                              <li class="list-inline-item">
                                  <a href="/store_images/{{$store->cover_photo}}" target="_blank" title="cover photo" >
                                   <i class="fa fa-file-image fa-2x" ></i>  </a>
                              </li>
                              <li class="list-inline-item">
                                  <a href="/store_images/verification/{{$store->aadharcard}}" target="_blank" title="Aadhar Card">
                                   <i class="fa fa-id-card fa-2x" ></i> 
                                  </a>
                              </li>
                              <li class="list-inline-item">
                                 <a href="/store_images/verification/{{$store->udhyog_gst}}" target="_blank" title="Udhyog / GST">
                                   <i class="fa fa-id-card fa-2x" ></i> 
                                </a>
                              </li>
                          </ul>

                    </td>
                    <td>{{ $store -> name }}<br/><small>Created {{date('d-m-Y H:i a', strtotime($store->created_at))}}</small></td>
                    <td>{{ $store -> address }}</td>
                    <td>{{ $store -> email }}</td>
                    <td>{{ $store -> opening_time }}</td>
                    <td>{{ $store -> closing_time }}</td>
                    <td>{{ $store -> commission }}</td>
                    <td>{{ $store -> city }}</td>
                    <td>{{ $store -> state }}</td>
                    <td>
                      @if($store->agent=="")
                         <span class="badge badge-danger">No Agent</span>
                      @else
                      {{$store->agent}}
                      @endif
                    </td>
                    <td>
                      @if( $store -> status == 1)
                      <a href="inactive/{{$store->id}}" > <span class="badge badge-success">Active</span> </a>
                     
                      @elseif( $store -> status == 0)
                      <a href="active/{{$store->id}}"><span class="badge badge-danger">Inactive</span></a>
                    
                       @elseif( $store -> status == 2)
                      <a href="active/{{$store->id}}"><span class="badge badge-warning"  onclick="return confirm('Are you sure? You check all the verification Ids to activate - {{$store->name}} - store');">Pending</span></a>
                      @else
                      @endif
                    </td>
                    <td>
                      <a href="editstore/{{$store->id}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fa fa-edit"></i></a>
                      <a href="deletestore/{{$store->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this store?');"><i class="fa fa-trash"></i>

                      </a>

                    </td>
                  </tr>
                  @empty
                  <tr><td colspan="14"><h4 class="text-center"> No record found</h4></td></tr>
                 
                  @endforelse
                 
              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $stores->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
