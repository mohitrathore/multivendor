@extends('admin.layouts.app')
 
@section('content') 
			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <form method="post" enctype="multipart/form-data" action="{{ route('addstore')}}">
             	@csrf
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			@if(session()->has('success'))
      <script>
      $( document ).ready(function() {
      toastr.success("{!! session()->get('success')!!}")
      });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
      $( document ).ready(function() {
      toastr.error("{!! session()->get('error')!!}")
      });
      </script>
      @endif  

			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Store</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/store.jpg')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
           <div class="col-md-2"> </div>
        <div class="col-md-8">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Store Details</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
               
              <div class="form-group">
                <label for="inputStatus">Store Owner Email <small>choose preffered email to allocate a store</small></label>
                <select class="form-control custom-select" name="email">
                    <option value="">-- Select Owner Email --</option>
                    @forelse ($owners as $owner)
                <option value="{{$owner->email}}"  
                  @if(old('email')== $owner->email) selected="selected" @endif >
                  {{ $owner->name }} ( {{$owner->email}} ) </option>

                @empty
                  <option >No Store Owner Emails Registerd Yet</option>
                  @endforelse
                </select>
              <span style="color:orange"> @error('email') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputName">Store Name</label>
                <input   class="form-control" placeholder="Store Name" name="store_name" type="text" value="{{old('store_name')}}">
                <span style="color:orange"> @error('store_name') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Store Description</label>
                <textarea   class="form-control ckeditor" cols="5" placeholder="Store Description" name="description" rows="5">{{old('description')}}</textarea>
               <span style="color:orange"> @error('description') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Store Commission % </label>
                <input   class="form-control" placeholder="Store commission %" name="commission" type="number" value="{{old('commission')}}">
                <span style="color:orange"> @error('commission') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Opening Time</label>
                <input   class="form-control" placeholder="Opening Time" name="opening_time" type="time" value="{{old('opening_time')}}">
                <span style="color:orange"> @error('opening_time') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Closing Time</label>
                <input   class="form-control  " placeholder="Closing Time" name="closing_time" type="time" value="{{old('closing_time')}}">
                <span style="color:orange"> @error('closing_time') {{ $message }}@enderror </span>
              </div>
               <div class="form-group">
                <label for="inputStatus">Agent(optional) <small>   Choose agent to allocate a store</small></label>
                <select class="form-control custom-select" name="agent">
                    <option value="">-- Select Agent --</option>
                    @forelse ($agentdetail as $agents)
               <option value="{{$agents->agentcode}}"  @if ( old("agent") == $agents->agentcode) selected="selected" @endif>{{ $agents->name }} ( {{$agents->email}} )</option>
                @empty
                  <option value="0">No Agent Registerd Yet</option>
                  @endforelse
                </select>

             
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Store Address</label>
                <input type="text" class="form-control" id="searchInput" placeholder="Address" name="address" class="form-control" autocomplete="nope"> 
                <input type="hidden" name="latitude" id="lat"/>
                <input type="hidden" name="longitude" id="lng"/>
                <input type="hidden" name="city" id="city"/>
                <input type="hidden" name="state" id="state"/>
                <div id="address-map-container"  style="display:none;">
                <div style="width: 100%; height: 100%"  class="map" id="map" ></div>

                </div>
                <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Cover Photo <small>   Jpg, Jpeg, Png allowed only*</small></label>
                  <br />
                <input type="file" name="cover_photo"> 
                 <span style="color:orange"> @error('cover_photo') {{ $message }}@enderror </span>
              </div>
               <div class="form-group">
                <label for="inputProjectLeader">Aadhaar Card  <small>   Jpg, Jpeg, Png allowed only* </small></label>
                <br />
                <input type="file" name="aadharcard"> 
                 <span style="color:orange"> @error('aadharcard') {{ $message }}@enderror </span>
              </div>
              <hr>
              <div class="form-group">
                <label for="inputProjectLeader">Udhyog Aadhar Card / GST Certificate<small>   Jpg, Jpeg, Png allowed only *</small></label>
                <br />
                <input type="file" name="udhyog_gst"> 
                 <span style="color:orange"> @error('udhyog_gst') {{ $message }}@enderror </span>
              </div>
              <div class="col-12">
              <a href="/viewstore" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Create new Store" class="btn btn-success float-right">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-2"> </div>
      </div>
    
  </form>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
@endsection
   @section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>
    <script>

    </script>
    <script src="/js/mapInput.js"></script>

@endsection