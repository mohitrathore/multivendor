@extends('admin.layouts.app')
 
@section('content')
		 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2"> 
            <div class="col-sm-3"> </div>
            <div class="col-sm-12">
            <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header text-white"
            style="background: url('../dist/img/photo1.png') center center;">
            <h3 class="widget-user-username text-center">
            @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  
            <!-- Open modal when error validation occur -->
                        @if($errors->has('restrict_cat')) 
                        <script>
                        $( document ).ready(function() {
                        $("#restrictions{{old('restrict_cat')}}").modal("show");
                        });
                        </script>
                        @endif
                        <!-- Open modal when error validation occur -->
            </h3>
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
            <li class="breadcrumb-item active ">View Store</li>
            </ol>
            </div>
            <div class="widget-user-image">
            <img class="img-circle" src="{{ asset('dist/img/store.jpg')}}" alt="User Avatar">
            </div>

            </div>
            </div>

            </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-5">
             <h3 class="card-title"><b>Store Owner</b></h3>
             
            <a href="{{route('store.addowner')}}"><button type="button" class="btn btn-dark float-right"> Add New Store Owner</button></a>
             </div>
            <div class="col-sm-5"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
             <div class="col-sm-2">
            <div class="card-tools">

              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0 table-responsive">
          <table class="table table-striped projects ">
              <thead>
                 <thead>
                  <tr> 
                    <th style="width:30%;">Name</th>
                    <th style="width:30%;">Email</th>
                    <th style="width:20%;">Mobile</th>
                    <th style="width:20%;">Status / Action </th>
                  </tr>
                  </thead>
                  <tbody id="myTable">
                  @forelse ($owners as $owner)
                  <div class="modal fade" id="restrictions{{$owner->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Restrictions For  <b class="text-capitalize">{{$owner->name}}</b> </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form method="post" action="{{ route('addrestrictions') }}" >
            <div class="modal-body">

            @csrf
             <div class="form-group">
                   <label for="inputStatus">Category Restrictions &nbsp;&nbsp; <small> <b>You can choose Multiple Restrictions using </b>&nbsp;<kbd>Ctrl + Mouse Left Click</kbd></small></label>
               <select class="form-control custom-select text-capitalize" name="restrictions[]" style="height: 200px !important;" multiple>
                <option value="">-- Select Categories --</option>
                @foreach($restrictions as $restrict)
                <option value="{{$restrict->id}}" @if (old("restrictions") == $restrict->id ) selected="selected" @endif>{{$restrict->name}}</option>
               
                @endforeach
                </select>
                 <span style="color:orange"> @error('restrictions') {{ $message }}@enderror </span>
                <input type="hidden" name="email" value="{{$owner->email}}" />
              </div>
            </div>
            <div class="modal-footer">
            <a href=""><button type="button" class="btn btn-secondary">Cancel</button></a>
            <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
            </div>
            </div>
            </div>
            <!-- Modal Edit end -->
                  <tr>
                    
                    <td>{{ $owner -> name }}<br/><small>Created {{date('d-m-Y H:i a', strtotime($owner->created_at))}}</small></td>
                    <td>{{ $owner -> email }}</td>
                    <td>{{ $owner -> mobile }}</td>
                    <td>
                      @if( $owner -> status == 1)
                      <a href="ownerinactive/{{$owner->id}}" > <span class="badge badge-success">Active</span> </a>
                      
                      @elseif( $owner -> status == 0)
                      <a href="owneractive/{{$owner->id}}"><span class="badge badge-danger">Inactive</span></a>
                      @else
                      @endif
                      <a href="#" class="btn btn-success btn-xs text-white" data-toggle="modal" data-target="#restrictions{{$owner->id}}">Restrict a Categories</a>

                    </td>
                   
                  </tr>
                  @empty
                  <tr><td colspan="5"><h4 class="text-center"> No record found</h4></td></tr>
                 
                  @endforelse
                 
              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $owners->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
