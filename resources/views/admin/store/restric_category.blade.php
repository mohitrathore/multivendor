@extends('admin.layouts.app')
 
@section('content') 
			  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <form method="post" enctype="multipart/form-data" action="{{ route('addowner')}}">
             	@csrf
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			@if(session()->has('success'))
      <script>
      $( document ).ready(function() {
      toastr.success("{!! session()->get('success')!!}")
      });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
      $( document ).ready(function() {
      toastr.error("{!! session()->get('error')!!}")
      });
      </script>
      @endif  

			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Store Owner</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/store.jpg')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="card card-secondary"> 
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Add Owner Details</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
                 <div class="form-group">
                   <label for="inputStatus">Category Restrictions &nbsp;&nbsp; <small> <b>You can choose Multiple Restrictions using </b>&nbsp;<kbd>Ctrl + Mouse Left Click</kbd></small></label>
               <select class="form-control custom-select text-capitalize" name="restrictions[]" style="height: 200px !important;" multiple>
                <option value="">-- Select Categories --</option>
                @foreach($restrictions as $restrict)
                <option value="{{$restrict->id}}" @if (old("restrictions") == $restrict->id ) selected="selected" @endif>{{$restrict->name}}</option>
               
                @endforeach
                </select>
                <span style="color:orange"> @error('gender') {{ $message }}@enderror </span>
              </div>
              <div class="col-12">
                <a href="{{route('storeowner.list')}}" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Create New Owner" class="btn btn-success float-right">
              </div>
      
      
            </div>

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
         <div class="col-md-2"></div>

      </div>
      
  </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
   