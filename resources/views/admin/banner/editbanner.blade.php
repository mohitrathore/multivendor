@extends('admin.layouts.app')
  
@section('content') 
			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <form method="post" enctype="multipart/form-data" action="{{route('updatebanner')}}">
      <input type="hidden" name="cid" value="{{$editbanner->id}}">
             	@csrf
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			 @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
    
      @endif 

			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Banners</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/banner.png')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Add Banner</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> 
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputEstimatedBudget">Name</label>
                <input   class="form-control" placeholder="Banner Name" name="name" type="text" value="{{$editbanner->name}}">
                <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Image</label>
                <input   class="form-control" placeholder="Image" id="image" name="image" type="file" value="">
                 <input type="hidden" class="form-group" name="oldimage" value="{{$editbanner->image}}"> 
               <img id="imageshow" src="/dist/img/ad_banner/{{$editbanner->image}}" style="width:50%;">
                <br />
                 <span style="color:orange"> @error('image') {{ $message }}@enderror </span>
                <span>
                </div>
              <div class="form-group">
                <label for="inputStatus">Type</label>
                <select class="form-control custom-select" name="type">
                <option value="">-- Select Banner Position --</option>
                <option value="top" @if ($editbanner->type == "top") selected="selected" @endif>Top</option>
                <option value="mid" @if ($editbanner->type == 'mid') selected="selected" @endif>Middle</option>
                <option value="bottom" @if ($editbanner->type == 'bottom') selected="selected" @endif>Bottom</option>
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
              
               <div class="col-12">
          <a href="/banners" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Update Banner" class="btn btn-success float-right">
        </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
          <div class="col-md-2"></div>
      </div>
      
  </form>
    </section>
    <!-- /.content -->
    <script>
    //load cover image only
  function readURL(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#imageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }

  $("#image").change(function() {
  readURL(this);
  });
    </script>
     
  <!-- /.content-wrapper -->
@endsection
