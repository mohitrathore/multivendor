@extends('admin.layouts.app')
 
@section('content')
		 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2"> 
            <div class="col-sm-3"> </div>
            <div class="col-sm-12">
            <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header text-white"
            style="background: url('../dist/img/photo1.png') center center;">
            <h3 class="widget-user-username text-center">
            @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

            </h3>
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
            <li class="breadcrumb-item active ">View Users</li>
            </ol>
            </div>
            <div class="widget-user-image">
            <img class="img-circle" src="{{ asset('dist/img/store.jpg')}}" alt="User Avatar">
            </div>

            </div>
            </div>

            </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-3"> <h3 class="card-title">Users</h3> </div>
            <div class="col-sm-7"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
            <div class="col-sm-1"> </div>
             <div class="col-sm-1">
            <div class="card-tools">

            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects ">
              <thead>
                 <thead>
                  <tr> 
                    <th >Name</th>
                    <th >Email</th>
                    <th >Gender</th>
                    <!-- <th style="width:10%;">Status</th>
                    <th style="width:10%;">Action</th> -->
                  </tr>
                  </thead>
                  <tbody id="myTable">

                  @forelse ($users as $user)
                  <tr>
                    <td class="text-capitalize">{{ $user -> name }}<br/><small>Created at 
                      {{date('d-m-Y H:i A', strtotime($user->created_at))}}</small></td>
                    <td>{{ $user -> email }}</td>
                    <td class="text-capitalize">{{ $user -> gender}}</td>
                   <!--  <td>
                      @if( $user -> status == 1)
                      <a href="inactive/{{$user->id}}" > <span class="badge badge-success">Active</span> </a>
                      @elseif( $user -> status == 0)
                      <a href="active/{{$user->id}}"><span class="badge badge-danger">Inactive</span></a>
                      @else
                      @endif
                    </td>
                    <td>
                      <a href="deleteuser/{{$user->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash"></i>
                      </a>

                    </td> -->
                  </tr>
                  @empty
                  <tr><td colspan="5"><h4 class="text-center"> No record found</h4></td></tr>
                 
                  @endforelse
                 
              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $users->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
