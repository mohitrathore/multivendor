@extends('admin.layouts.app')
  
@section('content')
     <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
      <section class="content-header">
   
      <div class="container-fluid">
      <div class="row mb-2">
      <div class="col-sm-3"> </div>
      <div class="col-sm-12">
      <div class="card card-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header text-white"
      style="background: url('../dist/img/photo1.png') center center;">
      <h3 class="widget-user-username text-center">
       @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
      @endif 

      </h3>
      <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
      <li class="breadcrumb-item active ">View Agent</li>
      </ol>
      </div>
      <div class="widget-user-image">
      <img class="img-circle" src="{{ asset('dist/img/agent.jpg')}}" alt="User Avatar">
      </div>

      </div>
      </div>

      </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
           <div class="card-header">
          <div class="row">
            <div class="col-sm-3"> <h3 class="card-title"><b>Agents</b></h3> </div>
            <div class="col-sm-7"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
            <div class="col-sm-1"> </div>
             <div class="col-sm-1">
            <div class="card-tools">

            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped  table-responsive">
              <thead>
                 <thead>
                  <tr> 
                    <th style="width:20%;">Verification</th>
                    <th style="width:20%;">Name</th>
                    <th style="width:20%;">Email</th>
                    <th style="width:5%;">Gender</th>
                    <th style="width:20%;">Mobile</th>
                    <th style="width:10%;">Status </th>
                    <th style="width:10%;">Action</th>
                  </tr>
                  </thead>
                  <tbody id="myTable">
                 
                  @forelse ($agentusers as $agentuser)
                  
                  <tr>
                    <td>
@foreach($agents as $agent)
<!-- Agent Verification details -->
<div class="modal fade" id="agentverification{{$agent->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Agent Verification Details for <b class="text-capitalize">{{$agent->name}}</b>  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <div class="form-group">
            @if($agent->status == 1)
          <span class="badge badge-success float-right"> Verified</span>
          @elseif($agent->status == 0)
          <span class="badge badge-danger float-right">Not Verified Yet !</span>
          @elseif($agent->status == 2)
          <span class="badge badge-warning float-right">Pending !</span>

          @else
          @endif
        </div>
        <div class="form-group">

      <label for="uname">Username:</label>
      <br>
      {{$agent->email}}
      <br />
    </div>
     <div class="form-group">
      <label for="uname">Address :</label>
      <textarea class="form-control" rows="5" readonly>{{$agent->address}}</textarea>
    </div> 
    <div class="form-group">
      <label for="uname">Aadhar Card:</label>
      <br />
      <img src="../store_images/verification/{{$agent->aadharcard}}" style="width:100%;">
    </div> 
    <div class="form-group">
      <label for="uname">Agentcode:</label>
      <input type="text" class="form-control" value="{{$agent->agentcode}}" required readonly>
      
    </div>
    <form method="post" action="{{route('admin.agentverification')}}"> 
      @csrf
     <div class="form-group">
                <label for="inputStatus">Verify :</label>
                <select class="form-control custom-select" name="verify">
               
                <option value="1" @if (old("verify") == 1) selected="selected" @endif>Yes, Verify this Agent</option>
                <option value="0" @if (old('verify') == 0) selected="selected" @endif>No, Reject this Agent</option>
                
                </select>
                <span style="color:orange"> @error('verify') {{ $message }}@enderror </span>
              </div>
               <input type="hidden" name="cid" value="{{$agent->id}}">
    </div>
    <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div>
</div>
<!-- Agent Verification details -->

@if($agent->status == 1)
 <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#agentverification{{$agent->id}}">Verification Done</button>
@elseif($agent->status == 0)
 <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#agentverification{{$agent->id}}">Rejected</button>
 @elseif($agent->status == 2)
 <button type="button" class="btn btn-warning btn-sm text-white" data-toggle="modal" data-target="#agentverification{{$agent->id}}">Pending</button>
@endif

 @endforeach
                    </td>
                    <td>{{ $agentuser -> name }} 
                      &nbsp;
                      <a href="/profile_images/{{$agentuser->profileImage}}" target="_blank"><i class="fa fa-id-card fa-2x" ></i>
                       </a>
                      <br/><small>Created at {{date('d-m-Y H:i a', strtotime($agentuser->created_at))}}</small></td>
                    <td>{{ $agentuser -> email }}</td>
                    <td>{{ $agentuser -> gender }}</td>
                    <td>{{ $agentuser -> mobile }}</td>
                    <td>
                      @if( $agentuser -> status == 1)
                      <a href="inactiveagent/{{$agentuser->id}}/{{$agentuser->email }}" > <span class="badge badge-success">Active</span> </a>
                      @elseif( $agentuser -> status == 0)
                      <a href="activeagent/{{$agentuser->id}}/{{$agentuser->email }}"><span class="badge badge-danger">Inactive</span></a>
                      @else
                      @endif
                    </td>
                    <td>
                      <a href="editagent/{{$agentuser->id}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fa fa-edit"></i></a>
                      <a href="deleteagent/{{$agentuser->id}}/{{$agentuser->email}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this Agent?');"><i class="fa fa-trash"></i></a>

                    </td>
                  </tr>
                  @empty
                  <tr><td colspan="8"><h4 class="text-center"> No Agent Found</h4></td></tr>
                  @endforelse
                 
              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $agents->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
