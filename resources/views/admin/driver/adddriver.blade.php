@extends('admin.layouts.app')
 
@section('content') 
			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <form method="post" enctype="multipart/form-data" action="{{route('adddriver')}}">
             	@csrf
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			 @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
      @endif 

			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Driver</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/bike.png')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Add Driver</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputEstimatedBudget">Full Name</label>
                <input   class="form-control" placeholder="Name" name="fullname" type="text" value="{{old('fullname')}}">
                <span style="color:orange"> @error('fullname') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputSpentBudget">Email</label>
               <input   class="form-control" placeholder="Email" name="email" type="email" value="{{old('email')}}">
                <span style="color:orange"> @error('email') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Password</label>
              <input   class="form-control  " placeholder="Password" name="password" type="password">
              <span style="color:orange"> @error('password') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Number</label>
                <input   class="form-control" placeholder="Contact No" name="number" type="number" value="{{old('number')}}">
                <span style="color:orange"> @error('number') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedDuration">Vehicle Number <small class="text-info"> &nbsp;Please avoid space between vehicle number</small></label>
                <input   class="form-control text-uppercase" placeholder="Vehicle Number" name="vehiclenumber" type="text" value="{{old('vehiclenumber')}}">
                <span style="color:orange"> @error('vehiclenumber') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputStatus">Gender</label>
                <select class="form-control custom-select" name="gender">
                <option value="">-- Select Gender --</option>
                <option value="male" @if (old("gender") == "male") selected="selected" @endif>Male</option>
                <option value="female" @if (old('gender') == 'female') selected="selected" @endif>Female</option>
                <option value="other" @if (old('gender') == 'other') selected="selected" @endif>Other</option>
                </select>
                <span style="color:orange"> @error('gender') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputClientCompany">Address</label>
                <input type="text" class="form-control" placeholder="Address" name="address"  autocomplete="nope" value="{{old('address')}}"> 
                <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">Driving Licence  <small class="text-info">   Jpg, Jpeg, Png allowed only* </small></label>
                <br />
                <input type="file" name="driving_license"> 
                 <span style="color:orange"> @error('driving_license') {{ $message }}@enderror </span>
              </div>
              <hr>
              <div class="form-group">
                <label for="inputProjectLeader">Registration Certificate copy (RC)<small class="text-info">   Jpg, Jpeg, Png allowed only *</small></label><br />
                <input type="file" name="vehicle_rc"> 
                 <span style="color:orange"> @error('vehicle_rc') {{ $message }}@enderror </span>
              </div>
               <div class="col-12">
          <a href="/viewdriver" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Create Driver" class="btn btn-success float-right">
        </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
          <div class="col-md-2"></div>
      </div>
      
  </form>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
@endsection
