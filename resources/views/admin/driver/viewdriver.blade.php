@extends('admin.layouts.app')
 
@section('content')
     <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
      <section class="content-header">
   
      <div class="container-fluid">
      <div class="row mb-2">
      <div class="col-sm-3"> </div>
      <div class="col-sm-12">
      <div class="card card-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header text-white"
      style="background: url('../dist/img/photo1.png') center center;">
      <h3 class="widget-user-username text-center">
       @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
      @endif 

      </h3>
      <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
      <li class="breadcrumb-item active ">View Driver</li>
      </ol>
      </div>
      <div class="widget-user-image">
      <img class="img-circle" src="{{ asset('dist/img/bike.png')}}" alt="User Avatar">
      </div>

      </div>
      </div>

      </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
           <div class="card-header">
          <div class="row">
            <div class="col-sm-3"> <h3 class="card-title"><b>Drivers</b></h3> </div>
            <div class="col-sm-7"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
            <div class="col-sm-1"> </div>
             <div class="col-sm-1">
            <div class="card-tools">

            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped  table-responsive">
              <thead>
                 <thead>
                  <tr> 
                    <th style="width:20%;">Photos</th>
                    <th style="width:20%;">Name</th>
                    <th style="width:15%;">Mobile No.</th>
                    <th style="width:5%;">Vehicle Number</th>
                    <th style="width:20%;">Email</th>
                    <th style="width:20%;">Address</th>
                    <th style="width:10%;">Status </th>
                    <th style="width:10%;">Action</th>
                  </tr>
                  </thead>
                  <tbody id="myTable">
                  @forelse ($drivers as $driver)
                  <tr>
                    <td>
                      <ul class="list-inline">
                              <li class="list-inline-item">
                                  <a href="/store_images/verification/{{$driver->verification1}}" target="_blank">
                                    <i class="fa fa-id-card fa-2x" ></i> </a>
                              </li>
                              <li class="list-inline-item">
                                  <a href="/store_images/verification/{{$driver->verification2}}" target="_blank"><i class="fa fa-id-card fa-2x" aria-hidden="true"></i>
                                  </a>
                              </li>
                             
                          </ul>

                    </td>
                    <td>{{ $driver -> name }}<br/><small>Created {{date('d-m-Y H:i a', strtotime($driver->created_at))}}</small></td>
                    <td>{{ $driver -> number }}</td>
                    <td>{{ $driver -> vehicle_number }}</td>
                    <td>{{ $driver -> email }}</td>
                    <td>{{ $driver -> address }}</td>
                    <td>
                      @if( $driver -> status == 1)
                      <a href="inactivedriver/{{$driver->id}}/{{$driver->email }}" > <span class="badge badge-success">Active</span> </a>
                      @elseif( $driver -> status == 0)
                      <a href="activedriver/{{$driver->id}}/{{$driver->email }}"><span class="badge badge-danger">Inactive</span></a>
                      @else
                      @endif
                    </td>
                    <td>
                      <a href="editdriver/{{$driver->id}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fa fa-edit"></i></a>
                      <a href="deletedriver/{{$driver->id}}/{{$driver->email}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this Driver?');"><i class="fa fa-trash"></i></a>

                    </td>
                  </tr>
                  @empty
                  <tr><td colspan="8"><h4 class="text-center"> No Driver Found</h4></td></tr>
                  @endforelse
                 
              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $drivers->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
