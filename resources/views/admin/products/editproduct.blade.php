@extends('admin.layouts.app')
 
@section('content') 
<style>
#wrapped+.input-group-addon {
    margin: 0;
    padding: 0;
    min-width: 8rem;
    border: none;
}
#wrapped+.input-group-addon>.form-control {
    border-radius: 0 .4rem .4rem 0;
    border-left: none;
}
img.thumb{
  width:20%;
}
</style>
			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header"> 
 
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			@if(session()->has('success'))
      <script>
      $( document ).ready(function() {
      toastr.success("{!! session()->get('success')!!}")
      });
      </script>
      @endif
     @if(session()->has('error'))
      <script>
      $( document ).ready(function() {
      toastr.error("{!! session()->get('error')!!}")
      });
      </script> 
      @endif  

       @if ($errors->any())
                    @foreach ($errors->all() as $error)

                    <script>
                    $( document ).ready(function() {
                  toastr.error("{{ $error }}")
                    });
                    </script>

                    @endforeach
        @endif   
        <!-- Product weight, price errors -->


			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Edit Product</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/product-main-icon.jpg')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
     <section class="content">
         <form method="post" enctype="multipart/form-data" id="forms" action="{{route('updateproduct')}}">
              @csrf
            <input type="hidden" name="cid" value="{{$productdetails->id}}">
       <div class="row">
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Edit Product </h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input type="text" id="inputName" name="name" class="form-control" value="{{ $productdetails->name }}">
                <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
              </div>
                <!-- Product code -->

              <div class="form-group">
                <label for="inputDescription">Product Description</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description" >{{ $productdetails->description}}</textarea>
                <span style="color:orange"> @error('description') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Disclaimer</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="disclaimer" >{{ $productdetails->disclaimer}}</textarea>
                <span style="color:orange"> @error('disclaimer') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Cover Image</label> <br />

               <input type="file" class="form-group" name="coverimagenew" id="coverimage"> 

               <input type="hidden" class="form-group" name="coverimage" value="{{$productdetails->coverimage}}"> 
               <img id="coverimageshow" src="/product_images/{{$productdetails->coverimage}}" style="width:15%;">
               <span style="color:orange"> @error('coverimagenew') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Images &nbsp;<small class="text-info"><b>multiple images allowed *</b></small></label> <br />

                <input type="hidden" name="realimages" value="{{$productdetails->images}}">

               <input type="file" class="form-group" name="images[]" type="file" id="file-input" onchange="loadPreview(this)"multiple>
                          <span class="text-danger">{{ $errors->first('images') }}</span>
                          <div id="thumb-output"> </div>
                          <div id="previousImg">
                             @foreach(json_decode($productdetails->images) as $images)
                               <img src="/product_images/{{$images}}" style="width:15%;">

                              @endforeach
                          </div>
                          <br>
               <span style="color:orange"> @error('images') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputStatus"> Product Stoke</label>
                <select class="form-control custom-select" name="stoke">
                  <option value="" selected disabled>Select one</option>
                  <option value="1" @if( old("stoke")== $productdetails->stoke || $productdetails->stoke == 1) selected="selected" @endif >In Stock</option>
                  <option value="0"  @if( old("stoke")== $productdetails->stoke || $productdetails->stoke == 0) selected="selected" @endif>Out of Stock</option>
                </select>
                <span style="color:orange"> @error('stoke') {{ $message }}@enderror </span>
              </div> 
              <div class="form-group">
                <label for="inputStatus"> Product Type</label>
                <select class="form-control custom-select" name="type">
                  <option value="" selected disabled>Select one </option>
                  <option value="veg"  @if( old("type")== $productdetails->type || $productdetails->type == "veg") selected="selected" @endif>Vegetarian</option>
                  <option value="non-veg" @if( old("type")== $productdetails->type || $productdetails->type == "non-veg") selected="selected" @endif>Non Vegetarian</option>
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>

              <div class="form-group">
                <label for="inputStatus"> Product Criteria</label>
                <select class="form-control custom-select" name="criteria">
                  <option value="" selected disabled>-- Select -- </option>
                  <option value="feature"  @if( old("criteria")== $productdetails->criteria || $productdetails->criteria == "feature") selected="selected" @endif>Featured</option>
                  <option value="top-saver"  @if( old("criteria")== $productdetails->criteria || $productdetails->criteria == "top-saver") selected="selected" @endif>Top Saver</option>
                  <option value="best-offer"  @if( old("criteria")==$productdetails->criteria || $productdetails->criteria == "best-offer") selected="selected" @endif>Best Offer</option>
                 
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
             
              <div class="form-group">
                <label for="inputClientCompany">Store</label>
                <select class="form-control custom-select text-capitalize" name="store_id">
                  <option value="">-- Select Store --</option>
                  @foreach ($stores as $store)
                  <option  value="{{$store->id}}" class="text-capitalize" @if( old("store_id")== $store->id || $store->id == $productdetails->store_id) selected="selected" @endif >
                  {{ $store->name}} ( {{$store->email}} )</option>

                  @endforeach
                </select>
                <span style="color:orange"> @error('store_id') {{ $message }}@enderror </span>
              </div>
              
              <div class="form-group">
                <label for="inputName">Product Discount</label>
                <input type="number" name="discount" class="form-control" value="{{ $productdetails->discount }}" >
                <span style="color:orange"> @error('discount') {{ $message }}@enderror </span>
              </div>
                            <div class="form-group">
                <label for="inputName">Product Quantity</label>
                <input type="number" name="quantity" class="form-control" id="quantity" value="{{ $productdetails->quantity }}">
                 <input type="hidden" id="quantitynew" value="{{ $productdetails->quantity }}">
                <span style="color:orange"> @error('quantity') {{ $message }}@enderror </span>
              </div>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Categories</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> 
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              
              <div class="form-group">
                <label for="inputEstimatedBudget">Main Category</label>
                <select class="form-control custom-select text-capitalize maincategory" name="maincategory">
                  <option value="" selected>-- Select Main Category --</option>
                  @foreach ($maincategories as $maincategory)
                  <option  value="{{$maincategory->id}}" @if( $maincategory->id == $productdetails->maincategory) selected="selected" @endif class="text-capitalize">
                  {{$maincategory->name}}</option>

                  @endforeach
                </select>
                <span style="color:orange"> @error('maincategory') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Second Category</label>
                <select class="form-control custom-select text-capitalize secondcategory" name="secondcategory">
                   <option value="" selected>-- Select Second Category --</option>
                 @foreach ($secondcategories as $secondcategory)
                  <option  value="{{$secondcategory->id}}" @if( $secondcategory->id == $productdetails->secondcategory) selected="selected" @endif class="text-capitalize">
                  {{$secondcategory->name}}</option>
                  @endforeach
                </select>
                <span style="color:orange"> @error('secondcategory') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Third Category</label>
                <select class="form-control custom-select text-capitalize thirdcategory" name="thirdcategory">
                 @foreach ($thirdcategories as $thirdcategory)
                  <option  value="{{$thirdcategory->id}}" @if( $thirdcategory->id == $productdetails->thirdcategory) selected="selected" @endif class="text-capitalize">
                  {{$thirdcategory->name}}</option>
                  @endforeach
                </select>
                <span style="color:orange"> @error('thirdcategory') {{ $message }}@enderror </span>
              </div>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        
        </div>
         <!-- Weight based prices --> 
           <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Products Price&nbsp;&nbsp;||&nbsp;&nbsp;Weight&nbsp;&nbsp;</h3>

              <div class="card-tools">
              <button type="button" name="add" id="add" class="btn btn-success btn-lg">+</button>
                <button type="button" class="btn btn-tool ml-3" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> 
                  <i class="fas fa-minus"></i></button> 
              </div>
            </div>
            <div class="card-body">
              <table class="table" id="dynamicTable">  

            
           <?php $i=0; ?>
            @foreach( $weightprices as $weightpriceshow)
           
            <?php $i++; ?>
            <tr>
              <td>
                <div class="input-group">
                  <input type="number" name="weightprice[<?php echo $i-1;?>][weight]" placeholder="Enter Your Weight" id="wrapped" class="form-control" value="{{$weightpriceshow->weight}}">
                  <div class="input-group-addon">
                    <select class="form-control" name="weightprice[<?php echo $i-1;?>][unit]">
                      <option value="">-- Select Weight Unit --</option>
                      <option value="kg" @if( $weightpriceshow->unit == "kg") selected="selected" @endif>Kilogram</option>
                      <option value="gm" @if( $weightpriceshow->unit =="gms") selected="selected" @endif>Gram</option>
                      <option value="litre" @if( $weightpriceshow->unit =="litre") selected="selected" @endif>litre</option>
                      <option value="ml" @if( $weightpriceshow->unit =="ml") selected="selected" @endif>Ml</option>
                    </select>
                  </div>
                </div>
              </td>
              <td>
                <input type="number" name="weightprice[<?php echo $i-1;?>][price]" placeholder="Slug" class="form-control"value="{{$weightpriceshow->price}}">
                 <input type="hidden" name="weightprice[<?php echo $i-1;?>][id]" value="{{$weightpriceshow->id}}">
              </td>
            </tr>
            @endforeach
          

        </table> 

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
</div>
      <div class="row">
        <div class="col-12">
          <a href="/viewproduct" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Update Product" class="btn btn-success float-right">
        </div>
      </div>
      <br />
    </form>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
<script type="text/javascript">

      $(".maincategory").change(function(){
        var maincategory = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
        url: "<?php echo route('productsecond') ?>",
        method: 'POST',
        data: {maincategory:maincategory, _token:token},
        success: function(data) {

        $(".secondcategory").html(data);
        }
        });
      }); //second category fetch method

      $(".secondcategory").change(function(){
        var secondcategory = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
        url: "<?php echo route('productthird') ?>",
        method: 'POST',
        data: {secondcategory:secondcategory, _token:token},
        success: function(data) {

        $(".thirdcategory").html(data);
        }
        });
      });  //third category fetch method


//preview multiple images
     function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                       $('#previousImg').hide();
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }

  //load cover image only
  function readURL(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#coverimageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }

  $("#coverimage").change(function() {
  readURL(this);
  });

//Product varients edit method
    

var i =<?php echo $i-1;?>;

      $('#quantity').change(function() {
      var old=document.getElementById('quantity').value;
        var quantity=document.getElementById('quantitynew').value=old;;

      }); //update quantity value
      $("#add").click(function(){

        ++i;
        var quantitynew=document.getElementById('quantitynew').value-1;

       //alert(quantity);
        if( i <= quantitynew){
        
         
        $("#dynamicTable").append('<tr><td><div class="input-group"><input type="text" name="weightprice['+i+'][weight]" placeholder="Enter Your Weight" id="wrapped" class="form-control maincategory"><div class="input-group-addon"><select class="form-control" name="weightprice['+i+'][unit]"><option value="" selected>-- Select Weight Unit --</option> <option value="kg">Kilogram</option><option value="gm">Gram</option><option value="litre">litre</option><option value="ml">Ml</option>></select></div></div></td><td><input type="text" name="weightprice['+i+'][price]" placeholder="Price" class="form-control"> <input type="hidden" name="weightprice['+i+'][id]" value=""></td> <td><button type="button" class="btn btn-danger remove-tr">-</button></td></tr>');


          $(document).on('click','.remove-tr', function(){  
          $(this).parents('tr').remove();
          });
        }
        else
        {
          
          swal({
            title: "Hold it Please",
            text: "You Cannot add Product Varient More then Your Product Quantity",
            icon: "warning",
            buttons: [
            'Yes, I dont want my varient details same',
            'No, I want my varient details same'
          ],
          dangerMode: true,
          }).then(function(isConfirm) {
          if (isConfirm) {
            swal({
            title: 'Fine',
            text: 'Your varient details are added back',
            icon: 'success'
            }).then(function() {
           //$('#forms').submit();
            });
          } else {
            swal("Fine", "Your Product Varients and Quantity is Refreshed now", "success");
            $('.remove-tr').parents('tr').remove();
            document.getElementById('quantity').value="";
          }
          });

        }
      });
</script>

@endsection
