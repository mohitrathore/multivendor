@extends('admin.layouts.app')
 
@section('content')
<style>
 .elipsis
{
 max-width: 300px;
 overflow: hidden;
 text-overflow: ellipsis;
 white-space: nowrap;
}
</style>
		 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2"> 
            <div class="col-sm-3"> </div>
            <div class="col-sm-12">
            <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header text-white"
            style="background: url('../dist/img/photo1.png') center center;">
            <h3 class="widget-user-username text-center">
            @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

            </h3>

            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard" class="text-white">Home</a></li>
            <li class="breadcrumb-item active ">View Product</li>
            </ol>
            </div>
            <div class="widget-user-image">
            <img class="img-circle" src="{{ asset('dist/img/product-main-icon.jpg')}}" alt="User Avatar">
            </div>

            </div>
            </div>

            </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-3"> <h3 class="card-title">Products
              

 
            </h3> </div>
            <div class="col-sm-7"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
            <div class="col-sm-1"> </div>
             <div class="col-sm-1">
            <div class="card-tools">

            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects table-responsive">
              <thead>
                 <thead>
                  <tr> 
                    <th style="min-width:190px;"></th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Disclaimer</th>
                    <th>Type</th>
                    <th>Product Code</th>
                    <th>Categories</th>
                    <th>Store Id</th>
                    <th>Discount %</th>
                    <th>Quantity</th>
                    <th>Stoke</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody id="myTable">
                  @forelse ($products as $product)
                  <tr>
                    <td>
                      <ul class="list-inline">
                              <li class="list-inline-item">
                                  <a href="/product_images/{{$product->coverimage}}" target="_blank" title="cover photo" >
                                   <i class="fa fa-file-image fa-2x" ></i>  </a>
                              </li>
                              <li class="list-inline-item">
                                <a href="./products/{{$product->id}}" class="text-success" target="_blank" title="Product View" > <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                              </li>
                              
                          </ul>

 
                    </td>
                    <td class="elipsis">{{ $product -> name }}...<br/><small>Created {{date('d-m-Y H:i a', strtotime($product->created_at))}}</small></td>
                    <td  class="elipsis">{{ $product -> description }}...</td>
                    <td  class="elipsis">{{ $product -> disclaimer}}...</td>
                    <td>
                      @if($product -> type == "veg")
                      <img src="{{asset('dist/img/veg.png')}}" style="width:50%;">
                      @elseif($product -> type =="non-veg")
                      <img src="{{asset('dist/img/nonveg.png')}}" style="width:50%;">
                      @endif

                    </td>
                    <td>{{ $product -> productcode }}</td>
                    <td>
                          <ul id="nestedlist">
                            <li>
                            @foreach($main as $ma)
                            @if($ma->id == $product->maincategory)
                            <span>{{$ma->name}}</span>
                            @endif
                            @endforeach
                              <ul>
                                <li> 
                                @foreach($second as $sec)
                                @if($sec->id == $product->secondcategory)
                                <span>{{$sec->name}}</span>
                                @endif
                                @endforeach
                                  <ul>
                                    <li> 
                                    @foreach($third as $thri)
                                    @if($thri->id == $product->thirdcategory)
                                    <span>{{$thri->name}}</span>
                                    @endif
                                    @endforeach
                                    </li>
                                  </ul>
                                </li>

                              </ul>
                            </li>
                          </ul>
                          </td>
                    <td>{{ $product -> store_id }}</td>
                    <td>{{ $product -> discount }} %</td>
                    <td>{{ $product -> quantity }}</td>
                    <td>
                      @if($product->stoke==1)
                         <span class="badge badge-success">In-Stoke</span>
                      @elseif($product->stoke==0)
                      <span class="badge badge-danger">Out-of-Stoke</span>
                      @endif
                    </td>
                    <td>
                      @if( $product -> status == 1)
                      <a href="inactiveproduct/{{$product->id}}" > <span class="badge badge-success">Active</span> </a>
                      @elseif( $product -> status == 0)
                      <a href="activeproduct/{{$product->id}}"><span class="badge badge-danger">Inactive</span></a>
                      @else
                      @endif
                    </td>
                    <td>
                      <a href="editproduct/{{$product->id}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fa fa-edit"></i></a>
                      <a href="deleteproduct/{{$product->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this product?');"><i class="fa fa-trash"></i>

                      </a>

                    </td>
                  </tr>
                  @empty
                  <tr><td colspan="12"><h4 class="text-center"> No record found</h4></td></tr>
                 
                  @endforelse
                 
              </tbody> 
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $products->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
