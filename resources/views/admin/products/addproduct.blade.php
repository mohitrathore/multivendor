@extends('admin.layouts.app')
 
@section('content') 
<style>
#wrapped+.input-group-addon {
    margin: 0;
    padding: 0;
    min-width: 8rem;
    border: none;
}
#wrapped+.input-group-addon>.form-control {
    border-radius: 0 .4rem .4rem 0;
    border-left: none;
}
img.thumb{
  width:20%;
}
</style>
			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
 
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			@if(session()->has('success'))
      <script>
      $( document ).ready(function() {
      toastr.success("{!! session()->get('success')!!}")
      });
      </script>
      @endif
     @if(session()->has('error'))
      <script>
      $( document ).ready(function() {
      toastr.error("{!! session()->get('error')!!}")
      });
      </script> 
      @endif  

       @if ($errors->any())
                    @foreach ($errors->all() as $error)

                    <script>
                    $( document ).ready(function() {
                  toastr.error("{{ $error }}")
                    });
                    </script>

                    @endforeach
        @endif   
        <!-- Product weight, price errors -->


			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Product</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/product-main-icon.jpg')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
     <section class="content">
         <form method="post" enctype="multipart/form-data" id="forms" action="{{ route('addproduct')}}">
              @csrf
      <input type="hidden"  name="productcode" value="PR<?php echo mt_rand();?>" >
      <input type="hidden"  name="status" value="1" >
      <div class="row">
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Product </h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input type="text" id="inputName" name="name" class="form-control" value="{{ old('name')}}">
                <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
              </div>
                <!-- Product code -->

              <div class="form-group">
                <label for="inputDescription">Product Description</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description" >{{ old('description')}}</textarea>
                <span style="color:orange"> @error('description') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Disclaimer</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="disclaimer" >{{ old('disclaimer')}}</textarea>
                <span style="color:orange"> @error('disclaimer') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Cover Image</label> <br />
               <input type="file" class="form-group" name="coverimage">
               <span style="color:orange"> @error('coverimage') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Images</label> <br />
               <input type="file" class="form-group" name="images[]" type="file" id="file-input" onchange="loadPreview(this)"multiple>
                       <span class="text-danger">{{ $errors->first('images') }}</span>
                       <div id="thumb-output"></div>
                       <br>
               <span style="color:orange"> @error('images') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputStatus"> Product Stoke</label>
                <select class="form-control custom-select" name="stoke">
                  <option value="" selected disabled>Select one</option>
                  <option value="1" @if( old("stoke")== 1) selected="selected" @endif >In Stock</option>
                  <option value="0"  @if( old("stoke") == 0) selected="selected" @endif>Out of Stock</option>
                </select>
                <span style="color:orange"> @error('stoke') {{ $message }}@enderror </span>
              </div> 
              <div class="form-group">
                <label for="inputStatus"> Product Contains</label>
                <select class="form-control custom-select" name="type">
                  <option value="" selected disabled>-- Select one --</option>
                  <option value="veg"  @if( old("type")== "veg") selected="selected" @endif>Vegetarian</option>
                  <option value="non-veg" @if( old("type")== "non-veg") selected="selected" @endif>Non Vegetarian</option>
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputStatus"> Product Criteria</label>
                <select class="form-control custom-select" name="criteria">
                  <option value="" selected disabled>-- Select -- </option>
                  <option value="feature"  @if( old("criteria")== "feature") selected="selected" @endif>Featured</option>
                  <option value="top-saver"  @if( old("criteria")== "top-saver") selected="selected" @endif>Top Saver</option>
                  <option value="best-offer"  @if( old("criteria")== "best-offer") selected="selected" @endif>Best Offer</option>
                 
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
             
              <div class="form-group">
                <label for="inputClientCompany">Store</label>
                <select class="form-control custom-select text-capitalize store" name="store_id" >
                  <option value="">-- Select Store --</option>
                  @foreach ($stores as $store)
                  <option  value="{{$store->id}}" class="text-capitalize" @if( old("store_id")== $store->id) selected="selected" @endif >
                  {{ $store->name}} ( {{$store->email}} )</option>

                  @endforeach
                </select>
                <span style="color:orange"> @error('store_id') {{ $message }}@enderror </span>
              </div>
              
              <div class="form-group">
                <label for="inputName">Product Discount</label>
                <input type="number" name="discount" class="form-control" value="{{ old('discount') }}" >
                <span style="color:orange"> @error('discount') {{ $message }}@enderror </span>
              </div>
                            <div class="form-group">
                <label for="inputName">Product Quantity</label>
                <input type="number" name="quantity" class="form-control" id="quantity" value="{{ old('quantity') }}">
                <span style="color:orange"> @error('quantity') {{ $message }}@enderror </span>
              </div>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-6">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Categories</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> 
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div id="maincategory"></div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Main Category</label>
                <select class="form-control custom-select text-capitalize maincategory" name="maincategory">
                 
                </select>
                <span style="color:orange"> @error('maincategory') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Second Category</label>
                <select class="form-control custom-select text-capitalize secondcategory" name="secondcategory">
                 
                </select>
                <span style="color:orange"> @error('secondcategory') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Third Category</label>
                <select class="form-control custom-select text-capitalize thirdcategory" name="thirdcategory">
                 
                </select>
                <span style="color:orange"> @error('thirdcategory') {{ $message }}@enderror </span>
              </div>
            </div>
            <!-- /.card-body -->
          </div>

          <!-- /.card -->
        
        </div>
         <!-- Weight based prices --> 
           <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Products Price&nbsp;&nbsp;||&nbsp;&nbsp;Weight&nbsp;&nbsp;</h3>

              <div class="card-tools">
                <button type="button" name="add" id="add" class="btn btn-success btn-lg mr-4">+</button> 
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"> 
                  <i class="fas fa-minus"></i></button> 
              </div>
            </div>
            <div class="card-body">
              <table class="table" id="dynamicTable">  

            

            <tr>  

                <td>
                  <div class="input-group">
                  <input type="text" name="weightprice[0][weight]" placeholder="Enter Your Weight" id="wrapped" class="form-control maincategory">
                  <div class="input-group-addon">
                  <select class="form-control" name="weightprice[0][unit]">
                  <option value="" selected>-- Select Weight Unit --</option>
                  <option value="kg">Kilogram</option>
                  <option value="gm">Gram</option>
                  <option value="litre">litre</option>
                  <option value="ml">Ml</option>
                  </select>
                  </div>
                  </div>
                </td>  
                
              
                <td><input type="text" name="weightprice[0][price]" placeholder="Price" class="form-control"></td> 

                

            </tr>  

        </table> 

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
</div>
      <div class="row">
        <div class="col-12">
          <a href="#" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Create new Product" class="btn btn-success float-right">
        </div>
      </div>
      <br />
    </form>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
<script type="text/javascript">

  $(".store").change(function(){
        var storeid= $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
        url: "<?php echo route('productrestrictcategory') ?>",
        method: 'POST',
        data: {storeid:storeid,_token:token},
        success: function(data) {

        $(".maincategory").html(data);
        }
        });
      }); //second category fetch method

      $(".maincategory").change(function(){
        var maincategory = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
        url: "<?php echo route('productsecond') ?>",
        method: 'POST',
        data: {maincategory:maincategory, _token:token},
        success: function(data) {

        $(".secondcategory").html(data);
        }
        });
      }); //second category fetch method

      $(".secondcategory").change(function(){
        var secondcategory = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
        url: "<?php echo route('productthird') ?>",
        method: 'POST',
        data: {secondcategory:secondcategory, _token:token},
        success: function(data) {

        $(".thirdcategory").html(data);
        }
        });
      });  //third category fetch method

    

var i = 0;
      $("#add").click(function(){

        ++i;
        var quantity=document.getElementById('quantity').value -1;
       //alert(quantity);
        if( i <= quantity){
        
         
        $("#dynamicTable").append('<tr><td><div class="input-group"><input type="text" name="weightprice['+i+'][weight]" placeholder="Enter Your Weight" id="wrapped" class="form-control maincategory"><div class="input-group-addon"><select class="form-control" name="weightprice['+i+'][unit]"><option value="" selected>-- Select Weight Unit --</option> <option value="kg">Kilogram</option><option value="gm">Gram</option><option value="litre">litre</option><option value="ml">Ml</option>></select></div></div></td><td><input type="text" name="weightprice['+i+'][price]" placeholder="Price" class="form-control"></td> <td><button type="button" class="btn btn-danger remove-tr">-</button></td></tr>');


          $(document).on('click','.remove-tr', function(){  
          $(this).parents('tr').remove();
          });
        }
        else
        {
          
          swal({
            title: "Hold it Please",
            text: "You Cannot add Product Varient More then Your Product Quantity",
            icon: "warning",
            buttons: [
            'Yes, I dont want my varient details same',
            'No, I want my varient details same'
          ],
          dangerMode: true,
          }).then(function(isConfirm) {
          if (isConfirm) {
            swal({
            title: 'Fine',
            text: 'Your varient details are added back',
            icon: 'success'
            }).then(function() {
           //$('#forms').submit();
            });
          } else {
            swal("Fine", "Your Product Varients and Quantity is Refreshed now", "success");
            $('.remove-tr').parents('tr').remove();
            document.getElementById('quantity').value="";
          }
          });

        }
      });
//preview images
     function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
</script>

@endsection
   @section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>
    <script>

    </script>
    <script src="/js/mapInput.js"></script>

@endsection
