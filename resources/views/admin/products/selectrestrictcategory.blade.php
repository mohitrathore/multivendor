<option value="" selected>-- Select Main Category --</option>
                @if($restrictions!==null)
                  <?php $restrict_cat=json_decode($restrictions->restrict_cat,true); ?>
                  @foreach ($maincategories as $maincategory)
                    @for( $i = 0 ; $i < count($restrict_cat) ; $i++)
                      @if($maincategory->id == $restrict_cat[$i])
                        <option  value="{{$maincategory->id}}" class="text-capitalize">
                        {{$maincategory->name}}</option>
                        @else
                      @endif
                    @endfor
                  @endforeach
                @else
                  @foreach ($maincategories as $maincategory)
                    <option  value="{{$maincategory->id}}" class="text-capitalize">
                    {{$maincategory->name}}</option>
                  @endforeach
                @endif