@extends('admin.layouts.app')
 
@section('content')
     <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
            <div class="row mb-2"> 
            <div class="col-sm-3"> </div>
            <div class="col-sm-12">
            <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header text-white"
            style="background: url('../../dist/img/photo1.png') center center;">
            <h3 class="widget-user-username text-center">
            @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

            </h3>
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
            <li class="breadcrumb-item active ">Product Details</li>
            </ol>
            </div>
            <div class="widget-user-image">
            <img class="img-circle" src="{{ asset('dist/img/product-main-icon.jpg')}}" alt="User Avatar">
            </div>

            </div>
            </div>

            </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-sm-6">
              <h3 class="d-inline-block d-sm-none"></h3>
              <div class="col-12 text-center" style="min-height:300px;">
                <img src="/product_images/{{$productdetails->coverimage}}" class="product-image" style="width:60%;" alt="Cover Image">
              </div>
              <div class="col-12 product-image-thumbs">
              @if($productdetails->images == "" || $productdetails ->images ==null)
              @else
                @foreach(json_decode($productdetails ->images) as $image)
                     <div class="product-image-thumb"><img src="/product_images/{{$image}}" ></div>
                @endforeach
              @endif
              </div>
            </div>
            <div class="col-12 col-sm-6">
              <a href="/products"><button class="btn btn-info float-right"> <i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp; Go Back</button></a>
            <div class="bg-light py-2 px-3 mt-4">
                <h3 class="mb-0 text-capitalize">
                {{$productdetails->name}}
                </h3>
                <br />
                <p>
                  <b>Created at {{date('d-m-Y H:i a', strtotime($productdetails->created_at))}}</b>
                  <b style="float:right;">
                    @if($productdetails-> type == "veg")
                    <img src="{{asset('dist/img/veg.png')}}" style="width:55%;">
                    @elseif($productdetails-> type =="non-veg")
                    <img src="{{asset('dist/img/nonveg.png')}}" style="width:55%;">
                    @endif
                  </b>
                </p> 
            </div>
            <br />
            <div class="row">
                <div class="col-12 col-sm-3">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Total Quantity</span>
                      <span class="info-box-number text-center text-muted mb-0">{{$productdetails->quantity}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Total Discount %</span>
                      <span class="info-box-number text-center text-muted mb-0">{{$productdetails->discount}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-3">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Availability</span>
                      <span class="info-box-number text-center text-muted mb-0">
                        @if($productdetails->stoke==1)
                         <span class="badge badge-success">In-Stoke</span>
                      @elseif($productdetails->stoke == 0)
                      <span class="badge badge-danger">Out-of-Stoke</span>
                      @endif
                       </span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-3">

                  @if($productdetails->status == 1)
                  <div class="info-box bg-success">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-white">Activated</span>
                    </div>
                  </div>
                    @elseif($productdetails->status == 0)
                  <div class="info-box bg-danger">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-white">Inactivated</span>
                    </div>
                  </div>
                    @endif
                </div>
              </div>
            <div class="row mt-4">
            <nav class="w-100">
              <div class="nav nav-tabs" id="product-tab" role="tablist">
                <a class="nav-item nav-link active" id="product-desc-tab" data-toggle="tab" href="#product-desc" role="tab" aria-controls="product-desc" aria-selected="true">Description</a>
                <a class="nav-item nav-link" id="product-comments-tab" data-toggle="tab" href="#product-comments" role="tab" aria-controls="product-comments" aria-selected="false">Disclaimer</a>
                <a class="nav-item nav-link" id="storetab" data-toggle="tab" href="#store" role="tab" aria-controls="product-comments" aria-selected="false">Store Details</a>
                <a class="nav-item nav-link" id="weigth-price" data-toggle="tab" href="#weight-price-details" role="tab" aria-controls="product-weight-price" aria-selected="false">Weight Based Price</a>
              </div>
            </nav>
            <div class="tab-content p-3" id="nav-tabContent">
              <div class="tab-pane fade show active p-2" id="product-desc" role="tabpanel" aria-labelledby="product-desc-tab"> 
                {{ $productdetails->description }} </div>

              <div class="tab-pane fade p-2" id="product-comments" role="tabpanel" aria-labelledby="product-comments-tab"> 
               {{$productdetails->disclaimer}}
              </div>
              <div class="tab-pane fade p-2" id="store" role="tabpanel" aria-labelledby="product-comments-tab"> 
             
              <ul class="list-unstyled">
                <li>
                  <i class="fa fa-store"></i> &nbsp;{{ $store->name}}
                </li>
                <li>
                  <i class="fa  fa-envelope"></i> &nbsp; {{ $store->email}}
                </li>
                <li>
                 <i class="fa fa-map-marker"></i> &nbsp;{{ $store->address}}
                </li>
                <li>
                 <i class="fa fa-clock"></i> &nbsp; {{ $store->opening_time}} - {{ $store->closing_time}}
                </li>
                
              </ul>
              </div>

              <div class="tab-pane fade p-2" id="weight-price-details" role="tabpanel" aria-labelledby="product-product-weight"> 
                            
                <div class="row">
                  <div class="col-sm-2">  </div>
                <div class="col-sm-4 text-justify text-bold">Weight ( Units )  </div>
                <div class="col-sm-4 text-justify text-bold"> Price ( INR )</div>
                 <div class="col-sm-2">  </div>
                 <hr>
                  @foreach($weightprices as $weightprice)
                   <div class="col-sm-2">  </div>
                <div class="col-sm-4 text-justify"> 
                  <i class="fa fa-balance-scale"></i> &nbsp; {{ $weightprice->weight }} &nbsp; {{ $weightprice->unit }}
                </div>
                <div class="col-sm-4 text-justify"><i class="fa fa-rupee-sign fa-xs"></i>&nbsp; {{ $weightprice->price}} </div>
                <div class="col-sm-2">  </div>
                 @endforeach
                </div>
                 
              </div>
            </div>
          </div>
           </div>
          </div>
          
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
