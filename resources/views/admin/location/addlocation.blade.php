@extends('admin.layouts.app')

@section('content')
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			<!-- Content Header (Page header) -->

			<div class="content-header">
			<div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-7 offset-sm-2">
			      &nbsp; 
                  @if(session()->has('success'))
			<script>
			$( document ).ready(function() {
			toastr.success("{!! session()->get('success')!!}")
			});
			</script>
			@endif
			@if(session()->has('error'))
			<script>
			$( document ).ready(function() {
			toastr.error("{!! session()->get('error')!!}")
			});
			</script>
			@endif 
			</div><!-- /.col -->
			<div class="col-sm-3">
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard">Home</a></li>
			<li class="breadcrumb-item active">Location</li>
			</ol>
			</div><!-- /.col -->
			</div><!-- /.row -->
			</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
			<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
			<div class="col-lg-12 col-12">
			<div class="container">
			<div class="row justify-content-center">
			<div class="col-md-8">
		   <h3 class="bg-dark text-white p-3 " style="font-size:20px;background:">Add Location</h3>
			<form method="post" action="{{ route('insertlocation')}}">
			@csrf<div class="form-group">
			<label for="exampleInputEmail1">Name</label>
			<input type="text" class="form-control" name="name" placeholder="Enter name" value="{{old('name')}}">
			<span style="color:orange"> @error('name') {{ $message }}@enderror </span>
			</div>
			<div class="form-group">
			<label for="address_address">Address</label>

			<input type="text" name="address_address" class="form-control" id="searchInput">	
				

			<input type="hidden" name="address_latitude" id="lat"/>
			<input type="hidden" name="address_longitude" id="lng"/>
			<input type="hidden" name="city" id="city"/>
			<input type="hidden" name="state" id="state"/>
		
			</div>
			<div id="address-map-container" style="width:100%;height:400px; ">
			<div style="width: 100%; height: 100%"  class="map" id="map" ></div>
			
			</div>
			<br />
			<div class="alert alert-info" role="alert">
			You can drag    <i class="nav-icon fa fa-hand-rock fa-2x"></i>   Map Icon to change a location !
			</div>
			<br />
			


			<!-- <div class="form-group">
			<label for="address_address">Address</label>
			<input type="text" id="address-input" name="address_address" class="form-control map-input">
			<input type="hidden" name="address_latitude" id="address-latitude" value="0" />
			<input type="hidden" name="address_longitude" id="address-longitude" value="0" />
			</div>
			<div id="address-map-container" style="width:100%;height:400px; ">
			<div style="width: 100%; height: 100%" id="address-map"></div>
			</div> -->
			<br />
			<button type="submit" name="add-data" class="btn btn-primary">Submit</button>
			</form>
			</div>
			</div>
			</div>
			</div>
			<!-- ./col -->
			</div>

			</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
			</div>
@endsection
@section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>
    <script>

    </script>
    <script src="/js/mapInput.js"></script>

@endsection