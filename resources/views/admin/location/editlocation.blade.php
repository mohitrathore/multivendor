@extends('admin.layouts.app')

@section('content')
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			<!-- Content Header (Page header) -->

			<div class="content-header">
			<div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-6">
			<h1 class="m-0 text-dark">Edit Location
          
			</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard">Home</a></li>
			<li class="breadcrumb-item active">Location</li>
			</ol>
			</div><!-- /.col -->
			</div><!-- /.row -->
			</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content">
			<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">
			<div class="col-lg-12 col-12">
			<div class="container">
			<div class="row justify-content-center">
			<div class="col-md-8">

			@if(session()->has('success'))
			<script>
			$( document ).ready(function() {
			toastr.success("{!! session()->get('success')!!}")
			});
			</script>
			@endif
			@if(session()->has('error'))
			<script>
			$( document ).ready(function() {
			toastr.error("{!! session()->get('error')!!}")
			});
			</script>
			@endif 				
			<p class="alert alert-dark text-white"> <b>Note - </b>Please update atleast one field to avoid errors !</p>
			<form method="post" action="{{route('updatelocation')}}">
			@csrf
			<input type="hidden" name="cid" value="{{$editlocationdata->id}}">
			<div class="form-group">
			<label for="exampleInputEmail1">Name</label>
			<input type="text" class="form-control" name="name" placeholder="Enter name" 
			value="{{$editlocationdata->name}}">
			<span style="color:orange"> @error('name') {{ $message }}@enderror </span>
			</div>
			<div class="form-group">
			<label for="address_address">Address </label>

			
			
			<input type="text" id="searchInput" name="newaddress_address" value="{{$editlocationdata->address_address}}"class="form-control" placeholder="Enter a new location to update">	
			<input type="hidden" name="address_latitude" id="lat" value="{{$editlocationdata->address_latitude}}"/>
			<input type="hidden" name="address_longitude" id="lng" value="{{$editlocationdata->address_longitude}}"/>

			<input type="hidden" name="city" id="city" value="{{$editlocationdata->city}}"/>
			<input type="hidden" name="state" id="state" value="{{$editlocationdata->state}}"/>
			</div>
			<br />
			
			<div class="row" style="width:100%;height:400px; ">
                <div class="col-md-5">
            	 <h4> Old Location</h4>
            <div style="width: 100%; height:400px;"   id="map1" ></div> </div>
            <div class="col-md-6 offset-1"> 

            <h4> New Location</h4>
            <div id="address-map-container" class="row" style="width:100%;height:400px; ">
            <div style="width: 100%; height:100%"  class="map" id="map" ></div>
            </div>
            </div>
           
           
             </div>
			<br />
			<br />
			<div class="alert alert-info" role="alert">
			You can drag    <i class="nav-icon fa fa-hand-rock fa-2x"></i>   Map Icon to change a location !
			</div>
			<br /><br />
			<button type="submit" name="add-data" class="btn btn-primary">Update</button>
			
			
			<br />
			</form>
			</div>
			</div>
			</div>
			</div>
			<!-- ./col -->
			</div>

			</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
			</div>
@endsection
@section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>
    <script>
      // Initialize and add the map
      function initMap() {
        // The location of Uluru
     
        const uluru = { lat:{{$editlocationdata->address_latitude}}, lng:{{$editlocationdata->address_longitude}} };
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map1"), {
          zoom: 13,
          center: uluru,
        });
        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
          position: uluru,
          map: map,
        });
      }
      google.maps.event.addDomListener(window, 'load', initMap);
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initMap" async defer></script>
    <script src="/js/mapInput.js"></script>

@endsection