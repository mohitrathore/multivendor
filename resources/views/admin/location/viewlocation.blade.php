@extends('admin.layouts.app')
 
@section('content')
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
			<!-- Content Header (Page header) -->

			<div class="content-header">
			<div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-6">
			<h1 class="m-0 text-dark">View Location 
			@if(session()->has('success'))
      <script>
      $( document ).ready(function() {
      toastr.success("{!! session()->get('success')!!}")
      });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
      $( document ).ready(function() {
      toastr.error("{!! session()->get('error')!!}")
      });
      </script>
      @endif  
			</h1>
			</div><!-- /.col -->
			<div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard">Home</a></li>
			<li class="breadcrumb-item active">View Location 
            </li>
			</ol>
			</div><!-- /.col -->
			</div><!-- /.row -->
			</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			     <div class="card">
            
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Status</th>
                    <th>Action</th>
                   
                  </tr>
                  </thead>
                  <tbody>
                  	@forelse ($locations as $location)
                  <tr>
                    <td>{{ $location -> name }}</td>
                    <td>{{ $location -> address_address }}</td>
                    <td>{{ $location -> city }}</td>
                    <td>{{ $location -> state }}</td>
                    <td> 
                        @if( $location -> status == 1)
                        <a href="inactivelocation/{{$location->id}}" > <span class="badge badge-success">Active</span> </a>
                        @elseif( $location -> status == 0)
                        <a href="activelocation/{{$location->id}}"><span class="badge badge-danger">Inactive</span></a>
                        @else
                        @endif
                    </td>
                    <td>
                    	<a href="editlocation/{{$location->id}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                    	<a href="deletelocation/{{$location->id}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure you want to delete this location?');"><i class="fa fa-trash"></i></a>

                    </td>

                  </tr>
                  @empty
                  <tr><td colspan="4"><h4 class="text-center"> No record found</h4></td></tr>
                  @endforelse
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Location</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
			<!-- /.content -->
			</div>
@endsection
@section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM') }}&libraries=places&callback=initialize" async defer></script>
    <script src="/js/mapInput.js"></script>

@endsection