@extends('admin.layouts.app')
  
@section('content') 
			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <form method="post"  action="{{route('add.couponcode')}}">
             	@csrf
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			 @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
    
      @endif 

			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Coupon Code</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/coupon.png')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Add Coupon Code</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputEstimatedBudget">Coupon Code</label>
                <input   class="form-control couponcode" placeholder="Coupon Code" name="code" type="text" value="{{old('code')}}" readonly>
                <span style="color:orange"> @error('code') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Coupon Code Amount</label>
                <input   class="form-control" placeholder="Coupon Code Amount" name="amount" type="number" value="{{old('amount')}}">
                <span style="color:orange"> @error('amount') {{ $message }}@enderror </span>
              </div>
             
              <div class="form-group">
                <label for="inputStatus">Type</label>
                <select class="form-control custom-select" name="type">
                <option value="">-- Select Coupon Code Type --</option>
                <option value="flat" @if (old("type") == "flat") selected="selected" @endif>Flat</option>
                <option value="percentage" @if (old('type') == 'percentage') selected="selected" @endif>Percentage % </option>
               
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
               <div class="form-group">
                <label for="inputEstimatedBudget">Coupon Expiry Date</label>
                <input type="date"  name="expirydate" class="form-control" id="expirydate" />
                <span style="color:orange"> @error('expirydate') {{ $message }}@enderror </span>
              </div>
              
              <div class="col-12">
              <a href="{{route('couponlist')}}" class="btn btn-secondary">Cancel</a>
              <input type="submit" value="Create Coupon Code" class="btn btn-success float-right">
              </div>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
          <div class="col-md-2"></div>
      </div>
      
  </form>
    </section>
    <!-- /.content -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
        $( document ).ready(function() {
       
        function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}
 $('.couponcode').val(randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'));
//coupon radndom code generate here

//expiry date previous date disable 
$(function(){
    var dtToday = new Date();
    
    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();
    if(month < 10)
        month = '0' + month.toString();
    if(day < 10)
        day = '0' + day.toString();
    
    var minDate= year + '-' + month + '-' + day;
    
    $('#expirydate').attr('min', minDate);
});


        });
      </script>
@endsection
