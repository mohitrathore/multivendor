@extends('admin.layouts.app')
 
@section('content')
     <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
      <section class="content-header">
   
      <div class="container-fluid">
      <div class="row mb-2">
      <div class="col-sm-3"> </div>
      <div class="col-sm-12">
      <div class="card card-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header text-white"
      style="background: url('../dist/img/photo1.png') center center;">
      <h3 class="widget-user-username text-center">
       @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
      @endif 
                       <!-- Open modal when error validation occur -->
                        @if($errors->has('name') || $errors->has('parentid')) 
                        <script>
                        $( document ).ready(function() {
                        $("#editmodal{{old('cid')}}").modal("show");
                        });
                        </script>
                        @endif
      </h3>
      <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
      <li class="breadcrumb-item active ">View Third Category</li>
      </ol>
      </div>
      <div class="widget-user-image">
      <img class="img-circle" src="{{ asset('dist/img/category-icon.jpg')}}" alt="User Avatar">
      </div>

      </div>
      </div>

      </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
           <div class="card-header">
          <div class="row">
            <div class="col-sm-5">
             <h3 class="card-title"><b>Third Category</b></h3>
             
            <a href="thirdcategory"><button type="button" class="btn btn-dark float-right"> Add New Category</button></a>
             </div>
            <div class="col-sm-5"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
           
             <div class="col-sm-2">
            <div class="card-tools">

              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped ">
              <thead>
                 <thead>
                  <tr> 
                    <th style="width:10%;"></th>
                    <th style="width:30%;">Parent Category</th>
                    <th style="width:20%;">Name</th>
                    <th style="width:20%;">Slug</th>
                    <th style="width:20%;">Status </th>
                    <th style="width:20%;">Action</th>
                  </tr>
                  </thead>
                  <tbody id="myTable">
                      @forelse ($categories as $category)
                      <!-- Modal edit start -->
                      <div class="modal fade" id="editmodal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit <b class="text-capitalize">{{$category->name}}</b> Third Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form method="post" action="{{ route('updatethirdcategory') }}">
                        <div class="modal-body">
                        @csrf
                         <label for="inputEstimatedBudget"> Parent Category</label>
                        <select class="form-control custom-select text-capitalize" name="parentid">
                          <option value="">-- Select Parent Category --</option>
                          @foreach ($parents as $parent)
                          <option  value="{{$parent->id}}" class="text-capitalize" @if ( $parent->id == $category->parentid) selected="selected" @endif>{{ $parent->name }}</option>
                         
                          @endforeach
                        </select>
                         <span style="color:orange"> @error('parentid') {{ $message }}@enderror </span>
                        <div class="form-group">
                        <label for="inputEstimatedBudget"> Third Category Name</label>
                        <input type="hidden" name="cid"  value="{{$category->id}}">
                        <input   class="form-control text-capitalize main" placeholder=" Main Category Name" name="name" type="text" value="{{$category->name}}"> 
                        <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
                        </div>

                        <div class="form-group">
                        <label for="inputEstimatedBudget"> Slug</label>
                        <input   class="form-control text-capitalize slug" placeholder=" Main Category Slug" name="slug" type="text" value="{{$category->slug}}">
                        <span style="color:orange"> @error('slug') {{ $message }}@enderror </span>
                        </div>

                        </div>
                        <div class="modal-footer">
                        <a href=""><button type="button" class="btn btn-secondary">Cancel</button></a>
                        <button type="submit" class="btn btn-primary">Update Third Category</button>
                        </div>
                      </form>
                      </div>
                      </div>
                      </div>
                      <!-- Modal Edit end -->
                  <tr>
                    <td> {{ $loop->index + 1 }}</td>
                    <td class="text-capitalize">
                        @foreach ($parents as $parent)
                        @if($parent->id == $category->parentid)
                        {{$parent->name}}
                         @if($parent->status == '0')
                          &nbsp;<i class="fa fa-circle fa-xs text-danger"></i>
                          @elseif($parent->status == '1')
                          &nbsp;<i class="fa fa-circle fa-xs text-success"></i>
                          @endif
                        @endif
                        @endforeach
                    </td>
                    <td class="text-capitalize">{{ $category -> name }}<br/><small>Created {{date('d-m-Y H:i A', strtotime($category->created_at))}}</small>
                    </td>
                    <td class="text-capitalize">
                      {{ $category -> slug }} 
                     </td>
                    <td>
                      @if( $category -> status == 1)
                     <a href="inactivethird/{{$category->id}}"><i data-toggle="tooltip" data-placement="left" title="Inactive" class="fas fa-toggle-on text-success fa-2x" ></i></a>
                      @elseif( $category -> status == 0)
                      @foreach ($parents as $parent)

                        @if($parent->id == $category->parentid)
                          @if($parent->status == '0')
                          &nbsp;<i data-toggle="tooltip" data-placement="left"  title="Active" class="fas fa-toggle-off text-danger fa-2x" id="parentoff"></i>
                          @elseif($parent->status == '1')
                         <a href="/activethird/{{$category->id}}"><i data-toggle="tooltip" data-placement="left"  title="Active" class="fas fa-toggle-off text-danger fa-2x" ></i></a>
                          @endif
                          @endif
                      @endforeach
                       
                      
                      @else
                      @endif
                    </td>
                    <td>
                       <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#editmodal{{$category->id}}"><i class="fa fa-edit"></i></button>
                      <a href="deletemaincategory/{{$category->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this Driver?');"><i class="fa fa-trash"></i></a>

                    </td>
                  </tr>
                  @empty
                  <tr><td colspan="5"><h5 class="text-center"> No Third Categories Found</h5></td></tr>
                  @endforelse
                 
              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $categories->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
      $( document ).ready(function() {
        $('#parentoff').on('click', function(){ 
            swal({
            title: "Parent Category is Inactivated",
            text: "If you Activate main category then all Sub,Sub-child are automatically Active. To activate your Parent Category, Please click on OK, Otherwise click on Cancel",
            icon: "error",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                 
                  swal({
                  title: '',
                  text: 'Wait! You will be redirect to main category section in 5 Sec.',
                  timer: 3500
                  }).then(function (result) {
                  window.location.href = "/viewsecondcategory";
                  });
            } 
            });
        }); 
      });
       $(document ).ready(function() {
              var $src = $('.main'),
              $dst = $('.slug');
              $src.on('input', function () {
              $dst.val($src.val());
              });
              });
  </script>
  <!-- Error message when parent category inactivated found -->


@endsection
