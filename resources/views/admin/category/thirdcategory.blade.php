@extends('admin.layouts.app')
 
@section('content') 


			  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    
      <div class="container-fluid">
			<div class="row mb-2">
			<div class="col-sm-3"> </div>
			<div class="col-sm-12">
			<div class="card card-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
			<div class="widget-user-header text-white"
			style="background: url('../dist/img/photo1.png') center center;">
			<h3 class="widget-user-username text-center">
			 @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
      @endif 
			

			</h3>
			<ol class="breadcrumb float-sm-right">
			<li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
			<li class="breadcrumb-item active ">Add Third Category</li>
			</ol>
			</div>
			<div class="widget-user-image">
			<img class="img-circle" src="{{ asset('dist/img/category-icon.jpg')}}" alt="User Avatar">
			</div>

			</div>
			</div>

			</div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">Add Third Category</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
           
            </div> 
            <form action="{{ route('addthirdcategory') }}" method="POST">

        @csrf

   

        @if ($errors->any())

            <div class="alert alert-danger">

                <ul>

                    @foreach ($errors->all() as $error)

                        <li>{{ $error }}</li>

                    @endforeach

                </ul>

            </div>

        @endif   

        <table class="table" id="dynamicTable">  
          <tr> 
          <td>
          <select class="form-control custom-select" name="thirdcategory[0][parentid]">
          <option value="">-- Select Parent Category --</option>
          @forelse ($categorydetail as $category)
          <option  value="{{$category->id}}" class="text-capitalize" @if ( old("thirdcategory[0][parentid]") == $category->id) selected="selected" @endif>{{ $category->name }}</option>
          @empty
          <option value="">No Parent Category Found</option>
          @endforelse
          </select>
          </td>
          <td>
          <input type="text" name="thirdcategory[0][name]" placeholder="Enter Your Third Category Name" class="form-control" />
          </td>
           <input type="hidden" name="thirdcategory[0][status]" class="form-control" value="1"><td><input type="text" name="thirdcategory[0][slug]" placeholder="Slug" class="form-control"></td>
          <td><button type="button" name="add" id="add" class="btn btn-success">+</button></td>  

          </tr>  

        </table> 

    

       <div class="col-12">
          <a href="{{url('/viewthirdcategory')}}" class="btn btn-secondary">Cancel</a>
            <input type="submit" value="Create Third Category" class="btn btn-success float-right">
        </div>
        <br />

    </form>

</div>


            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
          <div class="col-md-2"></div>
      </div>
      
 
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->


   

<script type="text/javascript">

    var i = 0;

    $("#add").click(function(){

        ++i;

        $("#dynamicTable").append('<tr><td><select class="form-control custom-select" name="thirdcategory['+i+'][parentid]"><option value="">-- Select Parent Category --</option>@forelse ($categorydetail as $category)<option  value="{{$category->id}}" class="text-capitalize" @if ( old("thirdcategory['+i+'][parentid]") == $category->id) selected="selected" @endif>{{ $category->name }}</option>@empty<option value="">No Parent Category Found</option>@endforelse</select></td><td><input type="text" name="thirdcategory['+i+'][name]" placeholder="Enter Your Third Category Name" class="form-control" /></td> <input type="hidden" name="thirdcategory['+i+'][status]" class="form-control" value="1"><td><input type="text" name="thirdcategory['+i+'][slug]" placeholder="Slug" class="form-control"></td> <td><button type="button" class="btn btn-danger remove-tr">-</button></td></tr>');

    });

    $(document).on('click', '.remove-tr', function(){  

         $(this).parents('tr').remove();

    });  

   

</script>

@endsection
