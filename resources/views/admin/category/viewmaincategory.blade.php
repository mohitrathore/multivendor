@extends('admin.layouts.app') 
 
@section('content')
     <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
      <section class="content-header">
   
      <div class="container-fluid">
      <div class="row mb-2">
      <div class="col-sm-3"> </div>
      <div class="col-sm-12">
      <div class="card card-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header text-white"
      style="background: url('../dist/img/photo1.png') center center;">
      <h3 class="widget-user-username text-center">
      @if(session()->has('success'))
     <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
      </script>
      @endif
      @if(session()->has('error'))
      <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
      </script>
      @endif             
                        <!-- Open modal when error validation occur -->
                        @if($errors->has('name') || $errors->has('slug') || $errors->has('image')) 
                        <script>
                        $( document ).ready(function() {
                        $("#editmodal{{old('cid')}}").modal("show");
                        });
                        </script>
                        @endif
                        <!-- Open modal when error validation occur -->
                        @if($errors->has('maincategory')) 
                        <script>
                        $( document ).ready(function() {
                        $("#inactivation{{old('cid1')}}").modal("show");
                        $("#chooseoption{{old('cid1')}} option[value=1]").attr('selected','selected');
                        $('.assign').show();
                        $('.noassign').hide();
                        });
                        </script>
                        @endif
       

      </h3>
      <ol class="breadcrumb float-sm-right">
      <li class="breadcrumb-item"><a href="dashboard" class="text-white">Home</a></li>
      <li class="breadcrumb-item active ">View Main Category</li>
      </ol>
      </div>
      <div class="widget-user-image">
      <img class="img-circle" src="{{ asset('dist/img/category-icon.jpg')}}" alt="User Avatar">
      </div>

      </div>
      </div>

      </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
           <div class="card-header">
           <div class="row">
            <div class="col-sm-5">
             <h3 class="card-title"><b>Main Category</b></h3>
             
            <a href="maincategory"><button type="button" class="btn btn-dark float-right"> Add New Category</button></a>
             </div>
            <div class="col-sm-5"> <input class="form-control" id="myInput" type="text" placeholder="Search.." ></div>
             <div class="col-sm-2">
            <div class="card-tools">

              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
             </div>
          </div>
        </div>
        <div class="card-body p-0">
        <table class="table table-striped ">
        <thead>
        <thead>
        <tr> 
        <th style="width:10%;"></th>
        <th style="width:25%;">Name</th>
        <th style="width:25%;">Slug</th>
        <th style="width:20%;">Status </th>
        <th style="width:20%;">Action</th>
        </tr>
        </thead>
        <tbody id="myTable">
        @forelse ($categories as $category)
            <!-- Modal edit start -->
            <div class="modal fade" id="editmodal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit <b class="text-capitalize">{{$category->name}}</b> Main Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form method="post" action="{{ route('maincategoryupdate') }}" enctype="multipart/form-data">
            <div class="modal-body">

            @csrf
            <div class="form-group">
            <label for="inputEstimatedBudget"> Name</label>
            <input type="hidden" name="cid"  value="{{$category->id}}">
            <input   class="form-control text-capitalize main" placeholder=" Main Category Name" name="name" type="text" value="{{$category->name}}">
             <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
            </div>
            <div class="form-group">
            <label for="inputEstimatedBudget"> Slug</label>
            <input   class="form-control text-capitalize slug" placeholder=" Main Category Slug" name="slug" type="text" value="{{$category->slug}}">
             <span style="color:orange"> @error('slug') {{ $message }}@enderror </span>
            </div>
            <div class="form-group">
            <label for="inputEstimatedBudget"> Image</label>
            <input   class="form-control text-capitalize imageold" placeholder="Image" name="oldimage" type="hidden" value="{{$category->image}}">
            <input  class="form-control text-capitalize image" placeholder="Image" name="image" type="file">
         
             <span style="color:orange"> @error('image') {{ $message }}@enderror </span>
            </div>

            </div>
            <div class="modal-footer">
            <a href=""><button type="button" class="btn btn-secondary">Cancel</button></a>
            <button type="submit" class="btn btn-primary">Update Main Category</button>
            </div>
            </form>
            </div>
            </div>
            </div>
            <!-- Modal Edit end -->

                <!-- Modal Inactivation Start -->
                <div class="modal fade" id="inactivation{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Do you want to Inactivate&nbsp;
                <b class="text-capitalize">{{$category->name}}</b> Main Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <form method="post" action="{{ route('assignmaincategory') }}">
                @csrf
                <div class="modal-body">
                <div class="form-group">
                <h6> <b>Note:</b> Inactivation method generally inactivate all Sub,Sub-Child Categories But if you don't want to un-arrange these Sub,Sub-Child categories. <br />You can assign new Parent category to <b>{{$category->name}}</b> Sub,Sub-Child Categories. <br />Re-assign option needs your action below :  </h6>
                </select>
                </div>
                <div class="form-group">
                <label for="inputEstimatedBudget">Choose this ?</label>
                <select class="form-control custom-select" id="chooseoption{{$category->id}}">
                <option value="">-- Select Choice --</option>
                <option value="1">Yes, I want to assign new categories</option>
                <option value="0">No, I don't want to assign any new categories</option>
                </select>
                </div>

                <div class="form-group assign" style="display:none;">
                <input type="hidden" name="cid1"  value="{{$category->id}}">
                <select class="form-control custom-select" name="maincategory">
                <option value="">-- Select Assign  Category --</option>
                @forelse ($categories as $categorydisplay)
                <option  value="{{$categorydisplay->id}}" 
                class="text-capitalize" @if ( old("maincategory") == $categorydisplay->id) selected="selected" @endif  @if ( $categorydisplay->id == $category->id or $categorydisplay->status == 0) hidden @endif>{{ $categorydisplay->name }}</option>
                @empty
                <option value="">No Main Category Found</option>
                @endforelse
                </select>
                <span style="color:orange"> @error('maincategory') {{ $message }}@enderror </span>
                </div>

                </div>
                <div class="modal-footer assign" style="display: none;">
                <a href=""><button type="button" class="btn btn-secondary">Cancel</button></a>
                <button type="submit" class="btn btn-primary">Assign New Parent Category</button>
                </div>

                <div class="modal-footer noassign">
                <a href=""><button type="button" class="btn btn-secondary">Cancel</button></a>
                <a href="inactivemaincategory/{{$category->id}}">
                <button type="button" class="btn btn-primary">Inactivate all</button></a>
                </div> 
                </div>
                <script>  
                $(document).ready(function(){
                $('#chooseoption{{$category->id}}').on('change', function() {
                if(this.value == 1)
                {
                $('.assign').show();
                $('.noassign').hide();

                }
                else if(this.value ==0)
                {
                $('.assign').hide();
                $('.noassign').show();
                }
                });
                });
                </script>
                </form>
                </div>
                </div>
                </div>
                <!-- Modal Inactivation end -->
                  <tr>
                    <td> {{ $loop->index + 1 }}</td>
                    <td class="text-capitalize">{{ $category -> name }} 
                      &nbsp;
                       @if( $category -> child == 1)
                      <i class="fas fa-child text-success" data-toggle="tooltip" data-placement="top" title="Childs Found"></i>
                       @elseif( $category -> child == 0)
                          <i class="fas fa-child text-danger" data-toggle="tooltip" data-placement="top" title="No Childs"></i>
                       @endif
                      <br/><small>Created {{date('d-m-Y H:i A', strtotime($category->created_at))}}</small></td>
                    <td class="text-capitalize">
                      {{ $category -> slug }} 
                     </td>
                    
                    <td>
                      @if( $category -> status == 1)

                           @if( $category -> child == 1)
                             <i data-toggle="modal" data-target="#inactivation{{$category->id}}" class="fas fa-toggle-on text-success fa-2x" ></i>
                           @elseif( $category -> child == 0)
                             <a href="inactivemaincategory/{{$category->id}}" ><i data-toggle="tooltip" data-placement="left" title="Inactivate When child not found" class="fas fa-toggle-on text-success fa-2x" ></i></a>
                           @endif
                  
                      @elseif( $category -> status == 0)
                      <a href="activemaincategory/{{$category->id}}"><i data-toggle="tooltip" data-placement="left" title="Activate Main Category" class="fas fa-toggle-off text-danger fa-2x"></i></a>
                      @else
                      @endif
                    </td>
                    <td>
                      <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#editmodal{{$category->id}}"><i class="fa fa-edit"></i></button>
                      
                      <a href="deletemaincategory/{{$category->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this Driver?');"><i class="fa fa-trash"></i></a>

                    </td>
                  </tr>
                  @empty
                  <tr><td colspan="4"><h5 class="text-center"> No Main Categories Found</h5></td></tr>
  
                  @endforelse

              </tbody>
             
          </table>
          <br />
          <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $categories->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    $(document ).ready(function() {
    var $src = $('.main'),
    $dst = $('.slug');
    $src.on('input', function () {
    $dst.val($src.val());
    });
    });


    
  </script>

@endsection
