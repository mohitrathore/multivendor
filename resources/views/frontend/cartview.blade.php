@extends('frontend.layouts.app')
 
@section('content')  
<section class="pt-3 pb-3 page-info section-padding border-bottom bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<a href="/"><strong><span class="mdi mdi-home"></span> Home</strong></a> <span class="mdi mdi-chevron-right"></span> <a href="">Cart</a> 
</div>
</div>
</div>
</section>
<section class="cart-page section-padding">
<div class="container">
<div class="row">
<div class="col-md-12">

<div class="card mt-2">
<h5 class="card-header"><span class="text-secondary float-right">({{$CartCount}} item)</span></h5>
@if($CartCount > 0)

<div class="card-body pt-0 pr-0 pl-0 pb-0 cartrow">
@foreach($products as $product)
<div class="cart-list-product">

<img class="img-fluid" src="/product_images/{{$product->coverimage}}" alt="">
<span class="badge badge-success">{{$product->discount}}% OFF</span>
<h5>
	<a href="#">{{$product->name}}</a>

	<style>
	.center{
	width: 150px;
	margin: 40px auto;

	}
	</style>
	
</h5>
<!-- <h6><strong><span class="mdi mdi-approval"></span> Available in</strong> - 500 gm</h6> -->
@foreach($carts as $cart)
@if($cart->productid == $product->id)
<?php $total = $cart->sum('total_sum'); ?>
<?php $totalsaving = $cart->sum('total_saving'); ?>


<a class="float-right remove-cart" href="{{route('shop.cartdelete',['pid' => $product->id, 'vid'=>$cart->price_varient_id])}}"><i class="mdi mdi-close"></i></a>
<p class="offer-price mt-1">
	<strong><span class="mdi mdi-approval text-danger"></span> </strong>{{$cart->size}}
 <i class="fas fa-rupee-sign fa-sm"></i>
 <b class="productprice{{$cart->price_varient_id}}">{{$cart->saleprice}}</b>
 <i class="mdi mdi-tag-outline"></i> <span class="regular-price">
 	<i class="fas fa-rupee-sign fa-sm"></i>
 <b class="productprice{{$cart->price_varient_id}}">{{$cart->price}}</b>
</span></p>

<div class="input-group" style="width:12%;float:right;">
          <span class="input-group-btn">
              <button type="button" class="btn btn-danger btn-number btn-sm minus{{$cart->price_varient_id}}">
                <span class="fa fa-minus"></span>
              </button>
          </span>
          &nbsp;
          <input type="text" name="quant" class="form-control count{{$cart->price_varient_id}}" value=" {{$cart->quantity}}" style="height:27px" disabled>
            &nbsp;
          <span class="input-group-btn">
              <button type="button" class="btn btn-success btn-number btn-sm plus{{$cart->price_varient_id}}">
                  <span class="fa fa-plus"></span>
              </button>
          </span>
      </div>
      <script>
$(document).ready(function(){

	$(document).on('click','.plus{{$cart->price_varient_id}}',function(){
     var pid={!! $product->id !!};
	 var vid={!! $cart->price_varient_id !!};
     

		var plusval=parseInt($('.count{{$cart->price_varient_id}}').val()) + 1 ;
		$('.count{{$cart->price_varient_id}}').val(plusval);
		
		$.ajaxSetup({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
		});

		$.ajax({
		url:'<?php echo route('shop.cartupdate') ?>',
		type:'post',
		data: {pid:pid,vid:vid,quant:plusval,task:1},
		success:function(data) {
           //alert(data);
       if(data[0] == 0){
       	var name=data[1];
       $('.plus{{$cart->price_varient_id}}').hide();
       var plusval=parseInt($('.count{{$cart->price_varient_id}}').val()) - 1 ;
		$('.count{{$cart->price_varient_id}}').val(plusval);
          toastr.error(name +" item quantity limit exceed")
       }

       if(data[0] == 1){
       	$('.count{{$cart->price_varient_id}}').val(data[1]);
		$('.totalvaluesavingshow').html(data[3]);
		$('.totalvalue').html(data[2]);
		$('.cart-sidebar-footer').attr('checkout',data[2]);
		  $('.couponstatus').addClass('d-none'); 
		  $('.couponcodesection').fadeIn(); 
		  $('.couponcode_input').val('');

       //toastr.success("Quantity added")
       }
		},
		error:function () {
		console.log('error');
		}
		});

		

	});
	//product value plus 
		$(document).on('click','.minus{{$cart->price_varient_id}}',function(){  
		     
                var minusval=parseInt($('.count{{$cart->price_varient_id}}').val()) - 1 ;
                var pid={!! $product->id !!};
				var vid={!! $cart->price_varient_id !!};

				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});

				$.ajax({
				url:'<?php echo route('shop.cartupdate') ?>',
				type:'post',
				data: {pid:pid,vid:vid,quant:minusval,task:0},
				success:function(data) {
				//alert(data);
					if(data[0] == 0){
					var name=data[1];

					toastr.success(name +" removed from your cart")
					location.reload();
					}
					if(data[0] == 1){
						$('.count{{$cart->price_varient_id}}').val(data[1]);
						$('.plus{{$cart->price_varient_id}}').show();
						$('.totalvaluesavingshow').html(data[3]);
						$('.totalvalue').html(data[2]);
						$('.cart-sidebar-footer').attr('checkout',data[2]);
					    $('.couponstatus').addClass('d-none'); 
					    $('.couponcodesection').fadeIn();
                		 $('.couponcode_input').val('');

					}
				},
				error:function () {
				console.log('error');
				}
				});
			
			

		});
});
</script>
<div>



</div>
@endif
@endforeach

 


</div>
@endforeach

</div>
<div class="card-footer cart-sidebar-footer" checkout="{{$total}}">
<div class="cart-store-details">
<p>Sub Total 
	<strong class="float-right "> 
		<i class="fas fa-rupee-sign fa-sm"></i> 
		<b class="totalvalue">
             {{$total}}
         </b>
		
	</strong>
</p>
<!-- Total Amount  -->
<p>Delivery Charges 
	<strong class="float-right text-danger">+ 
		<i class="fas fa-rupee-sign fa-sm"></i> 
	29.69
</strong>
</p>
<!-- Delivery Charge -->
<h6>Your total savings <strong class="float-right text-danger"><i class="fas fa-rupee-sign fa-sm"></i> 
<b class="totalvaluesavingshow" checkout_dis="{{$totalsaving}}">{{$totalsaving}}</b> </strong>
</h6>
<!-- Total saving -->

<div class="form-inline float-right couponcodesection" style="height:40px;"> 
<input type="text" placeholder="Coupon Code" class="form-control border-form-control form-control-sm couponcode_input"> 
&nbsp;
<button class="btn btn-success float-left btn-sm cartcoupon" style="padding:4px;"  type="submit">Apply</button>
</div>

<!-- Coupon Code -->

</div>
<br />
	

<!-- Coupon code -->
<script>
$(document).on('click','.cartcoupon',function(){  
		     
               /* unYs1T*/
				var totalamount=$('.cart-sidebar-footer').attr('checkout');;
				var couponcode=$('.couponcode_input').val();
				var savingamount={!! $totalsaving !!};;
				/*Today Date*/
				var today = new Date();
				var dd = String(today.getDate()).padStart(2, '0');
				var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
				var yyyy = today.getFullYear();

				today =yyyy + '-' + mm + '-' + dd ;
				
				
				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});

				$.ajax({
				url:'<?php echo route('couponcodecheck') ?>',
				type:'post',
				data: {code:couponcode},
				success:function(data) {
				
				if(data[0] == 0){
                toastr.error(data[1]);
				}
				if(data[0] == 1){
                 var discountamount=data[1]['amount'];
                 var discounttype=data[1]['type'];
                 var minimum=data[1]['minimum_value'];
                 var expiry=data[1]['expirydate'];
                 if(today >= expiry)
                 {
                   toastr.error("This coupon code is expired !")
                 }
                 else
                 {
					if(totalamount > minimum)
					{
					if(discounttype == 'flat')
					{  
					var coupondiscount_totalamount=totalamount - discountamount;
					var saving=discountamount + savingamount;
					$('.totalvaluesavingshow').html(saving);
					$('.totalvalue').html(coupondiscount_totalamount);
					$('.couponstatus').removeClass('d-none');
					$('.cart-sidebar-footer').attr('checkout',coupondiscount_totalamount);
                     $('.couponcodesection').fadeOut();
					}
					if(discounttype == 'percentage')
					{
					var coupondiscount_totalamount=totalamount*discountamount/100;
					var saving=discountamount + savingamount;
					$('.totalvaluesavingshow').html(saving);
					$('.totalvalue').html(coupondiscount_totalamount);
					$('.couponstatus').removeClass('d-none');
					$('.cart-sidebar-footer').attr('checkout',coupondiscount_totalamount);  
					$('.couponcodesection').fadeOut();

					}	
					}
					else
					{
					toastr.error("This coupon code need your minimum value greater then"
					+ '     '+'<i class="fas fa-rupee-sign fa-sm"></i>'+'   '+data[1]['minimum_value'])
					}

                 }
                 
                 
				}
				},
				error:function () {
				console.log('error');

				}
				});
			
			

		});
</script>
<!-- Checkout start -->
<div>

<button class="btn btn-secondary btn-lg btn-block text-left checkout" type="button"><span class="float-left"><i class="mdi mdi-cart-outline"></i> Proceed to Checkout </span>

<span class="float-right">
<b class="mr-2 d-none text-white couponstatus"> ( Coupon code applied &nbsp;<span class="mdi mdi-approval"></span> )  </b>
	<strong><i class="fas fa-rupee-sign fa-sm"></i> 
<b class="totalvalue">{{$total}}</b></strong> <span class="mdi mdi-chevron-right"></span></span></button>
<br>
<b class="float-right checkout_message d-none"> Please Wait We redirect you to the checkout page &nbsp;<i class="fas fa-spinner fa-2x text-success fa-pulse"></i></b>
</div>
</div>

@else
<p style="font: 22px ProximaNova-Semibold !important;color:#4a4a4a; margin: 56px auto 10px;">Your Cart is Empty.</p>
<br />
  <h6 style="color: #8f8f8f;text-align:center;"> 
  <a class="btn btn-info" href="{{url('/')}}">
            <i class="fa fa-arrow-left mr-2"></i>Return Back to Shop Page
          </a>
  </h6>
  <br />
</div>
@endif
</div>
</div>
</div>
</div>
</section>
<script>

$(document).on('click','.checkout',function(){  

var checkout=$('.cart-sidebar-footer').attr('checkout');

var checkout_disc=$('.totalvaluesavingshow').attr('checkout_dis');
  
				$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});

				$.ajax({
				url:'<?php echo route('gotocheckoutfromcart') ?>',
				type:'post',
				data: {checkout:checkout,checkout_dis:checkout_disc},
				success:function(data) {

					if(data[0] == 0){

					$(".checkout_message").removeClass('d-none');
					setInterval(function () {
					
					window.location.href="<?php echo route('checkout')?>";
					}, 3000); // in milliseconds

					}
				},
				error:function () {
				console.log('error');

				}
				});
});
</script>
@endsection