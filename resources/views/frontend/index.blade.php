@extends('frontend.layouts.app')
 
@section('content') 
<style>
.offer-price {
    line-height: 25px;
</style>

<section class="top-category section-padding">
<div class="container">
<div class="owl-carousel owl-carousel-category">

@foreach($maincategories as $maincategory)
<div class="item">
<div class="category-item">
<a href="shop/{{$maincategory->slug}}/{{$maincategory->id}}">
<img class="img-fluid" src="categories_image/{{$maincategory->image}}" alt="{{$maincategory->image}}">
<h6>{{$maincategory->name}}</h6>
</a>
</div>
</div>
@endforeach
</div>
</div>
</section>
<section class="carousel-slider-main text-center border-top border-bottom bg-white">
<div class="owl-carousel owl-carousel-slider">
@foreach($bannerstop as $banner)

<div class="item">
<a href="#"><img class="img-fluid" src="dist/img/ad_banner/{{$banner->image}}" alt="{{$banner->name}}"></a>
</div>

@endforeach

</div>
</section>
<!-- Top Savers Start-->

<section class="product-items-slider section-padding">
<div class="container">
<div class="section-header">
<h5 class="heading-design-h5">Top Savers Today <span class="badge badge-primary">20% OFF</span>
<a class="float-right text-secondary" href="shop.html">View All</a>
</h5>
</div>
<div class="owl-carousel owl-carousel-featured">
@foreach($products as $product)
@if($product->criteria == "top-saver")
<div class="item">
<div class="product">
<a href="{{route('shop.singleproduct',['product' => $product->name, 'productid' =>$product->id])}}">
<div class="product-header">
<span class="badge badge-success">{{$product->discount}} % OFF</span>
<img class="img-fluid" src="product_images/{{$product->coverimage}}" alt="">
@if($product->type == "veg")
<span class="veg text-success mdi mdi-circle"></span>
@elseif($product->type == "non-veg")
<span class="non-veg text-danger mdi mdi-circle"></span>
@endif
</div> 
</a>
<div class="product-body">
<h6>
<label for="varient"><span class="mdi mdi-approval"></span> Available in</label>
    <select class="form-control"  style="height:calc(1.5em + .45rem + 2px);height:" name="varient" id="varient{{$product->id}}">
      @foreach($varients as $varient)
       @if($varient->productid == $product->id)
      <option value="{{$varient->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>  -  ₹ {{$varient->price}}</option>
      @endif
      @endforeach
    </select>
</h6>  
<h5 style="white-space: nowrap;text-overflow: ellipsis;width: 100%;display: block;overflow: hidden">{{$product->name}} ..</h5>
</div>  
<br />


<div class="product-footer">

@guest
<a href="{{url('login')}}" class="btn btn-secondary btn-sm float-right addtocart text-white" cart="{{$product->id}}"><i class="mdi mdi-account-key"></i> Login</a>

<p class="offer-price mb-0">
 @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshow{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i>
</p>

@else
<a type="button" class="btn btn-secondary btn-sm float-right addtocart text-white" cart="{{$product->id}}"><i class="mdi mdi-cart-outline"></i> Add</a>
<p class="offer-price mb-0">
     @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshow{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i><!-- <br><span class="regular-price">$800.99</span> --></p>
  @endguest
<!-- add to cart and login button -->
</div>

</div>
</div>
<script>
$('#addtocart{{$product->id}}').on('click', function() {
  var price=$('#varient{{$product->id}}').value;
  alert(price);
  var text=$("#varient{{$product->id}} option:selected").text();
  $('#varientshow{{$product->id}}').html(text);
});

$('#varient{{$product->id}}').on('change', function() {
  var val=this.value;
  var text=$("#varient{{$product->id}} option:selected").text();
  $('#varientshow{{$product->id}}').html(text);
});
</script>
@endif
@endforeach
</div>
</div>
</section>
<!-- Top Savers End -->

<section class="offer-product">
<div class="container">
<div class="row no-gutters">
@foreach($bannersmid as $bannerm)

<div class="col-md-6">
<a href="#"><img class="img-fluid" src="dist/img/ad_banner/{{$bannerm->image}}" alt=""></a>
</div>

@endforeach

</div>
</div>
</section>

<!-- Best Offer Start-->
<section class="product-items-slider section-padding">
<div class="container">
<div class="section-header">
<h5 class="heading-design-h5">Best Offer View <span class="badge badge-info">20% OFF</span>
<a class="float-right text-secondary" href="shop.html">View All</a>
</h5>
</div>
<div class="owl-carousel owl-carousel-featured">
@foreach($products as $product)
@if($product->criteria == "best-offer")
<div class="item">
<div class="product"> 
<a href="{{route('shop.singleproduct',['product' => $product->name, 'productid' =>$product->id])}}">
<div class="product-header">
<span class="badge badge-success">{{$product->discount}} % OFF</span>
<img class="img-fluid" src="product_images/{{$product->coverimage}}" alt="">
@if($product->type == "veg")
<span class="veg text-success mdi mdi-circle"></span>
@elseif($product->type == "non-veg")
<span class="non-veg text-danger mdi mdi-circle"></span>
@endif
</div>
</a>
<div class="product-body">
<h6>  
<label for="varient"><span class="mdi mdi-approval"></span> Available in</label>
    <select class="form-control"  style="height:calc(1.5em + .45rem + 2px);height:" name="varient" id="varient{{$product->id}}">

    	@foreach($varients as $varient)
    	 @if($varient->productid == $product->id)

      <option value="{{$varient->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>  -  ₹ {{$varient->price}}</option>
      @endif
      @endforeach
    </select>
</h6>
<h5 style="white-space: nowrap;text-overflow: ellipsis;width: 100%;display: block;overflow: hidden">{{$product->name}} ..</h5>
</div>	
<br />
<div class="product-footer">
@guest
<a href="{{url('login')}}" class="btn btn-secondary btn-sm float-right addtocart text-white" cart="{{$product->id}}"><i class="mdi mdi-account-key"></i> Login</a>

<p class="offer-price mb-0">
 @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshow{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i>
</p>

@else
<button type="button" class="btn btn-secondary btn-sm float-right addtocart text-white" cart="{{$product->id}}"><i class="mdi mdi-cart-outline"></i> Add</button>
<p class="offer-price mb-0">
     @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshow{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i><!-- <br><span class="regular-price">$800.99</span> --></p>
  @endguest
</div>

</div>
</div>
<script>
$('#varient{{$product->id}}').on('change', function() {
  var val=this.value;
  var text=$("#varient{{$product->id}} option:selected").text();
  $('#varientshow{{$product->id}}').html(text);
});
</script>
@endif
@endforeach
</div>
</div>
</section>
<!-- Best Offer End -->


@endsection