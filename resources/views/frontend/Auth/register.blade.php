
@extends('frontend.layouts.app')
 
@section('content') 



<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-9 mx-auto">
<div class="row no-gutters">
<div class="col-md-12">
<div class="card account-left">
<div class="login-modal">
<div class="row">
<div class="col-lg-6 pad-right-0">
<div class="login-modal-left">
	        
<img class="rounded img-fluid" style="margin-top:30%;width:100%;"src="{{asset('dist/img/trolly-icon.jpg')}}" alt="Card image cap">
</div>
</div>
<div class="col-lg-6 pad-left-0">

<div class="login-modal-right">

<div class="tab-content">

		
<!-- Registration Section Start  -->
	
		
		<form method="post" action="{{route('register')}}">
			<!-- Session Messages -->
			@if(session()->has('success'))
				<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>    
				{!! session()->get('success')!!}
				</div>
			@endif
			@if(session()->has('error'))
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>    
				{!! session()->get('error')!!}
				</div>
			</script>

			@endif 
		@csrf
		<h5 class="heading-design-h5">Register Now!</h5>
		<fieldset class="form-group">
		<label>Name</label>
		<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" placeholder="Enter your name"  autofocus>
		@error('name')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group">
		<label>Email</label>
		<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email"   placeholder="Enter your email">

		@error('email')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>

		<fieldset class="form-group">
		<label>Mobile Number</label>
		<input id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}"  autocomplete="mobile"  placeholder="Enter your number" required>

		@error('mobile')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group">
		<label>Choose Gender </label>
		<select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender" autocomplete="gender"  >
		<option >-- Select Gender --</option>
		<option value="male" @if (old('gender') == 'male') selected="selected" @endif>Male</option>
		<option value="female"  @if (old('gender') == 'female') selected="selected" @endif>Female</option>
		<option value="other"  @if (old('gender') == 'other') selected="selected" @endif>Other</option>

		</select>  
		@error('gender')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group">
		<label>Password</label>
		<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="Enter password" >

		@error('password')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group">
		<label>Confirm Password</label>
		<input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" placeholder="Enter confirm password" >

		</fieldset>
		<input type="hidden" class="form-control" name="role" value="user">
		<div class="custom-control custom-checkbox">
		<input type="checkbox" class="custom-control-input" id="customCheck2" >
		<label class="custom-control-label" for="customCheck2">I Agree with <a href="#">Term and Conditions</a></label>
		</div>
		<fieldset class="form-group">
		<button type="submit" class="btn btn-lg btn-secondary btn-block">Create Your Account</button>
		</fieldset>
		
 </form>
		
   

<!-- Registration Section End-->

</div>
<div class="clearfix"></div>

<div class="clearfix"></div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</section>

@endsection