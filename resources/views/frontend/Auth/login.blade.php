@extends('frontend.layouts.app')
 
@section('content') 



<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-9 mx-auto">
<div class="row no-gutters">
<div class="col-md-12">
<div class="card account-left">
      <div class="login-modal">
<div class="row">
<div class="col-lg-6 pad-right-0">
<div class="login-modal-left">

			

	<img class="rounded img-fluid" style="width:100%;"src="{{asset('dist/img/trolly-icon.jpg')}}" alt="Card image cap">
</div>
</div>
<div class="col-lg-6 pad-left-0">

<div class="login-modal-right">

<div class="tab-content">
	<!-- Login Section Start -->
	<form method="post" action="{{ route('login') }}">
	 @csrf	
		<div class="tab-pane active" id="login" role="tabpanel">
			
				<!-- Session Messages -->
				@if(session()->has('success'))
				<script>
				$( document ).ready(function() {
				toastr.success("{!! session()->get('success')!!}")
				});
				</script>
				@endif
				@if(session()->has('error'))
				<script>
				$( document ).ready(function() {
				toastr.error("{!! session()->get('error')!!}")
				});
				</script>

				@endif 
                
				@if ($errors->any())
				@foreach ($errors->all() as $error)
				<script>
				$( document ).ready(function() {
				toastr.error("{!! $error !!}")
				});
				</script>
				@endforeach

        @endif   
		<h5 class="heading-design-h5">Login to your account</h5>
		<fieldset class="form-group">

		<label>Enter Email</label>
		<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="Enter Email" autofocus>

		@error('email')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group">
		<label>Enter Password</label>
		<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  placeholder="Enter Password" autocomplete="current-password">

		@error('password')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group otp">
			<div class="row">
            <div class="col-lg-6 sm-6"><label>Enter OTP</label>
		<input id="otp" type="number" class="form-control @error('otp') is-invalid @enderror" name="otp"  placeholder="Enter OTP" autocomplete="otp"> </div>
            <div class="col-lg-6 sm-6"> <button type="button" class="btn btn-sm btn-info btn-block" style="margin-top:2rem;" onclick="sendOtp()">Send OTP </button></div>
			</div>
		
		@error('otp')
		<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
		</span>
		@enderror
		</fieldset>
		<fieldset class="form-group">
		<button type="submit" class="btn btn-lg btn-secondary btn-block otp">Login to your Account</button>
		</fieldset>
		<fieldset class="form-group send">
		<button type="button" class="btn btn-lg btn-secondary btn-block" onclick="sendOtp()">Login to your Account</button>
		</fieldset>
		<!-- <div class="login-with-sites text-center">
		<p>or Login with your social profile:</p>
		<button class="btn-facebook login-icons btn-lg"><i class="mdi mdi-facebook"></i> Facebook</button>
		<button class="btn-google login-icons btn-lg"><i class="mdi mdi-google"></i> Google</button>
		<button class="btn-twitter login-icons btn-lg"><i class="mdi mdi-twitter"></i> Twitter</button>
		</div> -->
		<div class="custom-control custom-checkbox" id="pp">
		<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

		<label class="form-check-label" for="remember">
		{{ __('Remember Me') }}
		</label>
		@if (Route::has('password.request'))
		<a class="btn btn-link " href="{{ route('password.request') }}">
		{{ __('Forgot Your Password?') }}
		</a>
		@endif
		</div>

		</div>
		<input id="rolebased" type="hidden" class="form-control" name="role">
	</form>
	<!-- Login Section Ends -->
	

</div>
<div class="clearfix"></div>

<div class="clearfix"></div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</section>
			<script>
			$('.otp').addClass("d-none");
			function sendOtp() {

				 $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


			var email = $('#email').val();
			var password = $('#password').val();

			if(email==" " ||  email=="" || email==null)
			{
			$("#email").addClass("is-invalid");
			toastr.error("Please provide a valid email address")

			}else if(password==" " ||  password=="" || password==null)
			{
			$("#email").addClass("is-valid");
			$("#email").removeClass("is-invalid");
			$("#password").addClass("is-invalid");
			toastr.error("Please provide a password")
			}
			else{
			$("#email").addClass("is-valid");
			$("#password").addClass("is-valid");
			$("#email").removeClass("is-invalid");
			$("#password").removeClass("is-invalid");

			$.ajax({
			url:'<?php echo route('sendotp') ?>',
			type:'post',
			data: {'email': $('#email').val()},
			success:function(data) {
			//alert(data);
			if(data[0] != 0){
			$('.otp').removeClass("d-none");
			$('.send').hide();
			$('#rolebased').val(data[1]['role']);
			toastr.success("OTP send to your registered Mobile Number !")
			}else{
			toastr.error("No user found !")
			$('#rolebased').val('');
			}

			},
			error:function () {
			console.log('error');
			}
			});
			}



			}
			</script>
@endsection