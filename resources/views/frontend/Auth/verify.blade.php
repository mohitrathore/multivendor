@extends('frontend.layouts.app')
 
@section('content') 



<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-9 mx-auto">
<div class="row no-gutters">
<div class="col-md-12">
<div class="card account-left">
      <div class="login-modal">
<div class="row">
<div class="col-lg-6 pad-right-0">
<div class="login-modal-left">

			

	<img class="rounded img-fluid" style="width:100%;"src="{{asset('dist/img/trolly-icon.jpg')}}" alt="Card image cap">
</div>
</div>
<div class="col-lg-6 pad-left-0">

<div class="login-modal-right">

<div class="tab-content">

	
		<div class="tab-pane active" id="login" role="tabpanel">
			
				<!-- Session Messages -->
				@if(session()->has('success'))
				<script>
				$( document ).ready(function() {
				toastr.success("{!! session()->get('success')!!}")
				});
				</script>
				@endif
				@if(session()->has('error'))
				<script>
				$( document ).ready(function() {
				toastr.error("{!! session()->get('error')!!}")
				});
				</script>

				@endif 
				@if (session('resent'))
                       <script>
				$( document ).ready(function() {
				toastr.success("A fresh verification link has been sent to your email address.")
				});
				</script>
                    @endif
                
				@if ($errors->any())
				@foreach ($errors->all() as $error)
				<script>
				$( document ).ready(function() {
				toastr.error("{!! $error !!}")
				});
				</script>
				@endforeach

        @endif   
		<h5 class="heading-design-h5">{{ __('Please Verify Your Email Address') }}</h5>
		
		<fieldset class="form-group">
		 {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}
                    <br /><br />
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-lg btn-secondary btn-block">{{ __('Click Here to Re-send Verification Email') }}</button>.
                    </form>
		</fieldset>
		</div>
		<input id="rolebased" type="hidden" class="form-control" name="role">

	

</div>
<div class="clearfix"></div>

<div class="clearfix"></div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</section>
@endsection