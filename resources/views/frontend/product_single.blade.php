@extends('frontend.layouts.app')
 
@section('content') 
<section class="pt-3 pb-3 page-info section-padding border-bottom bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<a href="#"><strong><span class="mdi mdi-home"></span> Home</strong></a> <span class="mdi mdi-chevron-right"></span> <a href="#">Fruits & Vegetables</a> <span class="mdi mdi-chevron-right"></span> <a href="#">Fruits</a>
</div>
</div>
</div>
</section>
<section class="shop-single section-padding pt-3">
<div class="container">
<div class="row">
<div class="col-md-6">
<div class="shop-detail-left">
<div class="shop-detail-slider">
<div class="favourite-icon">
<a class="fav-btn" title="" data-placement="bottom" data-toggle="tooltip" href="#" data-original-title="59% OFF"><i class="mdi mdi-tag-outline"></i></a>
</div>
<div id="sync1" class="owl-carousel">

@foreach(json_decode($products->images) as $images)
  <div class="item"><img alt="" src="../../../product_images/{{$images}}" class="img-fluid img-center"></div>
@endforeach
</div>
<div id="sync2" class="owl-carousel">
<div class="item"><img alt="" src="../img/item/1.jpg" class="img-fluid img-center"></div>
<div class="item"><img alt="" src="../img/item/2.jpg" class="img-fluid img-center"></div>
<div class="item"><img alt="" src="../img/item/3.jpg" class="img-fluid img-center"></div>
<div class="item"><img alt="" src="../img/item/4.jpg" class="img-fluid img-center"></div>
<div class="item"><img alt="" src="../img/item/5.jpg" class="img-fluid img-center"></div>
<div class="item"><img alt="" src="../img/item/6.jpg" class="img-fluid img-center"></div>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="shop-detail-right">
	<p class="offer-price float-right"><i class="mdi mdi-tag-outline"></i> MRP : 
	@foreach($varients as $varient)
       @if($varient->productid == $products->id)
         <b id="varientshow{{$products->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;&nbsp; - &nbsp;&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach

   </p>
<span class="badge badge-success">{{$products->discount}}% OFF</span>
<h2 class="text-capitalize" style="white-space:nowrap;text-overflow:ellipsis;width:100%;display: block;overflow: hidden">{{$products->name}}</h2>
<br />
<h6>
	<div class="form-group">
 <strong><span class="mdi mdi-approval"></span> Available in</strong> 
  <select class="form-control mt-2"  style="height:calc(1.5em + .45rem + 2px);height:" name="varient" id="varient{{$products->id}}">
      @foreach($varients as $varient)
       @if($varient->productid == $products->id)
      <option value="{{$varient->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>  -  ₹ {{$varient->price}}</option>
     
      @endif
    
      @endforeach
    </select>
</div>

</h6>

<!-- varient price change --> 
<script>
$('#varient{{$products->id}}').on('change', function() {
  var val=this.value;
  var text=$("#varient{{$products->id}} option:selected").text();
  $('#varientshow{{$products->id}}').html(text);
});
</script>
<!-- varient price change --> 
<a href="checkout.html"><button type="button" class="btn btn-secondary btn-lg"><i class="mdi mdi-cart-outline"></i> Add To Cart</button> </a>
<div class="short-description">
<h5>
Description
<p class="float-right">Availability: 
	@if($products->stoke !== 0)
        <span class="badge badge-success">
		In Stock
	</span>
		@elseif($products->stoke == 0)
       <span class="badge badge-danger">
		Out of Stock
	</span>
	   @endif
	
</p>
</h5>
<p>{{$products->description}}</p>
</div>
<div class="short-description mt-2">
<h5>
Disclaimer

</h5>
<p>{{$products->disclaimer}}</p>
</div>
<h6 class="mb-3 mt-4">Why shop from Grocery ?</h6>
<div class="row">
<div class="col-md-6">
<div class="feature-box">
<i class="mdi mdi-truck-fast"></i>
<h6 class="text-info">Free Delivery</h6>
<p>Lorem ipsum dolor...</p>
</div>
</div>
<div class="col-md-6">
<div class="feature-box">
<i class="mdi mdi-basket"></i>
<h6 class="text-info">100% Guarantee</h6>
<p>Rorem Ipsum Dolor sit...</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="product-items-slider section-padding bg-white border-top">
<div class="container">
<div class="section-header">
<h5 class="heading-design-h5">Best Offers View <span class="badge badge-primary">20% OFF</span>
<a class="float-right text-secondary" href="shop.html">View All</a>
</h5>
</div>
<div class="owl-carousel owl-carousel-featured">

@foreach($productbest_offer as $product)
@if($product->criteria == "best-offer")
<div class="item">
<div class="product">
<a href="#">
<div class="product-header">
<span class="badge badge-success">{{$product->discount}} % OFF</span>
<img class="img-fluid" src="../../../../../../product_images/{{$product->coverimage}}" alt="">
@if($product->type == "veg")
<span class="veg text-success mdi mdi-circle"></span>
@elseif($product->type == "non-veg")
<span class="non-veg text-danger mdi mdi-circle"></span>
@endif
</div>
</a>
<div class="product-body">
<h6>  
<label for="varient"><span class="mdi mdi-approval"></span> Available in</label>
    <select class="form-control"  style="height:calc(1.5em + .45rem + 2px);height:" name="varient" id="varientbest{{$product->id}}">

      @foreach($varients as $varient)
       @if($varient->productid == $product->id)

      <option value="{{$varient->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>  -  ₹ {{$varient->price}}</option>
      @endif
      @endforeach
    </select>
</h6>
<h5 class="text-capitalize"style="white-space: nowrap;text-overflow: ellipsis;width: 100%;display: block;overflow: hidden">{{$product->name}} ..</h5>
</div>  
<br />
<div class="product-footer">
<button type="button" class="btn btn-secondary btn-sm float-right"><i class="mdi mdi-cart-outline"></i> Add</button>
<p class="offer-price mb-0">
     @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshowbest{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i><!-- <br><span class="regular-price">$800.99</span> --></p>
</div>

</div>
</div>
<script>
$('#varientbest{{$product->id}}').on('change', function() {
  var val=this.value;
  var text=$("#varientbest{{$product->id}} option:selected").text();
  $('#varientshowbest{{$product->id}}').html(text);
});
</script>
@endif
@endforeach
</div>
</div>
</section>
@endsection