
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Grocery Ecommerce Content">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="author" content="Grocery">
<title>Grocery</title>

<link rel="icon" type="image/png" href="{{asset('dist/img/AdminLTELogo.png')}}">

<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>

<link  href="{{ asset('dist/icons/css/materialdesignicons.min.css') }}" media="all" rel="stylesheet" type="text/css">

<link href="{{asset('css/select2-bootstrap.css')}}" />
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
<link href="{{asset('css/osahan2.css')}}" rel="stylesheet">

<link rel="stylesheet" href="{{asset('dist/owl-carousel/owl.carousel.css')}}">

<link rel="stylesheet" href="{{asset('dist/owl-carousel/owl.theme.css')}}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

<div class="navbar-top bg-info pt-2 pb-2">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12 text-center">
<a href="shop.html" class="mb-0 text-white">
20% cashback for new users | Code: <strong><span class="text-light">OGOFERS13 <span class="mdi mdi-tag-faces"></span></span> </strong>
</a>
</div>
</div>
</div>
</div>
<nav class="navbar navbar-light navbar-expand-lg bg-dark bg-faded osahan-menu">
<div class="container-fluid">
<a class="navbar-brand" href="{{route('index.frontend')}}"> <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="logo" style="width:29%;"> </a>
<ul class="list-inline main-nav-right">
<li class="list-inline-item">
 <!-- Location Menu -->
 <a class=" dropdown-toggle location-top" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-map-marker-circle" aria-hidden="true"></i> <span id="InputLocation">
  @if(Cookie::has('LocationCity'))
  {{Cookie::get('LocationCity')}}
  @else
  <span> Your Location</span>
  <script>
  $(document).ready(function(){
   $(".locationMenu").show();
   $(".fa-spinner").hide();
   $("body").addClass('toggled');
   $(".cart-sidebar").css("position", "unset");
        var $win = $(window); // or $box parent container
        var $box = $(".locationMenu");
      
        
         $win.on("click.Bst", function(event){    
          if ( 
            $box.has(event.target).length == 0 //checks if descendants of $box was clicked
            &&
            !$box.is(event.target) //checks if the $box itself was clicked
          ){
            $(".locationMenu").show();
            var options = {};
            $( ".locationMenu" ).effect('bounce', options, 1000, callback );
            function callback() {
            setTimeout(function() {
         
            }, 1000 );
            }; //animation bounce
            
            return false;
          }
        });
});

  </script>
  @endif


</span></a>
 
<div class="dropdown-menu locationMenu" style="left: 10%;width:30%;height:auto;">
 
  <div class="card-body bg-dark text-white">
    @if(Cookie::has('LocationCity'))
    <script>
    $(document).ready(function(){
    $('.locationfound').show();
    $('.locationform').hide();
    });
    </script>
    @else
    <script>
     $(document).ready(function(){
        $('.locationfound').hide();
     $('.locationform').show();
    });
    </script>
    @endif
    <div class="location-body_top-container locationfound">
      <div style="display: flex;">
        <div>
          <i class="location-icon"></i>
        </div>
        <div style="margin-left: 10px;">
          <div class="delivery-location">Delivery Location</div>
          <div class="prev-address">
            <div class="ellipsis"> {{Cookie::get('LocationAddress')}}</div>
          </div>
        </div>
            <div style="display: flex; vertical-align: middle; align-items: center;"><div class="location-box change-button">
      <button type="button" class="btn btn-success btn-sm" 
      style="border:none;padding:10px;margin-left: 10px;float:right;" id="locationswitch"> Change </button>
    </div>
  </div>
</div>
</div>

  @if(session()->has('locationerror'))
  <script>
  swal("Sorry!", "{!! session()->get('locationerror')!!}", "error");
  </script>
  @endif

  <form method="post" enctype="multipart/form-data" action="{{route('index.location.update')}}" class="locationform" id="locationform">
    @csrf
  @if(Cookie::has('LocationCity'))
  <div class="form-group">
  <button type="button" class="close text-white backtoaddress" style="margin-top: -30px;" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
  </div>
  @endif
  <div class="form-group">
  <label for="inputName"> 
    <i class="mdi mdi-map-marker-circle" style="font-size: 25px;"aria-hidden="true"></i> Find Your Current Location  to Start Shopping </label>
  <input   class="form-control" placeholder="Location" id="searchInput" name="location" type="text" value="{{old('location')}}" autocomplete="nope">      
  </div>
  <div class="form-group">
  <button type="submit" class="btn btn-success btn-sm" style="border:none;padding:2%;margin-left: 0px;"> Continue </button>
  <span class="text-white" style="font-size:0.8rem;">- or -</span>
  <button type="button" class="btn btn-info btn-sm" style="border:none;padding:2%;margin-left: 0px;" onclick="getLocation()"> Detect Me </button>
  <i class="fas fa-spinner fa-pulse float-right"></i>
  <br /><br />
  <p id="LocationStatus" ></p>
  </div>
          <div id="address-map-container d-none"  >
          <div style="width: 100%; height: 100%;"  class="map" id="map" ></div>
          </div>
          <input type="hidden" name="latitude" id="lat"/>
          <input type="hidden" name="longitude" id="lng"/>
          <input type="hidden" name="city" id="city"/>
          <input type="hidden" name="state" id="state"/>
  </form>
  
  </div>


</div>
</li>
</ul>
<!-- location Menu -->
<button class="navbar-toggler navbar-toggler-white" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="navbar-collapse" id="navbarNavDropdown">
<div class="navbar-nav mr-auto mt-2 mt-lg-0 margin-auto top-categories-search-main">
<div class="top-categories-search">
<div class="input-group">
<span class="input-group-btn categories-dropdown">
<!-- <select class="form-control-select">
<option selected="selected">Your City</option>
<option value="0">New Delhi</option>
<option value="2">Bengaluru</option>
<option value="3">Hyderabad</option>
<option value="4">Kolkata</option>
</select> -->
</span>


<input class="form-control" placeholder="Search products in Your City" aria-label="Search products in Your City" type="text">
<span class="input-group-btn">
<button class="btn btn-secondary" type="button"><i class="mdi mdi-file-find"></i> Search</button>
</span>
</div>
</div>
</div>
<div class="my-2 my-lg-0">

<ul class="list-inline main-nav-right">
@guest

<li class="list-inline-item">
<a href="{{url('login')}}"  class="btn btn-link border-0"><i class="mdi mdi-account-key"></i>&nbsp; Login</a>
<a href="{{url('register')}}"  class="btn btn-link border-0"><i class="mdi mdi-account-plus"></i>&nbsp; Register</a>
</li>

@else

<li class="list-inline-item">
 <a class=" dropdown-toggle btn btn-link" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
@if(Auth::user()->profileImage !== null)

<img class="card-profile-img tecxt" src="../profile_images/{{Auth::user()->profileImage}}" style="width: 12%;height: 12%;">

@else
<i class="mdi mdi-account-circle"></i>
@endif
  &nbsp;My Account</a>
<div class="dropdown-menu" style="left: 80%;">
  @if(Auth::user()->role == 'user')
<a class="dropdown-item" href="{{route('profile.view')}}"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> My Profile</a>
<a class="dropdown-item" href="{{route('address.view')}}"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> My Address</a>
<a class="dropdown-item" href="{{route('order.index')}}"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Order List</a>
@elseif(Auth::user()->role == 'store')
<a class="dropdown-item" href="{{route('store.index')}}"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Dashboard Panel</a>
@elseif(Auth::user()->role == 'agent')
<a class="dropdown-item" href="{{route('agent.index')}}"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Dashboard Panel</a>
@elseif(Auth::user()->role == 'driver')
<a class="dropdown-item" href="{{route('driver.index')}}"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Dashboard Panel</a>
@endif
<!-- Log Out -->
  <a class="dropdown-item" href="{{ route('logout') }}" 
  onclick="event.preventDefault(); 
  document.getElementById('logout-form').submit();" 
  style="background: #095c91;color: white;"> {{ __('Logout') }} </a>

  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
  @csrf
  </form>

</div>
</li>
<li class="list-inline-item cart-btn">
<a href="{{route('shop.cart')}}"  class="btn btn-link border-none"><i class="mdi mdi-cart"></i> My Cart <small class="cart-value cartcountshow">{{$CartCount}}</small></a>
</li>
@endguest

</ul>
</div>
</div>
</div>
</nav>
@if (\Request::is('shop/*')) 
<nav class="navbar navbar-expand-lg navbar-light osahan-menu-2 pad-none-mobile">
<div class="container-fluid">
<div class="collapse navbar-collapse" id="navbarText">
<ul class="navbar-nav mr-auto mt-2 mt-lg-0 margin-auto">
<!-- <li class="nav-item">
<a class="nav-link shop" href="index.html"><span class="mdi mdi-store"></span> Super Store</a>
</li> -->
<li class="nav-item">
<a href="index.html" class="nav-link">Home</a>
</li>
<li class="nav-item">
<a href="about.html" class="nav-link">About Us</a>
</li>
<li class="nav-item">
<a class="nav-link" href="shop.html">Fruits & Vegetables</a>
</li>
<li class="nav-item">
<a class="nav-link" href="shop.html">Grocery & Staples</a>
</li>
<li class="nav-item dropdown">
<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Pages
</a>
<div class="dropdown-menu">
<a class="dropdown-item" href="shop.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Shop Grid</a>
<a class="dropdown-item" href="single.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Single Product</a>
<a class="dropdown-item" href="cart.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Shopping Cart</a>
<a class="dropdown-item" href="checkout.html"><i class="mdi mdi-chevron-right" aria-hidden="true"></i> Checkout</a>
</div>
</li>

</ul>
</div>
</div>
</nav>
@endif
<!-- Header End -->

 @yield('content')

 <!-- Footer Start -->

<section class="section-padding bg-white border-top">
<div class="container">
<div class="row">
<div class="col-lg-4 col-sm-6">
<div class="feature-box">
<i class="mdi mdi-truck-fast"></i>
<h6>Free & Next Day Delivery</h6>
<p>Lorem ipsum dolor sit amet, cons...</p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="feature-box">
<i class="mdi mdi-basket"></i>
<h6>100% Satisfaction Guarantee</h6>
<p>Rorem Ipsum Dolor sit amet, cons...</p>
</div>
</div>
<div class="col-lg-4 col-sm-6">
<div class="feature-box">
<i class="mdi mdi-tag-heart"></i>
<h6>Great Daily Deals Discount</h6>
<p>Sorem Ipsum Dolor sit amet, Cons...</p>
</div>
</div>
</div>
</div>
</section>

<section class="section-padding footer bg-white border-top">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3">
 <h4 class="mb-5 mt-0"><a class="logo" href="index.html"><img src="img/logo-footer2.png" alt="Groci"></a></h4>
<p class="mb-0"><a class="text-dark" href="#"><i class="mdi mdi-phone"></i> +61 525 240 310</a></p>
<p class="mb-0"><a class="text-dark" href="#"><i class="mdi mdi-cellphone-iphone"></i> 12345 67890, 56847-98562</a></p>
<p class="mb-0"><a class="text-success" href="#"><i class="mdi mdi-email"></i> <span class="__cf_email__" data-cfemail="2049414d4f534148414e60474d41494c0e434f4d">[email&#160;protected]</span></a></p>
<p class="mb-0"><a class="text-primary" href="#"><i class="mdi mdi-web"></i> www.askbootstrap.com</a></p>
</div>
<div class="col-lg-2 col-md-2">
<h6 class="mb-4">TOP CITIES </h6>
<ul>
<li><a href="#">New Delhi</a></li>
<li><a href="#">Bengaluru</a></li>
<li><a href="#">Hyderabad</a></li>
<li><a href="#">Kolkata</a></li>
<li><a href="#">Gurugram</a></li>
<ul>
</div>
<div class="col-lg-2 col-md-2">
<h6 class="mb-4">CATEGORIES</h6>
<ul>
<li><a href="#">Vegetables</a></li>
<li><a href="#">Grocery & Staples</a></li>
<li><a href="#">Breakfast & Dairy</a></li>
<li><a href="#">Soft Drinks</a></li>
<li><a href="#">Biscuits & Cookies</a></li>
<ul>
</div>
<div class="col-lg-2 col-md-2">
<h6 class="mb-4">ABOUT US</h6>
<ul>
<li><a href="#">Company Information</a></li>
<li><a href="#">Careers</a></li>
<li><a href="#">Store Location</a></li>
<li><a href="#">Affillate Program</a></li>
<li><a href="#">Copyright</a></li>
<ul>
</div>
<div class="col-lg-3 col-md-3">
<h6 class="mb-4">Download App</h6>
<div class="app">
<a href="#"><img src="../img/google.png" alt=""></a>
<a href="#"><img src="../img/apple.png" alt=""></a>
</div>
<h6 class="mb-3 mt-4">GET IN TOUCH</h6>
<div class="footer-social">
<a class="btn-facebook" href="#"><i class="mdi mdi-facebook"></i></a>
<a class="btn-twitter" href="#"><i class="mdi mdi-twitter"></i></a>
<a class="btn-instagram" href="#"><i class="mdi mdi-instagram"></i></a>
<a class="btn-whatsapp" href="#"><i class="mdi mdi-whatsapp"></i></a>
<a class="btn-messenger" href="#"><i class="mdi mdi-facebook-messenger"></i></a>
<a class="btn-google" href="#"><i class="mdi mdi-google"></i></a>
</div>
</div>
</div>
</div>
</section>


<section class="pt-4 pb-4 footer-bottom">
<div class="container">
<div class="row no-gutters">
<div class="col-lg-6 col-sm-6">
<p class="mt-1 mb-0">&copy; Copyright 2020 <strong class="text-dark">Groci</strong>. All Rights Reserved<br>
<small class="mt-0 mb-0">Made with <i class="mdi mdi-heart text-danger"></i> by <a href="https://askbootstrap.com/" target="_blank" class="text-primary">Ask Bootstrap</a>
</small>
</p>
</div>
<div class="col-lg-6 col-sm-6 text-right">
<img alt="osahan logo" src="../img/payment_methods.png">
</div>
</div>
</div>
</section>


<!-- Footer Scripts -->
<script data-cfasync="false" src="{{asset('js/email-decode.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}" type="482a6fa3de782e6b196b5c57-text/javascript"></script>

<script src="{{asset('js/select2.min.js')}}" type="482a6fa3de782e6b196b5c57-text/javascript"></script>

 <script src="{{asset('dist/owl-carousel/owl.carousel.js')}}" type="482a6fa3de782e6b196b5c57-text/javascript"></script>

<script src="{{asset('js/custom.js')}}" type="482a6fa3de782e6b196b5c57-text/javascript"></script>
<script src="{{asset('js/rocket-loader.min.js')}}" data-cf-settings="482a6fa3de782e6b196b5c57-|49" defer=""></script></body>

<script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

<!-- Mirrored from askbootstrap.com/preview/groci/theme-two/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Feb 2021 12:04:01 GMT -->
</html>
<script>
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

</script>

<script>
  var x=document.getElementById("LocationStatus");
function getLocation(){
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition,showError);
        $(".fa-spinner").show();
        $("#LocationStatus").removeClass("text-danger");
    }
    else{
        x.innerHTML="Geolocation is not supported by this browser.";
    }
}

function showPosition(position){
    lat=position.coords.latitude;
    lon=position.coords.longitude;
    
    displayLocation(lat,lon);
}

function showError(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            x.innerHTML="We do not have permission to determine your location. Please enter manually."
        break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML="Location information is unavailable."
        break;
        case error.TIMEOUT:
            x.innerHTML="The request to get user location timed out."
        break;
        case error.UNKNOWN_ERROR:
            x.innerHTML="An unknown error occurred."
        break;
    }
$("#LocationStatus").addClass("text-danger");
$(".fa-spinner").hide();
}

function displayLocation(latitude,longitude){

    
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);

    geocoder.geocode(
        {'latLng': latlng}, 
        function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;

                    var  value=add.split(",");

                    count=value.length;
                    
                    country=value[count-1];
                    state=value[count-2];
                  
                    city=value[count-3];
                    remov = state.slice(-6);
                    state = state.replace(remov,'');

                    const myForm = document.getElementById("locationform");
                    document.getElementById('searchInput').value=add;
                    document.getElementById('city').value=city;
                    document.getElementById('state').value=state;
                    document.getElementById('lat').value=latitude;
                    document.getElementById('lng').value=longitude;
                    //all information goes to form inputs for submit
                    setInterval(function () {
                    document.getElementById('InputLocation').innerHTML=city;
                    $(".fa-spinner").hide();
                     myForm.submit();
                    }, 3000); // in milliseconds
                     $("#LocationStatus").removeClass("text-danger");
                }
                else  {
                    x.innerHTML = "address not found";
                     $("#LocationStatus").addClass("text-danger");
                }

            }
            else {
                x.innerHTML = "Geocoder failed due to: " + status;
                 $("#LocationStatus").addClass("text-danger");
            }
        }
    );
}
//Slide location bar 
$(document).ready(function(){
  $(".location-top").click(function(){
    $(".locationMenu").fadeToggle();
     $(".fa-spinner").hide();
      $("#LocationStatus").removeClass("text-danger");
  });


  $("#locationswitch").click(function(){
    $(".locationform").fadeIn('slow');
    $(".locationfound").hide();
    
  });
  $(".backtoaddress").click(function(){
    $(".locationform").hide();
    $(".locationfound").fadeIn('slow');

  });

});
$(document).ready(function() {
        $(".locationform").submit(function (e) {
            e.preventDefault();
            var form = this;
             $(".fa-spinner").show('2');
            setInterval(function () {
                form.submit();
            }, 3000); // in milliseconds
        });
         $("#LocationStatus").removeClass("text-danger");
    });
//Continue Form submit

$(document).ready(function() {
        $(".locationform").submit(function (e) {
            e.preventDefault();
            var form = this;
             $(".fa-spinner").show('2');
            setInterval(function () {
                form.submit(); 
            }, 3000); // in milliseconds
        });
         $("#LocationStatus").removeClass("text-danger");
    });

//add to cart 

$(document).ready(function() {
       $(".addtocart").click(function(){
        var productid=$(this).attr("cart");
        var varientid=$('#varient'+productid).val();
       // window.location.href = "{{url('/cart','')}}"+"/"+productid+"/"+varientid;

         $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

      $.ajax({
      url:'<?php echo route('shop.cartadd') ?>',
      type:'post',
      data: {pid:productid,vid: varientid},
      success:function(data) {
      //alert(data);
       if(data[0] == 0){
       toastr.error("Item not added to your cart !")
       }

       if(data[0] == 1){
       toastr.success("Item added to your cart")
       }

       if(data[0] == 2){
       toastr.warning("Item Out of stock OR May be duplicate entries found !")
       }
       $('.cartcountshow').html(data[1]);
      },
      error:function () {
      console.log('error');
      }
      });
     
       });
    });


</script>
<!-- Session Messages -->
        @if(session()->has('success'))
        <script>
        $( document ).ready(function() {
        toastr.success("{!! session()->get('success')!!}")
        });
        </script>
        @endif
        @if(session()->has('error'))
        <script>
        $( document ).ready(function() {
        toastr.error("{!! session()->get('error')!!}")
        });
        </script>

        @endif 
@if (\Request::is('addressview') || \Request::is('checkout')|| \Request::is('addresseditview/*'))     
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=checkoutaddress" async defer></script>

<script src="{{asset('js/checkout_address.js')}}"></script>
@else
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>

<script src="{{asset('js/mapInput.js')}}"></script>
@endif  

