@extends('frontend.layouts.app')
 
@section('content') 

<!-- Address block condition -->
@if($addressstatuscheck !== null)
<script>
$(document).ready(function(){
  $('.addressbutton').addClass('collapsed');
  $('.addresscollapse').removeClass('show');
  //payment section
/*  $('.paymentbutton').removeClass('collapsed');
  $('.paymentcollapse').addClass('show');
*/  //add date & time section
  $('.date_time_button').removeClass('collapsed');
  $('.date_time_collapse').addClass('show');
});

</script>
@endif
<!-- Date & Time block condition -->
@if(Session::get('date_time') !== null)
<script>
$(document).ready(function(){
  $('.addressbutton').addClass('collapsed');
  $('.addresscollapse').removeClass('show');
  //payment section
  $('.paymentbutton').removeClass('collapsed');
  $('.paymentcollapse').addClass('show');
  //add date & time section
  $('.date_time_button').addClass('collapsed');
  $('.date_time_collapse').removeClass('show');
});

</script>
@endif

<section class="pt-3 pb-3 page-info section-padding border-bottom bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<a href="/"><strong><span class="mdi mdi-home"></span> Home</strong></a> <span class="mdi mdi-chevron-right"></span> <a href="">Checkout</a>
{{Session::get('checkout')}}
</div>
</div>
</div>
</section>
<section class="checkout-page section-padding">
<div class="container">
<div class="row">
<div class="col-md-8">
<div class="checkout-step">
<div class="accordion" id="accordionExample">

<div class="card checkout-step-two">
<div class="card-header" id="headingTwo">
<h5 class="mb-0">
<button class="btn btn-link addressbutton" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
<span class="number">1</span> Delivery Address
@if($addressstatuscheck !== null)
<br>
<p class="ml-5 font-weight-bold">{{$addressstatuscheck->address}}</p>
@endif
</button>

</h5>
</div>
<div id="collapseTwo" class="collapse show addresscollapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
<div class="card-body">
<!-- Add Address Content start-->
<div class="row">
<div class="col-sm-12"> 

<button type="button" type="button" class="btn btn-secondary mb-2 btn-lg float-right add_address_button">
 <i class="fa fa-plus-circle" aria-hidden="true"></i>
Add New Address
</button>
<br /><br />
<div class="d-none add_address_content">
<form action="{{route('address.add')}}" method="post">
  @csrf
<div class="row">
<div class="col-sm-12">
<div class="form-group">
<label class="control-label"> Address <span class="required">*</span></label>
 <input   class="form-control" placeholder="Type Your Address ( All Information Fetch Automatically)" id="checkout_address_Input" name="address" type="text" value="{{old('address')}}" autocomplete="nope" required> 
<span style="color:orange"> @error('address') {{ $message }}@enderror </span>
<!-- address -->
    <div id="address-map-container">
    <div style="width: 100%; height: 100%;"  class="map" id="checkout_map" ></div>
    </div>
    <input type="hidden" name="latitude" id="checkout_address_lat"/>
    <input type="hidden" name="longitude" id="checkout_address_lng"/>
</div>
</div>

</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label"> Name <span class="required">*</span></label>
<input class="form-control border-form-control" name="name" placeholder="Your Name" type="text" value="{{old('name')}}" required>

<span style="color:orange"> @error('name') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Mobile Number <span class="required">*</span></label>
<input class="form-control border-form-control"  name="mobile" placeholder="Your Mobile Number" type="number" value="{{old('mobile')}}" required>

<span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">City <span class="required">*</span></label>
<input class="form-control border-form-control" name="city" id="checkout_address_city" placeholder="Your City" type="text" value="{{old('city')}}" required>
<span style="color:orange"> @error('city') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">State <span class="required">*</span></label>
<input class="form-control border-form-control" name="state"  id="checkout_address_state"placeholder="Your State" type="text" value="{{old('state')}}" required>
<span style="color:orange"> @error('state') {{ $message }}@enderror </span>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Country <span class="required">*</span></label>
<input class="form-control border-form-control" name="country" id="checkout_address_country" placeholder="Your Country" type="text" value="{{old('country')}}" required>
<span style="color:orange"> @error('country') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Zip Code <span class="required">*</span></label>
<input class="form-control border-form-control" name="zipcode" id="checkout_address_zip"placeholder="Your Zip Code" type="text" value="{{old('zipcode')}}" required>
<span style="color:orange"> @error('zipcode') {{ $message }}@enderror </span>
</div>
</div>
</div>
<div class="row">
  <div class="col-sm-6">
<div class="form-group">
<label class="control-label">Landmark <span class="required">*</span></label>
<input class="form-control border-form-control" name="landmark" placeholder="Your Nearest Landmark" type="text" value="{{old('landmark')}}" autocomplete="off" required> 
<span style="color:orange"> @error('landmark') {{ $message }}@enderror </span>
</div>
</div>
  <div class="col-sm-6">
<div class="form-group">
<label class="control-label">Nick Name for your address <span class="required">*</span></label>
<input class="form-control border-form-control" name="nickname" placeholder="Your Address Nick Name" type="text" value="{{old('nickname')}}" autocomplete="off" required> 
<span style="color:orange"> @error('nickname') {{ $message }}@enderror </span>
</div>
</div>
</div>
  <div class="row">
  <div class="col-sm-12 text-right">
  <button type="button" class="btn btn-danger btn-lg cancel_add_address"> Cancel </button>
  <button type="submit" class="btn btn-success btn-lg"> Save Changes</button>
  </div> 
  </div>
</form>
</div>
</div>
</div>
<!-- Add Address Content end -->
<div class="row addresses_area">

@foreach($addresses as $address)
  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
      	@if($address->status == 1)
      	<i class="fas fa-arrow-down float-right text-success bounce" style="font-size:17px;"></i>
      	@endif
        <h5 class="card-title text-capitalize" >{{$address->nick_name}}</h5>
        <p class="card-text" style="height: 120px;">
        <b class="text-capitalize">{{$address->name}} </b>
        <br/>
        <b> {{$address->address}}</b>
    </p>
       @if($address->status == 1)
     <a type="button" class="btn btn-success text-white">Selected</a>
       @else 
     <a href="{{route('deliverhere',$address->id)}}" class="btn btn-info">Deliever Here</a>
       @endif
       
      </div>
    </div>
  </div>
 @endforeach
</div>
<br />
@if($addressstatuscheck !== null)
<button type="button" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="btn btn-secondary mb-2 btn-lg float-right addresses_area">NEXT STEP</button>
@endif
</div>

</div>
</div>
<div class="card">
<div class="card-header" id="date_times">
<h5 class="mb-0">

@if(Session::get('date_time') !== null || Session::get('date_time') !== " " && $addressstatuscheck !== null)
<button class="btn btn-link collapsed date_time_button" type="button" data-toggle="collapse" data-target="#date_time" aria-expanded="false" aria-controls="date_time">
<span class="number">2</span> Delivery Date & Time
<br>
<p class="ml-5 font-weight-bold">{{Session::get('date_time')}}</p>
</button> 
@else
<button class="btn btn-link collapsed date_time_button" type="button">
<span class="number">2</span> Delivery Date & Time
</button>
@endif

</h5>
</div>  
<div id="date_time" class="collapse date_time_collapse" aria-labelledby="date_times" data-parent="#date_time">
  <!-- Date & Time -->
  <div class="col-xs-12 p-5">
    <form method="post" action="{{route('datetime')}}">
      @csrf
  <nav>
  <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
 
  <?php
  $Date = date("D M j");
  ?>
  <a class="nav-item nav-link  datelink" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" date="{{$Date}}">{{$Date}}</a>
  <a class="nav-item nav-link datelink" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" date="{{date('D M j', strtotime($Date. ' + 1 days'))}}">{{date('D M j', strtotime($Date. ' + 1 days'))}}</a>
  <a class="nav-item nav-link datelink" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" date="{{date('D M j', strtotime($Date. ' + 2 days'))}}">{{date('D M j', strtotime($Date. ' + 2 days'))}}</a>
  <a class="nav-item nav-link datelink" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" date="{{date('D M j', strtotime($Date. ' + 3 days'))}}">{{date('D M j', strtotime($Date. ' + 3 days'))}}</a>
  <a class="nav-item nav-link datelink" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" date="{{date('D M j', strtotime($Date. ' + 4 days'))}}">{{date('D M j', strtotime($Date. ' + 4 days'))}}</a>
  <a class="nav-item nav-link datelink" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" date="{{date('D M j', strtotime($Date. ' + 5 days'))}}">{{date('D M j', strtotime($Date. ' + 5 days'))}}</a>
  <a class="nav-item nav-link datelink" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" date="{{date('D M j', strtotime($Date. ' + 6 days'))}}">{{date('D M j', strtotime($Date. ' + 6 days'))}}</a>
  <input type="hidden" name="datetime" class="datetime_input" value="{{$Date}}"/>
  </div>
  </nav>
  <div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-sat" role="tabpanel" aria-labelledby="pills-sat-tab">
    <span class="text-danger float-right font-weight-bold" >@error('time') {{ $message }}@enderror</span>
  <ul>
  <li>
  <input type="checkbox" class="check" name="time[]" value="6 AM-9 AM" style="margin-right: 15px;"> <label>6 AM - 9 AM</label>
  </li>
  <li>
  <input type="checkbox" class="check" name="time[]" value="9 AM-12 PM"style="margin-right: 15px;"> <label>9 AM - 12 PM</label>
  </li>
  <li>
  <input type="checkbox" name="time[]" class="check" value="4 PM-7 PM" style="margin-right: 15px;"> <label>4 PM - 7 PM</label>
  </li>
  <li>
  <input type="checkbox" name="time[]" class="check" value="7 PM-9 PM"style="margin-right: 15px;"> <label>7 PM - 9 PM</label>
  </li>
  </ul>

  </div>
  <div class="tab-pane fade" id="pills-sun" role="tabpanel" aria-labelledby="pills-sun-tab">

  </div>

  </div>
  <br />

  @if($addressstatuscheck !== null && Session::get('date_time') !==null)
  <button  type="submit" class="btn btn-info mb-2 btn-lg float-right addresses_area">Submit</button>
  &nbsp;
<button  type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="btn btn-secondary mb-2 btn-lg float-right addresses_area">NEXT STEP</button>
@else
<button  type="submit" class="btn btn-info mb-2 btn-lg float-right addresses_area">Submit</button>
@endif
</form>
  </div>

</div>

</div>
<div class="card">
<div class="card-header" id="headingThree">
<h5 class="mb-0">

@if($addressstatuscheck !== null && Session::get('date_time') !==null)
<button class="btn btn-link collapsed paymentbutton" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
<span class="number">3</span> Payment
</button> 
@else
<button class="btn btn-link collapsed paymentbutton" type="button">
<span class="number">3</span> Payment
</button>
@endif

</h5>
</div>
<div id="collapseThree" class="collapse paymentcollapse" aria-labelledby="headingThree" data-parent="#accordionExample">
<div class="card-body" style="margin: 0px 0px 0px 59px;">

<div class="form-check">
  <input class="form-check-input" type="radio" name="payment" id="payment" value="razorpay">
  <label class="form-check-label razorpaylabel" for="exampleRadios1" >
<img src="{{asset('dist/img/payment/razorpay.png')}}" style="width:30%;">
  </label>
  <br>
  <div class="razorpay box" style="display: none;height:80px;">
        <br />

    <form action="/razorpay" method="POST" class="mx-auto">
        @csrf

      <script
          src="https://checkout.razorpay.com/v1/checkout.js"
          data-key="rzp_test_RY9YnlOGgxjtXk"
          data-amount="{{Session::get('checkout')}}" 
          data-currency="INR"
          data-order_id="{{Session::get('order_id')}}"
          data-buttontext="Pay with Razorpay"
          data-name="Grocery"
          data-description="Order Payment"
          data-prefill.name="{{Auth::user()->name}}"
          data-prefill.email="{{Auth::user()->email}}"
          data-prefill.contact="{{Auth::user()->mobile}}"
          data-theme.color="#095c91"
      ></script>
      <input type="hidden" custom="Hidden Element" name="hidden" />
      </form>

  </div>
</div>
<br />
<div class="form-check">
  <input class="form-check-input" type="radio" name="payment" id="payment" value="cashfree">
  <label class="form-check-label cashfreelabel" for="exampleRadios2">
<img src="{{asset('dist/img/payment/cashfree.png')}}" style="width:30%;">
  </label>
  <br>
  <div class="cashfree box" style="display: none;height:80px;">
    <br />
     <button class="btn btn-info btn-lg">Pay With Cashfree </button>
  </div>
</div>

   
    <br />


</div>
</div>
</div>

</div>
</div>
</div>
<div class="col-md-4">
<div class="card">
<h5 class="card-header">My Cart <span class="text-secondary float-right">({{$CartCount}} item)</span></h5>
<div class="card-body pt-0 pr-0 pl-0 pb-0">
@foreach($products as $product)
<div class="cart-list-product">
<img class="img-fluid" src="/product_images/{{$product->coverimage}}" alt="">
<span class="badge badge-success">{{$product->discount}}0% OFF</span>
<h5><a href="#">{{$product->name}}</a></h5>

@foreach($carts as $cart)
@if($cart->productid == $product->id)

<p class="offer-price mb-0">
    <strong><span class="mdi mdi-approval text-danger"></span> </strong>{{$cart->size}}
	<i class="fas fa-rupee-sign fa-sm"></i>{{$cart->saleprice}}
	 <i class="mdi mdi-tag-outline"></i>
	 <span class="regular-price">
	 	<i class="fas fa-rupee-sign fa-sm"></i>
	 	{{$cart->price}}
	 </span>
</p>

@endif
@endforeach
</div>
@endforeach
</div>
</div>
</div>
</div>
</div>
</section>

<script>
$(document).ready(function(){

  $(document).on('click','.add_address_button',function(){
       
       $('.add_address_content').removeClass('d-none');
       $('.add_address_content').fadeIn();
       $('.addresses_area').hide();
    });
  $(document).on('click','.cancel_add_address',function(){
       
       $('.add_address_content').addClass('d-none');
       $('.add_address_content').fadeOut();
       $('.addresses_area').show();
    });
  });

$(document).ready(function(){
    $('.check').click(function() {
        $('.check').not(this).prop('checked', false);
    });
    $('.datelink').click(function(){
      var datetime=$(this).attr('date');
      $('.datetime_input').val(datetime);
    });
});

//payment div change according to selection of payment gateway
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".box").not(targetBox).hide();
        $(targetBox).fadeIn();
    });
});
</script>
@endsection