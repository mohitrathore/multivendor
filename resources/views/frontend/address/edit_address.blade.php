@extends('frontend.layouts.app')
 
@section('content') 
<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-9 mx-auto">
<div class="row no-gutters">
<div class="col-md-4">
<div class="card account-left">
<div class="user-profile-header">
	
@if($users->profileImage !== null)

<img class="card-profile-img tecxt" src="../profile_images/{{$users->profileImage}}">

@else
<i class="mdi mdi-account-circle" style="font-size: 70px;"></i>
@endif

<h5 class="mb-1 text-secondary text-capitalize"><strong>Hi </strong> {{$users->name}}</h5>
<p> +91 {{$users->mobile}}</p>
</div>
<div class="list-group">
<a href="{{route('profile.view')}}" class="list-group-item list-group-item-action "><i aria-hidden="true" class="mdi mdi-account-outline"></i> My Profile</a>
<a href="{{route('address.view')}}" class="list-group-item list-group-item-action active"><i aria-hidden="true" class="mdi mdi-map-marker-circle"></i> My Address</a>
<a href="{{route('order.index')}}" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-format-list-bulleted"></i> Order List</a>
<a href="{{ route('logout') }}" onclick="event.preventDefault(); 
  document.getElementById('logout-form').submit();" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-lock"></i> Logout</a>
<!-- Log Out -->
 
  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
  @csrf
  </form>
</div> 
</div>
</div>
<div class="col-md-8">
<div class="card card-body account-right">
<div class="widget">
<div class="section-header">
<h5 class="heading-design-h5">
Add New Address
</h5>
</div>
<form action="{{route('address.edit')}}" method="post">
	@csrf

<input type="hidden" name="cid" value="{{$address->id}}" />
<div class="row">
<div class="col-sm-12">
<div class="form-group">
<label class="control-label"> Address <span class="required">*</span></label>
 <input   class="form-control" placeholder="Type Your Address ( All Information Fetch Automatically)" id="checkout_address_Input" name="address" type="text" value="{{$address->address}}" autocomplete="off"> 
<span style="color:orange"> @error('address') {{ $message }}@enderror </span>
<!-- address -->
    <div id="address-map-container">
    <div style="width: 100%; height: 100%;"  class="map" id="checkout_map" ></div>
    </div>
    <input type="hidden" name="latitude" id="checkout_address_lat"/>
    <input type="hidden" name="longitude" id="checkout_address_lng"/>
</div>
</div>

</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label"> Name <span class="required">*</span></label>
<input class="form-control border-form-control" name="name" placeholder="Your Name" type="text" value="{{$address->name}}">

<span style="color:orange"> @error('name') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Mobile Number <span class="required">*</span></label>
<input class="form-control border-form-control"  name="mobile" placeholder="Your Mobile Number" type="number" value="{{$address->mobile}}">

<span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">City <span class="required">*</span></label>
<input class="form-control border-form-control" name="city" id="checkout_address_city" placeholder="Your City" type="text" value="{{$address->city}}">
<span style="color:orange"> @error('city') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">State <span class="required">*</span></label>
<input class="form-control border-form-control" name="state"  id="checkout_address_state"placeholder="Your State" type="text" value="{{$address->state}}">
<span style="color:orange"> @error('state') {{ $message }}@enderror </span>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Country <span class="required">*</span></label>
<input class="form-control border-form-control" name="country" id="checkout_address_country" placeholder="Your Country" type="text" value="{{$address->country}}">
<span style="color:orange"> @error('country') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Zip Code <span class="required">*</span></label>
<input class="form-control border-form-control" name="zipcode" id="checkout_address_zip"placeholder="Your Zip Code" type="text" value="{{$address->zip}}">
<span style="color:orange"> @error('zipcode') {{ $message }}@enderror </span>
</div>
</div>
</div>
<div class="row">
  <div class="col-sm-6">
<div class="form-group">
<label class="control-label">Landmark <span class="required">*</span></label>
<input class="form-control border-form-control" name="landmark" placeholder="Your Nearest Landmark" type="text" value="{{$address->landmark}}" autocomplete="off"> 
<span style="color:orange"> @error('landmark') {{ $message }}@enderror </span>
</div>
</div>
  <div class="col-sm-6">
<div class="form-group">
<label class="control-label">Nick Name for your address <span class="required">*</span></label>
<input class="form-control border-form-control" name="nickname" placeholder="Your Address Nick Name" type="text" value="{{$address->nick_name}}" autocomplete="off"> 
<span style="color:orange"> @error('nickname') {{ $message }}@enderror </span>
</div>
</div>
</div>
	<div class="row">
	<div class="col-sm-12 text-right">
	<a href="{{route('address.view')}}" class="btn btn-danger btn-lg"> Cancel </a>
	<button type="submit" class="btn btn-success btn-lg"> Save Changes</button>
	</div> 
	</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
@endsection