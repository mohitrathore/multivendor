@extends('frontend.layouts.app')
 
@section('content') 
<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-12 mx-auto">
<div class="row no-gutters">
<div class="col-md-4">
<div class="card account-left">
<div class="user-profile-header">
	
@if($users->profileImage !== null)

<img class="card-profile-img tecxt" src="../profile_images/{{$users->profileImage}}">

@else
<i class="mdi mdi-account-circle" style="font-size: 70px;"></i>
@endif

<h5 class="mb-1 text-secondary text-capitalize"><strong>Hi </strong> {{$users->name}}</h5>
<p> +91 {{$users->mobile}}</p>
</div>
<div class="list-group">
<a href="{{route('profile.view')}}" class="list-group-item list-group-item-action "><i aria-hidden="true" class="mdi mdi-account-outline"></i> My Profile</a>
<a href="{{route('address.view')}}" class="list-group-item list-group-item-action active"><i aria-hidden="true" class="mdi mdi-map-marker-circle"></i> My Address</a>
<a href="{{route('order.index')}}" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-format-list-bulleted"></i> Order List</a>
<a href="{{ route('logout') }}" onclick="event.preventDefault(); 
  document.getElementById('logout-form').submit();" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-lock"></i> Logout</a>
<!-- Log Out -->
 
  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
  @csrf
  </form>
</div> 
</div>
</div>
<div class="col-md-8">
<div class="card card-body account-right">
<div class="widget">
<div class="section-header">
<h5 class="heading-design-h5">
My Address
<a href="{{route('address.addview')}}" class="btn btn-info btn-lg float-right" > Add New Address </a>
</h5>
</div>
<br />
<div class="order-list-tabel-main table-responsive">
<table class="datatabel table table-striped table-bordered order-list-tabel" width="100%" cellspacing="0">
<thead>
<tr>
<th style="width:10%;">#</th>
<th style="width:10%;">Name</th>
<th style="width:25%;">Address</th>
<th style="width:20%;">Landmark</th>
<th style="width:20%;">Nick Name</th>
<th style="width:15%;">Action</th>
</tr>
</thead>
<tbody>
@foreach($addressall as $address)
<tr>
<td>#{{$address->id}}</td>
<td>{{$address->name}}</td>
<td>{{$address->address}}</td>
<td>{{$address->landmark}}</td>
<td>{{$address->nick_name}}</td>
<td>
  <a href="{{route('address.editview',$address->id)}}" class="btn btn-info btn-sm" style=""><i class="mdi mdi-pencil"></i></a>
  
  <a href="{{route('address.delete',$address->id)}}" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
@endsection