@extends('frontend.layouts.app')
 
@section('content') 

<section class="pt-3 pb-3 page-info section-padding border-bottom bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<a href="#"><strong><span class="mdi mdi-home"></span> Home</strong></a> <span class="mdi mdi-chevron-right"></span> <a href="#">Shop</a>
</div>
</div>
</div>
</section>
<section class="shop-list section-padding">
<div class="container">
<div class="row">
<div class="col-md-3">
<div class="shop-filters">
<div id="accordion">
<div class="card">
<div class="card-header" id="headingOne">
<h5 class="mb-0"> 
<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
Category   
<i class="fas fa-spinner fa-pulse ml-5 d-none" style="font-size:1.5em;"></i>
<span class="mdi mdi-chevron-down float-right"></span>
</button>
</h5>
</div>
<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
<div class="card-body card-shop-filters">
<form class="form-inline mb-3">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search By Category">
<button type="submit" class="pl-2 pr-2 btn btn-secondary btn-lg"><i class="mdi mdi-file-find"></i></button>
</div>
<style>

</style>
<div class="panel-group ml-1 mt-3" id="maincategory_accordin" role="tablist" aria-multiselectable="true">
@foreach($maincategory as $maincat)
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne3">
<h4 class="panel-title">
<a  role="button" data-toggle="collapse" data-parent="#maincategory_accordin" href="#main{{$maincat->id}}" aria-expanded="@if($maincat->id == $mainid)true @else false @endif" aria-controls="main{{$maincat->id}}" class="text-capitalize @if($maincat->id == $mainid) @else collapsed @endif">
</a>
<span type="button" 
class=" btn @if($maincat->id == $mainid) btn-danger @else btn-default @endif text-capitalize font-weight-bold main{{$maincat->id}}" 
style="color:@if($maincat->id == $mainid) white @else #004e89 @endif;margin-left:-20px;border:none;">
{{$maincat->name}}
 </span>

</h4>
</div>

<!--For Main Category  -->
<script type="text/javascript">



$('.main{{$maincat->id}}').on('click', function() {
 
  var categoryId="{{$maincat->id}}";
  var categoryName="{{$maincat->name}}";
  var url='/shop/'+categoryName+'/'+categoryId;


$('.fa-spinner').removeClass('d-none');
  setInterval(function () {
              window.location.href=url;
            }, 2000); // in milliseconds
  
  
});
</script>
<!--For Main Category  -->
<div id="main{{$maincat->id}}" class="panel-collapse collapse @if($maincat->id == $mainid)show in @else  @endif" role="tabpanel" aria-labelledby="headingOne3" style="margin-left: -20px;">

<div class="panel-group ml-1 mt-3" id="secondcategory_accordin" role="tablist" aria-multiselectable="true">
@foreach($secondcategory as $secondcat)
@if($secondcat->parentid == $maincat->id)
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne3">
<h4 class="panel-title">
<a  role="button" data-toggle="collapse" data-parent="#secondcategory_accordin" href="#second{{$secondcat->id}}" aria-expanded="@if($secondcat->id == $secondid)true @else false @endif" aria-controls="second{{$secondcat->id}}" class="text-capitalize @if($secondcat->id == $secondid) @else collapsed @endif">
</a>
<span type="button" 
class=" btn @if($secondcat->id == $secondid) btn-danger @else btn-default @endif text-capitalize font-weight-bold second{{$secondcat->id}} " 
style="color:@if($secondcat->id == $secondid) white @else #004e89 @endif;margin-left:-20px;border:none;">
{{$secondcat->name}}
 </span>

</h4>
</div>
<!--For Second Category  -->
<script type="text/javascript">



$('.second{{$secondcat->id}}').on('click', function() {
 
  var categoryId="{{$mainid}}";
  var categoryName="{{$main}}";
  var categoryId_second="{{$secondcat->id}}";
  var categoryName_second="{{$secondcat->name}}";
  var url='/shop/'+categoryName+'/'+categoryId+'/'+categoryName_second+'/'+categoryId_second;


$('.fa-spinner').removeClass('d-none');
  setInterval(function () {
              window.location.href=url;
            }, 2000); // in milliseconds
  
  
});
</script>
<!--For second Category  -->
<div id="second{{$secondcat->id}}" class="panel-collapse collapse @if($secondcat->id == $secondid)show in @else  @endif" role="tabpanel" aria-labelledby="headingOne3">

@foreach($thirdcategory as $thirdcat)
@if($thirdcat->parentid == $secondcat->id)
       <div class="panel-group " id="thirdcategory_accordin" role="tablist" aria-multiselectable="true" style="margin-left: -12px">

      <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne3">
      <h4 class="panel-title">
      <a role="button" data-toggle="collapse" data-parent="#thirdcategory_accordin" href="#third{{$thirdcat->id}}" aria-expanded="false" aria-controls="third{{$thirdcat->id}}" class="text-capitalize collapsed">
      
      </a>
      <span type="button" class=" btn @if($thirdcat->id == $thirdid) btn-danger @else btn-default @endif text-capitalize font-weight-bold third{{$thirdcat->id}}" style="color:@if($thirdcat->id == $thirdid) white @else #004e89 @endif;margin-left:-30px;">{{$thirdcat->name}} </span>
      </h4>
      </div>
      
      </div>

      </div>
      <!--For third Category  -->
<script type="text/javascript">



$('.third{{$thirdcat->id}}').on('click', function() {
 
  var categoryId="{{$mainid}}";
  var categoryName="{{$main}}";
  var categoryId_second="{{$secondcat->id}}";
  var categoryName_second="{{$secondcat->name}}";
  var categoryId_third="{{$thirdcat->id}}";
  var categoryName_third="{{$thirdcat->name}}";
  var url='/shop/'+categoryName+'/'+categoryId+'/'+categoryName_second+'/'+categoryId_second+'/'+categoryName_third+'/'+categoryId_third;


$('.fa-spinner').removeClass('d-none');
  setInterval(function () {
              window.location.href=url;
            }, 2000); // in milliseconds
  
  
});
</script>
<!--For third Category  -->
@endif
@endforeach

</div>

</div>
@endif
@endforeach
</div>

</div>

</div>
@endforeach
</div>

</div>
 

</div>
</div>
<div class="card">
<div class="card-header" id="headingTwo">
<h5 class="mb-0">
<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
Price <span class="mdi mdi-chevron-down float-right"></span>
</button>
</h5>
</div>
<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
<div class="card-body card-shop-filters">
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="1">
<label class="custom-control-label" for="1">$68 to $659 <span class="badge badge-warning">50% OFF</span></label>
</div>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="2">
<label class="custom-control-label" for="2">$660 to $1014</label>
</div>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="3">
<label class="custom-control-label" for="3">$1015 to $1679</label>
</div>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="4">
<label class="custom-control-label" for="4">$1680 to $1856</label>
</div>
</div>
</div>
</div>
<div class="card">
<div class="card-header" id="headingThree">
<h5 class="mb-0">
<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
Brand <span class="mdi mdi-chevron-down float-right"></span>
</button>
</h5>
</div>
<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
<div class="card-body card-shop-filters">
 <form class="form-inline mb-3">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search By Brand">
</div>
<button type="submit" class="btn btn-secondary ml-2">GO</button>
</form>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="b1">
<label class="custom-control-label" for="b1">Imported Fruits <span class="badge badge-warning">50% OFF</span></label>
</div>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="b2">
<label class="custom-control-label" for="b2">Seasonal Fruits <span class="badge badge-secondary">NEW</span></label>
</div>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="b3">
<label class="custom-control-label" for="b3">Imported Fruits <span class="badge badge-danger">10% OFF</span></label>
</div>
<div class="custom-control custom-checkbox">
<input type="checkbox" class="custom-control-input" id="b4">
<label class="custom-control-label" for="b4">Citrus</label>
</div>
</div>
</div>
</div>
<div class="card">
<div class="card-header" id="headingThree">
<h5 class="mb-0">
<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
Imported Fruits <span class="mdi mdi-chevron-down float-right"></span>
</button>
</h5>
</div>
<div id="collapsefour" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
<div class="card-body">
<div class="list-group">
<a href="#" class="list-group-item list-group-item-action">All Fruits</a>
<a href="#" class="list-group-item list-group-item-action">Imported Fruits</a>
<a href="#" class="list-group-item list-group-item-action">Seasonal Fruits</a>
<a href="#" class="list-group-item list-group-item-action">Citrus</a>
<a href="#" class="list-group-item list-group-item-action disabled">Cut Fresh & Herbs</a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="left-ad mt-4">
<img class="img-fluid" src="http://via.placeholder.com/254x557" alt="">
</div>
</div>
<div class="col-md-9">
<a href="#"><img class="img-fluid mb-3" src="../img/shop2.jpg" alt=""></a>
<div class="shop-head">
<a href="#">
  <span class="mdi mdi-home"></span> Home
</a> 
<span class="mdi mdi-chevron-right"></span>
 <a href="/shop/{{$main}}/{{$mainid}}" class="text-capitalize">{{$main}}
 </a>
 <span class="mdi mdi-chevron-right"></span> 
 <a href="/shop/{{$main}}/{{$mainid}}" class="text-capitalize">{{$second}}
 </a>
 <span class="mdi mdi-chevron-right"></span> 
 <a href="/shop/{{$main}}/{{$mainid}}/{{$second}}/{{$secondid}}/{{$third}}/{{$thirdid}}" class="text-capitalize">{{$third}}
 </a>

<!-- <span class="mdi mdi-chevron-right"></span> <a href="#">Fruits</a> -->
<div class="btn-group float-right mt-2">
<button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
Sort by Products &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</button>
<div class="dropdown-menu dropdown-menu-right">
<a class="dropdown-item" href="#">Relevance</a>
<a class="dropdown-item" href="#">Price (Low to High)</a>
<a class="dropdown-item" href="#">Price (High to Low)</a>
<a class="dropdown-item" href="#">Discount (High to Low)</a>
<a class="dropdown-item" href="#">Name (A to Z)</a>
</div>
</div>
<h5 class="mb-3 text-capitalize">{{$third}}</h5>
</div>
<div class="row no-gutters">
@if(count($products) > 0)
@foreach($products as $product) 
<div class="col-md-4">
<div class="product">
<a href="{{route('shop.singleproduct',['product' => $product->name, 'productid' =>$product->id])}}">
 <div class="product-header">
<span class="badge badge-success">50% OFF</span>
<img class="img-fluid" src="/product_images/{{$product->coverimage}}" alt="">
@if($product->type == "veg")
<span class="veg text-success mdi mdi-circle"></span>
@elseif($product->type == "non-veg")
<span class="non-veg text-danger mdi mdi-circle"></span>
@endif
</div> 
</a>
<div class="product-body">
  <h6>
<label for="varient"><span class="mdi mdi-approval"></span> Available in</label>

    <select class="form-control"  style="height:calc(1.5em + .45rem + 2px);height:" name="varient" id="varient{{$product->id}}">
      @foreach($varients as $varient)
       @if($varient->productid == $product->id)
      <option value="{{$varient->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>  -  ₹ {{$varient->price}}</option>
     
      @endif
    
      @endforeach
    </select>
</h6>
<h5 class="text-capitalize" style="white-space:nowrap;text-overflow: ellipsis;width:100%;display:block;overflow:hidden">{{$product->name}}</h5>

</div>
<br />
<div class="product-footer">
<button type="button" class="btn btn-secondary btn-sm float-right addtocart text-white" cart="{{$product->id}}"><i class="mdi mdi-cart-outline"></i> Add</button>
<p class="offer-price mb-0">
     @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshow{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i><!-- <br><span class="regular-price">$800.99</span> --></p>
</div>

<script>
$('#varient{{$product->id}}').on('change', function() {
  var val=this.value;
  var text=$("#varient{{$product->id}} option:selected").text();
  $('#varientshow{{$product->id}}').html(text);
});
</script>

</div>
</div>

@endforeach
@else
<div>
  <p style="font: 22px ProximaNova-Semibold !important;color:#4a4a4a; margin: 56px auto 10px;">We are sorry, no results found.</p>
  <h6 style="font: 16px ProximaNovaA-Regular !important;color: #8f8f8f;"> Please try to search with a different spelling or check out our categories. </h6>
</div>
@endif



</div>

<nav>
<ul class="pagination justify-content-center mt-4">

<li class="page-item">

  {{ $products->links()}}


</li>

</ul>
 </nav>
</div>
</div>
</div>
</section>

<!-- Top Savers Start-->
<section class="product-items-slider section-padding bg-white border-top">
<div class="container">
<div class="section-header">
<h5 class="heading-design-h5">Best Offer View <span class="badge badge-info">20% OFF</span>
<a class="float-right text-secondary" href="shop.html">View All</a>
</h5>
</div>
<div class="owl-carousel owl-carousel-featured">

@foreach($products as $product)
@if($product->criteria == "best-offer")
<div class="item">
<div class="product">
<a href="{{route('shop.singleproduct',['product' => $product->name, 'productid' =>$product->id])}}">
<div class="product-header">
<span class="badge badge-success">{{$product->discount}} % OFF</span>
<img class="img-fluid" src="../../../../../../product_images/{{$product->coverimage}}" alt="">
@if($product->type == "veg")
<span class="veg text-success mdi mdi-circle"></span>
@elseif($product->type == "non-veg")
<span class="non-veg text-danger mdi mdi-circle"></span>
@endif
</div>
</a>
<div class="product-body">
<h6>  
<label for="varient"><span class="mdi mdi-approval"></span> Available in</label>
    <select class="form-control"  style="height:calc(1.5em + .45rem + 2px);height:" name="varient" id="varientbest{{$product->id}}">

      @foreach($varients as $varient)
       @if($varient->productid == $product->id)

      <option value="{{$varient->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>  -  ₹ {{$varient->price}}</option>
      @endif
      @endforeach
    </select>
</h6>
<h5 class="text-capitalize"style="white-space: nowrap;text-overflow: ellipsis;width: 100%;display: block;overflow: hidden">{{$product->name}} ..</h5>
</div>  
<br />
<div class="product-footer">
<button type="button" class="btn btn-secondary btn-sm float-right addtocart text-white" cart="{{$product->id}}"><i class="mdi mdi-cart-outline"></i> Add</button>
<p class="offer-price mb-0">
     @foreach($varients as $varient)
       @if($varient->productid == $product->id)
         <b id="varientshowbest{{$product->id}}"><i>{{$varient->weight}}&nbsp;{{$varient->unit}}</i>&nbsp;-&nbsp;₹ {{$varient->price}}</b>
         @break
       @endif
       @endforeach
  <i class="mdi mdi-tag-outline"></i><!-- <br><span class="regular-price">$800.99</span> --></p>
</div>

</div>
</div>
<script>
$('#varientbest{{$product->id}}').on('change', function() {
  var val=this.value;
  var text=$("#varientbest{{$product->id}} option:selected").text();
  $('#varientshowbest{{$product->id}}').html(text);
});
</script>
@endif
@endforeach
</div>
</div>
</section>
<!-- Best Offer End -->



@endsection