<html>
<head>
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootsrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
</head>
<body>

  <div class="container-fluid my-5">

    <div class="row">
        <div class="col-sm-2"> </div>
        <div class="col-sm-8"> 
                <div class="card card-1">
                <div class="card-header bg-white">
                <div class="media flex-sm-row flex-column-reverse justify-content-between ">
                      
                <div class="col my-auto">

                    <h4 class="mb-0">Thanks for your Order, <span class="change-color text-capitalize">{{$orders->name}}</span> !</h4>
                </div>
                <div class="col-auto text-center my-auto pl-0 pt-sm-4"> 
                    <img class="img-fluid my-auto align-items-center mb-0 pt-3" 
                    src="{{asset('dist/img/AdminLTELogo.png')}}" width="115" height="115">
                    <p class="mb-4 pt-0 Glasses">Apke Apne Manpasand Products</p>
                     <button id="printInvoice" class="btn btn-info float-right"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row justify-content-between mb-3">
                <div class="col-auto">
                    <h6 class="color-1 mb-0 change-color">Receipt</h6>
                </div>
                <div class="col-auto "> <small>Receipt Voucher : 1KAU9-84UIL</small> </div>
            </div>
@foreach($products as $product)
            <div class="row">
            <div class="col">
            <div class="card card-2">
            <div class="card-body">
            <div class="media">
            <div class="sq align-self-center "> 
                <img class="img-fluid my-auto align-self-center mr-2 mr-md-4 pl-0 p-0 m-0" src="/product_images/{{$product->coverimage}}" width="135" height="135" /> </div>
                <div class="col my-auto">
            <h6 class="mb-0">{{$product->name}}</h6>
            </div>
            @foreach($orderitems as $varient)
            @if($varient->productid == $product->id)
            <div class="media-body my-auto text-right">
            <div class="row my-auto flex-column flex-md-row">
            
         <!--    <div class="col-auto my-auto"> <small>Golden Rim </small></div> -->
            <div class="col my-auto"> <small>Size : {{$varient->size}}</small></div>
            <div class="col my-auto"> 
                <small>Qty : {{$varient->quantity}} x <i class="fas fa-rupee-sign fa-sm"></i> {{$varient->price}}</small></div>
            <div class="col my-auto">
                <?php $price=$varient->quantity *  $varient->price;?>
            <h6 class="mb-0">&#8377;{{$price}}</h6>
            </div>
            </div>
            </div>
            @endif
            @endforeach
            </div>
            <hr class="my-3 ">
            <div class="row">
            <div class="col-md-3 mb-3"> <small> Track Order <span><i class=" ml-2 fa fa-refresh" aria-hidden="true"></i></span></small> </div>
            <div class="col mt-auto">
            <div class="progress my-auto">
            <div class="progress-bar progress-bar rounded" style="width: 62%" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="media row justify-content-between ">
            <div class="col-auto text-right"><span> <small class="text-right mr-sm-2"></small> <i class="fa fa-circle active"></i> </span></div>
            <div class="flex-col"> <span> <small class="text-right mr-sm-2">Out for delivary</small><i class="fa fa-circle active"></i></span></div>
            <div class="col-auto flex-col-auto"><small class="text-right mr-sm-2">Delivered</small><span> <i class="fa fa-circle"></i></span></div>
            </div>
            </div>
            </div>
            </div>
@endforeach
            </div>
            </div>
            </div>
          
            <div class="row mt-4">
                <div class="col">
                    <div class="row justify-content-between">
                        <div class="col-auto">
                            <p class="mb-1 text-dark"><b>Order Details</b></p>
                        </div>
                        <div class="flex-sm-col text-right col">
                            <p class="mb-1"><b>Total</b></p>
                        </div>
                        <div class="flex-sm-col col-auto">
                            <p class="mb-1">&#8377;{{$orders->amount}}</p>
                        </div>
                    </div>
                        @if($orders->discount !== null)
                        <div class="row justify-content-between">
                        <div class="flex-sm-col text-right col">
                        <p class="mb-1"> <b>Discount</b></p>
                        </div>

                        <div class="flex-sm-col col-auto">
                        <p class="mb-1">&#8377;{{$orders->discount}}</p>
                        </div>

                        </div>
                        @endif
                    
                    <div class="row justify-content-between">
                        <div class="flex-sm-col text-right col">
                            <p class="mb-1"><b>Delivery Charges</b></p>
                        </div>
                        <div class="flex-sm-col col-auto">
                            <p class="mb-1">Free</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row invoice ">
                <div class="col">
                    <p class="mb-1"> Invoice Number : 788152</p>
                    <p class="mb-1">Invoice Date : {{date('d-m-Y H:i A', strtotime($orders->created_at))}}</p>
                    <p class="mb-1">Order Id : <b>{{$orders->order_id}}</b></p>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="jumbotron-fluid">
                <div class="row justify-content-between ">
                    <div class="col-sm-auto col-auto my-auto">
                        <img class="img-fluid my-auto align-self-center " src="{{asset('dist/img/AdminLTELogo.png')}}" width="115" height="115"></div>
                    <div class="col-auto my-auto ">
                        <h2 class="mb-0 font-weight-bold">TOTAL PAID</h2>
                    </div>
                    <div class="col-auto my-auto ml-auto">
                        <h1 class="display-3 ">&#8377; {{$orders->amount}}</h1>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
        </div>
        <div class="col-sm-2"> </div>

    </div>
    
</div>
<script>
 $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
</script>
</body>
</html>
