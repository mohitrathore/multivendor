@extends('frontend.layouts.app')
 
@section('content') 
<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-12 ">
<div class="row no-gutters">
<div class="col-md-3">
<div class="card account-left">
<div class="user-profile-header">
	
@if($users->profileImage !== null)

<img class="card-profile-img tecxt" src="../profile_images/{{$users->profileImage}}">

@else
<i class="mdi mdi-account-circle" style="font-size: 70px;"></i>
@endif

<h5 class="mb-1 text-secondary text-capitalize"><strong>Hi </strong> {{$users->name}}</h5>
<p> +91 {{$users->mobile}}</p>
</div>
<div class="list-group">
<a href="{{route('profile.view')}}" class="list-group-item list-group-item-action "><i aria-hidden="true" class="mdi mdi-account-outline"></i> My Profile</a>
<a href="{{route('address.view')}}" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-map-marker-circle"></i> My Address</a>
<a href="{{route('order.index')}}" class="list-group-item list-group-item-action active"><i aria-hidden="true" class="mdi mdi-format-list-bulleted"></i> Order List</a>
<a href="{{ route('logout') }}" onclick="event.preventDefault(); 
  document.getElementById('logout-form').submit();" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-lock"></i> Logout</a>
<!-- Log Out -->
 
  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
  @csrf
  </form>
</div> 
</div>
</div>
<div class="col-md-9">
<div class="card card-body account-right">
<div class="widget">
<div class="section-header">
<h5 class="heading-design-h5">
Orders
</h5>
</div>
<br />
<div class="order-list-tabel-main table-responsive">
<table class="datatabel table table-striped table-bordered order-list-tabel" width="100%" cellspacing="0">
<thead>
<tr>
<th style="width:15%;">Purchase Date</th>
<th style="width:15%;">Name</th>
<th style="width:20%;">Delievery Address</th>
<th style="width:15%;">Landmark</th>
<th style="width:20%;">Delievery Date </th>
<th style="width:10%;">Order Status </th>
<th style="width:5%;">Order Details</th>
</tr>
</thead>
<tbody>
@foreach($orders as $order)
<!-- Order View Modal -->
<div class="modal fade" id="orderviews{{$order->id}}" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      <!--   <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<tr>
<td>{{date('d-m-Y H:i A', strtotime($order->created_at))}}</td>
<td>{{$order->name}}</td>
<td>{{$order->address}}</td>
<td>{{$order->landmark}}</td>
<td class="font-weight-bold">{{$order->date_time}}</td>
<td>
	@if($order->store_status == 0)
    <span class="badge badge-warning">Processing, Move Forward for accept/deny</span>
    @endif
    @if($order->store_status == 1)
     <span class="badge badge-success">Order Accepted, Move Forward for Dispatching</span>
     @endif
   @if($order->store_status == 2)
    <span class="badge badge-danger">Order Cancelled From Owner</span>
    @endif
</td>

<td>
	<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#orderviews{{$order->id}}"><i class="mdi mdi-eye"></i></a>
     <a href="{{route('order.detail',$order->id)}}" class="btn btn-success btn-sm" >Order Invoice</a>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6"> {{ $orders->links()}}</div>
            <div class="col-sm-3"></div>
        
          </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
@endsection