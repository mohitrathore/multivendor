@extends('frontend.layouts.app')
 
@section('content') 
<section class="pt-3 pb-3 page-info section-padding border-bottom bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<a href="/"><strong><span class="mdi mdi-home"></span> Home</strong></a> <span class="mdi mdi-chevron-right"></span> <a href="">Success Page</a> 
</div>
</div>
</div>
</section>
<section class="cart-page section-padding">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="card">
<div class="card-body">
<div class="text-center">
<div class="col-lg-10 col-md-10 mx-auto order-done">
<i class="mdi mdi-check-circle-outline text-secondary"></i>
<h4 class="text-success">Congrats! Your Order has been Accepted..</h4>
<p>
<!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, Sed ex est, consectetur eget consectetur, Lorem ipsum dolor sit amet... -->
</p>
</div>
<br />
<div class="text-center">
<a href="{{url('/')}}" class="btn btn-secondary mb-2 btn-lg">Return Back to Shop Page</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
@endsection