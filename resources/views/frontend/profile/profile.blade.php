@extends('frontend.layouts.app')
 
@section('content') 
<section class="account-page section-padding">
<div class="container">
<div class="row">
<div class="col-lg-9 mx-auto">
<div class="row no-gutters">
<div class="col-md-4">
<div class="card account-left">
<div class="user-profile-header">
	
@if($users->profileImage !== null)

<img class="card-profile-img tecxt" src="../profile_images/{{$users->profileImage}}">

@else
<i class="mdi mdi-account-circle" style="font-size: 70px;"></i>
@endif

<h5 class="mb-1 text-secondary text-capitalize"><strong>Hi </strong> {{$users->name}}</h5>
<p> +91 {{$users->mobile}}</p>
</div>
<div class="list-group">
<a href="{{route('profile.view')}}" class="list-group-item list-group-item-action active"><i aria-hidden="true" class="mdi mdi-account-outline"></i> My Profile</a>
<a href="{{route('address.view')}}" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-map-marker-circle"></i> My Address</a>
<a href="{{route('order.index')}}" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-format-list-bulleted"></i> Order List</a>
<a href="{{ route('logout') }}" onclick="event.preventDefault(); 
  document.getElementById('logout-form').submit();" class="list-group-item list-group-item-action"><i aria-hidden="true" class="mdi mdi-lock"></i> Logout</a>
<!-- Log Out -->
 
  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
  @csrf
  </form>
</div> 
</div>
</div>
<div class="col-md-8">
<div class="card card-body account-right">
<div class="widget">
<div class="section-header">
<h5 class="heading-design-h5">
My Profile
</h5>
</div>
<form action="{{route('profile.edit')}}" enctype="multipart/form-data"method="post">
	@csrf
<div class="row">
<div class="col-sm-12">
<div class="form-group">
<label class="control-label"> Name <span class="required">*</span></label>
<input class="form-control border-form-control" name="name" placeholder="Your Name" type="text" value="{{$users->name}}">

<span style="color:orange"> @error('name') {{ $message }}@enderror </span>
</div>
</div>

</div>
<div class="row">
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Mobile Number <span class="required">*</span></label>
<input class="form-control border-form-control"  name="mobile" placeholder="Your Mobile Number" type="number" value="{{$users->mobile}}">

<span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="control-label">Email Address <span class="required">*</span></label>
<input class="form-control border-form-control "  placeholder="{{$users->email}}" disabled="" type="email">

</div>
</div>
<div class="col-md-6">
<div class="form-group">
<label class="form-label">Gender</label>
<select class="form-control custom-select" name="gender">
<option value="" selected>-- Select --</option>
<option value="male" @if($users->gender == 'male')selected @endif>Male</option>
<option value="female" @if($users->gender == 'female')selected @endif>Female</option>
<option value="other" @if($users->gender == 'other')selected @endif>Other</option>
</select>
<span style="color:orange"> @error('gender') {{ $message }}@enderror </span>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label class="form-label">Profile Picture</label>
<input type="file" class="form-control" name="profileImage" id="profileImage"> 
<span style="color:orange"> @error('profileImage') {{ $message }}@enderror </span>
</div>
</div>

</div>

<div class="row">
<div class="col-sm-12 text-right">
<button type="button" class="btn btn-danger btn-lg"> Cancel </button>
<button type="submit" class="btn btn-success btn-lg"> Save Changes </button>
</div> 
<div class="col-sm-6 col-md-4">
<img id="profileImageshow"  style="width:100%;;" >
</div>
</div>
 <input type="hidden" class="form-control" name="oldprofileImage" value="{{$users->profileImage}}"> 
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script>
 function profileImage(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#profileImageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }
    $("#profileImage").change(function() {
  profileImage(this);
  });
</script>
@endsection