@extends('store.layouts.app')
 
@section('content') 
        
       
        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Orders</h3>
                  </div>
                  <div class="table-responsive ">
                    <table class="table card-table  text-nowrap">
                      <thead>
                        <tr>
                          <th class="w-1" style="width:20%;">Order ID</th>
                          <th style="width:50%;">Store</th>
                          <th style="width:10%;">Price</th>
                          <th style="width:10%;">Order Status</th> 
                          <th style="width:10%;">Accept/Cancle</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                         <tr>
                          <td># 787958391</td>

                          <td>Redx Technosoft Store</td>
                          <td>₹6500</td>
                          <td>
                            <span class="status-icon bg-warning"></span> Pending
                          </td>
                            <td>
                            
                        </td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
 @endsection
