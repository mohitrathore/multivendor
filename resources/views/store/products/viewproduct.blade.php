@extends('store.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

           <style>
            li 
            {
            color: #9aa0ac;
            }
           </style> 

        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  
                      <div class="col-sm-4"><h3 class="card-title font-weight-bold">Products</h3> </div>
                      <div class="col-sm-4"> </div>
                      <div class="col-sm-4"> <a href="{{route('store.productview')}}" class="btn btn-info btn-md float-right"> Add New Product </a> </div>
                   
                  </div>
                  
                  <div class="table-responsive ">
                    <table class="table card-table  text-nowrap">
                      <thead>
                        <tr>
                          <th class="w-1" style="width:20%;">Cover Image</th>
                          <th style="width:20%;">Name</th>
                          <th style="width:10%;">Type</th>
                          <th style="width:5%;">Product Code</th>
                          <th style="width:5%;">Discount %</th>
                          <th style="width:5%;">Categories</th>
                          <th style="width:5%;">Quantity</th>
                          <th style="width:10%;">Stoke</th>
                          <th style="width:10%;">Status</th>
                          <th style="width:10%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      	 @forelse ($viewproducts as $product)
                        
                         <tr>
                          <td>
                              <!----modal starts here--->
                              <div id="coverphotomodal{{$product->id}}" class="modal fade" role='dialog' >
                              <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                              <div class="modal-header">
                              
                              <h4 class="modal-title text-capitalize" style="white-space: nowrap;
                              text-overflow: ellipsis;width: 100%;display: block;overflow: hidden">{{$product->name}}</h4>
                             
                              <i class="fa fa-close fa-2x" data-dismiss="modal" aria-hidden="true"></i>
                              </div>
                              <div class="modal-body text-center">
                              <img src="../product_images/{{$product->coverimage}}" style="width:40%;height:40%;">       
                              </div>
                              <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                              </div>
                              </div>
                              </div>
                              <!--Modal ends here--->

                            <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#coverphotomodal{{$product->id}}">
                              <span class="avatar avatar-md" style="background-image: url(../product_images/{{$product->coverimage}})"></span>
                            </a>
                           
                          </td>
                          <td >
                          <span style="white-space: nowrap;
                          text-overflow: ellipsis;display: block;width:350px;overflow: hidden">{{$product->name}}</span>
                          <small>Created {{date('d-m-Y H:i a', strtotime($product->created_at))}}</small>
                          </td>
                          <td>
                          @if($product -> type == "veg")
                          <img src="{{asset('dist/img/veg.png')}}" style="width:50%;">
                          @elseif($product -> type =="non-veg")
                          <img src="{{asset('dist/img/nonveg.png')}}" style="width:50%;">
                          @endif
                          </td>
                          <td>
                            {{$product->productcode}}
                         
                          </td>
                          <td> 
                            {{$product->discount}} %
                          </td>
                          <td>
                          <ul id="nestedlist">
                            <li>
                            @foreach($main as $ma)
                            @if($ma->id == $product->maincategory)
                            <span>{{$ma->name}}</span>
                            @endif
                            @endforeach
                              <ul>
                                <li> 
                                @foreach($second as $sec)
                                @if($sec->id == $product->secondcategory)
                                <span>{{$sec->name}}</span>
                                @endif
                                @endforeach
                                  <ul>
                                    <li> 
                                    @foreach($third as $thri)
                                    @if($thri->id == $product->thirdcategory)
                                    <span>{{$thri->name}}</span>
                                    @endif
                                    @endforeach
                                    </li>
                                  </ul>
                                </li>

                              </ul>
                            </li>
                          </ul>
                          </td>
                          <td>
                          {{$product->quantity}}
                          </td>
                          <td>
                         @if($product->stoke==1)
                         <span class="badge badge-success">In-Stoke</span>
                      @elseif($product->stoke==0)
                      <span class="badge badge-danger">Out-of-Stoke</span>
                      @endif
                          </td>
                          
                          <td>
                            @if( $product -> status == 0)
                            <a href="storeactiveproduct/{{$product->id}}" > <span class="badge badge-danger">Inactive</span> </a>
                            @elseif($product -> status == 1)
                            <a href="storeinactiveproduct/{{$product->id}}"><span class="badge badge-success">Active</span></a>
                            @elseif($product -> status == 2)
                            <span class="badge badge-warning inactivestore" style="cursor:pointer;">Pending</span>
                            @endif
                          </td>
                          <td>
                          @if( $product -> status == 1)
            							<a href="editproductview/{{$product->id}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fa fa-edit"></i></a>
            							<a href="storedeleteproduct/{{$product->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this Product?');"><i class="fa fa-trash"></i></a>
            							@endif		

                          </td>
                            <td>
                            
                        </td>
                        </tr>
                         @empty
                  <tr><td colspan="6"><h4 class="text-center"> No Store Found</h4></td></tr>
                 
                  @endforelse
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          <script>
              $('.inactivestore').click(function() {
              toastr.warning('Your store is not approved by ADMIN Yet !')
              });
          </script>
 @endsection
