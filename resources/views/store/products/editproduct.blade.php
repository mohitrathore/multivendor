@extends('store.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('warning'))
            <script>
            $( document ).ready(function() {
            toastr.warning("{!! session()->get('warning')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  
            <style>
              label {
              font-weight: bold;
              }
              .card-header .card-title {
              margin-bottom: 0;
              font-weight: bold;
              text-transform: uppercase;
              }
              img.thumb {
              width: 20%;
              }
            </style>
        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-1"> </div>
              <div class="col-10">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title"> Edit Product  <br /><br /><b> {{$productdetails->name}}</b></h3>
                    
                  </div>
                  <div class="card-body">
                   
                 
         <form method="post" enctype="multipart/form-data" id="forms" action="{{ route('store.editproduct')}}">
              @csrf
                <input type="hidden" name="cid" value="{{$productdetails->id}}">
               <input type="hidden"  name="productcode" value="PR<?php echo mt_rand();?>" >
    
              <div class="form-group">
                <label for="inputClientCompany">Store</label>
                <select class="form-control custom-select text-capitalize" name="store">
                  <option value="">-- Select Store --</option>
                  @foreach ($stores as $store)
                  <option  value="{{$store->id}}" class="text-capitalize" @if( $productdetails->store_id== $store->id) selected="selected" @endif >
                    {{$no ++}} - {{ $store->name}} </option>

                  @endforeach
                </select>
                <span style="color:orange"> @error('store_id') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputName">Product Name</label>
                <input type="text" id="inputName" name="name" class="form-control" value="{{$productdetails->name}}">
                <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
              </div>
                <!-- Product code -->

              <div class="form-group">
                <label for="inputDescription">Product Description</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="description" >{{ $productdetails->description}}</textarea>
                <span style="color:orange"> @error('description') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Disclaimer</label>
                <textarea id="inputDescription" class="form-control" rows="4" name="disclaimer" >{{ $productdetails-> disclaimer}}</textarea>
                <span style="color:orange"> @error('disclaimer') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Cover Image</label> <br />

               <input type="file" class="form-group" name="coverimagenew" id="coverimage"> 

               <input type="hidden" class="form-group" name="coverimage" value="{{$productdetails->coverimage}}"> 
               <img id="coverimageshow" src="/product_images/{{$productdetails->coverimage}}" style="width:15%;">
               <span style="color:orange"> @error('coverimagenew') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputDescription">Product Images &nbsp;<small class="text-info"><b>Multiple images allowed *</b></small></label> <br />

                <input type="hidden" name="realimages" value="{{$productdetails->images}}">

               <input type="file" class="form-group" name="images[]" type="file" id="file-input" onchange="loadPreview(this)"multiple>
                          <span class="text-danger">{{ $errors->first('images') }}</span>
                          <div id="thumb-output"> </div>
                          <div id="previousImg">
                             @foreach(json_decode($productdetails->images) as $images)
                               <img src="/product_images/{{$images}}" style="width:15%;">

                              @endforeach
                          </div>
                          <br>
               <span style="color:orange"> @error('images') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputStatus"> Product Stoke</label>
                <select class="form-control custom-select" name="stoke">
                  <option value="" selected>Select one</option>
                  <option value="1" @if( $productdetails->stoke== 1) selected="selected" @endif >In Stock</option>
                  <option value="0"  @if( $productdetails->stoke == 0) selected="selected" @endif>Out of Stock</option>
                </select>
                <span style="color:orange"> @error('stoke') {{ $message }}@enderror </span>
              </div> 
              <div class="form-group">
                <label for="inputStatus"> Product Contains</label>
                <select class="form-control custom-select" name="type">
                  <option value="" selected disabled>-- Select one --</option>
                  <option value="veg"  @if( $productdetails->type== "veg") selected="selected" @endif>Vegetarian</option>
                  <option value="non-veg" @if( $productdetails->type== "non-veg") selected="selected" @endif>Non Vegetarian</option>
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputStatus"> Product Criteria</label>
                <select class="form-control custom-select" name="criteria">
                  <option value="" selected disabled>-- Select -- </option>
                  <option value="feature"  @if( $productdetails->criteria== "feature") selected="selected" @endif>Featured</option>
                  <option value="top-saver"  @if( $productdetails->criteria== "top-saver") selected="selected" @endif>Top Saver</option>
                  <option value="best-offer"  @if( $productdetails->criteria== "best-offer") selected="selected" @endif>Best Offer</option>
                 
                </select>
                <span style="color:orange"> @error('type') {{ $message }}@enderror </span>
              </div>
             
              
              <div class="form-group">
                <label for="inputName">Product Discount</label>
                <input type="number" name="discount" class="form-control" value="{{ $productdetails->discount }}" >
                <span style="color:orange"> @error('discount') {{ $message }}@enderror </span>
              </div>
                            <div class="form-group">
                <label for="inputName">Product Quantity</label>
                <input type="number" name="quantity" class="form-control" id="quantity" value="{{ $productdetails->quantity }}">
                 <input type="hidden" id="quantitynew" value="{{ $productdetails->quantity }}">
                <span style="color:orange"> @error('quantity') {{ $message }}@enderror </span>
              </div>
              <div class="col-md-12">
         
            <div class="card-header">
              <h3 class="card-title">Categories</h3>

            </div>
            <div class="card-body">
              
              <div class="form-group">
                <label for="inputEstimatedBudget">Main Category</label>
                <select class="form-control custom-select text-capitalize maincategory" name="maincategory">
                  <option value="" selected>-- Select Main Category --</option>
                  @if($restrictions!==null)
                  <?php $restrict_cat=json_decode($restrictions->restrict_cat,true); ?>
                  @foreach ($maincategories as $maincategory)
                    @for( $i = 0 ; $i < count($restrict_cat) ; $i++)
                      @if($maincategory->id == $restrict_cat[$i])
                        <option  value="{{$maincategory->id}}" class="text-capitalize" @if( $productdetails->maincategory== $restrict_cat[$i]) selected="selected" @endif>
                        {{$maincategory->name}}</option>
                        @else
                      @endif
                    @endfor
                  @endforeach
                @else
                  @foreach ($maincategories as $maincategory)
                    <option  value="{{$maincategory->id}}" class="text-capitalize" @if( $productdetails->maincategory== $maincategory->id) selected="selected" @endif>
                    {{$maincategory->name}}</option>
                  @endforeach
                @endif
                </select>
                <span style="color:orange"> @error('maincategory') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Second Category</label>
                <select class="form-control custom-select text-capitalize secondcategory" name="secondcategory">
                  @foreach ($secondcategories as $secondcategory)
                  <option  value="{{$secondcategory->id}}" @if( $secondcategory->id == $productdetails->secondcategory) selected="selected" @endif class="text-capitalize">
                  {{$secondcategory->name}}</option>
                  @endforeach
                </select>
                <span style="color:orange"> @error('secondcategory') {{ $message }}@enderror </span>
              </div>
              <div class="form-group">
                <label for="inputEstimatedBudget">Third Category</label>
                <select class="form-control custom-select text-capitalize thirdcategory" name="thirdcategory">
                  @foreach ($thirdcategories as $thirdcategory)
                  <option  value="{{$thirdcategory->id}}" @if( $thirdcategory->id == $productdetails->thirdcategory) selected="selected" @endif class="text-capitalize">
                  {{$thirdcategory->name}}</option>
                  @endforeach
                </select>
                <span style="color:orange"> @error('thirdcategory') {{ $message }}@enderror </span>
              </div>
            </div>
            <!-- /.card-body -->
          
        
        </div>
        <!-- Weight based prices --> 
                <div class="col-md-12">

                <div class="card-header">
                <h3 class="card-title">Products Price&nbsp;&nbsp;||&nbsp;&nbsp;Weight&nbsp;&nbsp;</h3>
                               <button type="button" name="add" id="add" class="btn btn-success btn-lg">+</button>
                </div>
             
               <table class="table" id="dynamicTable">  

            
           <?php $i=0; ?>
            @foreach( $weightprices as $weightpriceshow)
           
            <?php $i++; ?>
            <tr>
              <td>
                <div class="input-group">
                  <input type="number" name="weightprice[<?php echo $i-1;?>][weight]" placeholder="Enter Your Weight" id="wrapped" class="form-control" value="{{$weightpriceshow->weight}}">
                  <div class="input-group-addon">
                    <select class="form-control" name="weightprice[<?php echo $i-1;?>][unit]">
                      <option value="">-- Select Weight Unit --</option>
                      <option value="kg" @if( $weightpriceshow->unit == "kg") selected="selected" @endif>Kilogram</option>
                      <option value="gms" @if( $weightpriceshow->unit =="gms") selected="selected" @endif>Gram</option>
                      <option value="litre" @if( $weightpriceshow->unit =="litre") selected="selected" @endif>litre</option>
                      <option value="ml" @if( $weightpriceshow->unit =="ml") selected="selected" @endif>Ml</option>
                    </select>
                  </div>
                </div>
              </td>
              <td>
                <input type="number" name="weightprice[<?php echo $i-1;?>][price]" placeholder="Price" class="form-control"value="{{$weightpriceshow->price}}">
                 <input type="hidden" name="weightprice[<?php echo $i-1;?>][id]" value="{{$weightpriceshow->id}}">
              </td>
            </tr>
            @endforeach
          

        </table>  
                <div class="row">
                <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" value="Update Product" class="btn btn-success float-right">
                </div>
                </div>
               
                </div>
              </form>
            </div>
             
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>

                  </div>
                </div>
              </div>
              <div class="col-1"> </div>
           
<script type="text/javascript">

      $.ajaxSetup({
      headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
      });
      /* Ajax CSRF token */

      $(".maincategory").change(function(){
        var maincategory = $(this).val();
        
        $.ajax({
        url: "<?php echo route('storeproductsecond') ?>",
        method: 'POST',
        data: {maincategory:maincategory},
        success: function(data) {

        $(".secondcategory").html(data);
        }
        });
      }); //second category fetch method

      $(".secondcategory").change(function(){
        var secondcategory = $(this).val();
       
        $.ajax({
        url: "<?php echo route('storeproductthird') ?>",
        method: 'POST',
        data: {secondcategory:secondcategory},
        success: function(data) {

        $(".thirdcategory").html(data);
        }
        });
      });  //third category fetch method

    

var i =<?php echo $i-1;?>;

      $('#quantity').change(function() {
      var old=document.getElementById('quantity').value;
        var quantity=document.getElementById('quantitynew').value=old;;

      }); //update quantity value
      $("#add").click(function(){

        ++i;
        var quantitynew=document.getElementById('quantitynew').value-1;

       //alert(quantity);
        if( i <= quantitynew){
        
         
        $("#dynamicTable").append('<tr><td><div class="input-group"><input type="text" name="weightprice['+i+'][weight]" placeholder="Enter Your Weight" id="wrapped" class="form-control maincategory"><div class="input-group-addon"><select class="form-control" name="weightprice['+i+'][unit]"><option value="" selected>-- Select Weight Unit --</option> <option value="kg">Kilogram</option><option value="gms">Gram</option><option value="litre">litre</option><option value="ml">Ml</option>></select></div></div></td><td><input type="number" name="weightprice['+i+'][price]" placeholder="Price" class="form-control"> <input type="hidden" name="weightprice['+i+'][id]" value=""></td> <td><button type="button" class="btn btn-danger remove-tr">-</button></td></tr>');


          $(document).on('click','.remove-tr', function(){  
          $(this).parents('tr').remove();
          });
        }
        else
        {
          
          swal({
            title: "Hold it Please",
            text: "You Cannot add Product Varient More then Your Product Quantity",
            icon: "warning",
            buttons: [
            'Yes, I dont want my varient details same',
            'No, I want my varient details same'
          ],
          dangerMode: true,
          }).then(function(isConfirm) {
          if (isConfirm) {
            swal({
            title: 'Fine',
            text: 'Your varient details are added back',
            icon: 'success'
            }).then(function() {
           //$('#forms').submit();
            });
          } else {
            swal("Fine", "Your Product Varients and Quantity is Refreshed now", "success");
            $('.remove-tr').parents('tr').remove();
            document.getElementById('quantity').value="";
          }
          });

        }
      });
//preview images
     function loadPreview(input){
       var data = $(input)[0].files; //this file data
       $.each(data, function(index, file){
           if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
               var fRead = new FileReader();
               fRead.onload = (function(file){
                   return function(e) {
                       var img = $('<img/>').addClass('thumb').attr('src', e.target.result); //create image thumb element
                       $('#thumb-output').append(img);
                   };
               })(file);
               fRead.readAsDataURL(file);
           }
       });
   }
    //Cover image load

  function coverimage(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#coverimageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }
    $("#coverimage").change(function() {
  coverimage(this);
  });

</script>


 @endsection
