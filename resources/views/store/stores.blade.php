@extends('store.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

            

        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  
                      <div class="col-sm-4"><h3 class="card-title font-weight-bold">Stores</h3> </div>
                      <div class="col-sm-4"> </div>
                      <div class="col-sm-4"> <a href="{{route('store.addpage')}}" class="btn btn-info btn-md float-right"> Add New Store </a> </div>
                   
                  </div>
                  <div class="table-responsive ">
                    <table class="table card-table  text-nowrap">
                      <thead>
                        <tr>
                          <th class="w-1" style="width:10%;">Cover Image</th>
                          <th class="w-1" style="width:5%;">ID</th>
                          <th style="width:30%;">Store Name</th>
                          <th style="width:5%;">Commission % </th>
                          <th style="width:30%;">Address</th> 
                          <th style="width:10%;">Status</th>
                          <th style="width:10%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      	 @forelse ($stores as $store)
                        
                         <tr>
                          <td>
                              <!----modal starts here--->
                              <div id="coverphotomodal{{$store->id}}" class="modal fade" role='dialog'>
                              <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                              <div class="modal-header">
                              
                              <h4 class="modal-title text-capitalize">{{$store->name}}</h4>
                             
                              <i class="fa fa-close fa-2x" data-dismiss="modal" aria-hidden="true"></i>
                              </div>
                              <div class="modal-body text-center">
                              <img src="../store_images/{{$store->cover_photo}}" style="width:70%;height:70%;">       
                              </div>
                              <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                              </div>
                              </div>
                              </div>
                              <!--Modal ends here--->

                            <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#coverphotomodal{{$store->id}}">
                              <span class="avatar avatar-md" style="background-image: url(../store_images/{{$store->cover_photo}})"></span>
                            </a>
                           
                          </td>
                          <td>#{{$store->id}}</td>

                          <td>{{$store->name}}<small>Created {{date('d-m-Y H:i a', strtotime($store->created_at))}}</small></td>
                          <td>{{$store->commission}}</td>
                          <td>{{$store->address}}</td>
                          <td>
                            @if( $store -> status == 0)
                            <a href="activestore/{{$store->id}}" > <span class="badge badge-danger">Inactive</span> </a>
                            @elseif($store -> status == 1)
                            <a href="inactivestore/{{$store->id}}"><span class="badge badge-success">Active</span></a>@elseif($store -> status == 2)
                            <span class="badge badge-warning inactivestore" style="cursor:pointer;">Pending</span>
                            @endif
                          </td>
                          <td>
                          @if( $store -> status == 1)
            							<a href="store_edit/{{$store->id}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fa fa-edit"></i></a>
            							
                					@endif		
                          <a href="deletestore/{{$store->id}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to delete this Store?');"><i class="fa fa-trash"></i></a>
                          </td>
                            <td>
                            
                        </td>
                        </tr>
                         @empty
                  <tr><td colspan="6"><h4 class="text-center"> No Store Found</h4></td></tr>
                 
                  @endforelse
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          <script>
              $('.inactivestore').click(function() {
              toastr.warning('Your store is not approved by ADMIN Yet !')
              });
          </script>
 @endsection
