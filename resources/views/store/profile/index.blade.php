@extends('store.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('warning'))
            <script>
            $( document ).ready(function() {
            toastr.warning("{!! session()->get('warning')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  
            <style>
                  label {
                  font-weight: bold;
                  }
                  .card-profile .card-header 
                  {
                  height: 295px !important;
                  }
            </style>
     <div class="my-3 my-md-5">
          
           <!----modal starts here--->
          <div id="profileImagemodal" class="modal fade" role='dialog'>
          <div class="modal-dialog modal-lg">
          <div class="modal-content">
          <div class="modal-header">

          <h4 class="modal-title text-capitalize">{{$storeOwner->name}}</h4>

          <i class="fa fa-close fa-2x" data-dismiss="modal" aria-hidden="true"></i>
          </div>
          <div class="modal-body text-center">
          <img src="../profile_images/{{$storeOwner->profileImage}}" style="width:50%;height:50%;">       
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </div>
          </div>
          </div>
          <!--Modal ends here--->

          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                <div class="card card-profile">
                  <div class="card-header" style="background-image: url(../dist/img/grocery-store.png);"></div>
                  <div class="card-body text-center">

                    @if($storeOwner->profileImage !== null)
                    <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#profileImagemodal">
                     <img class="card-profile-img" src="../profile_images/{{$storeOwner->profileImage}}">
                    </a>
                    @else
                    <img class="card-profile-img" src="../dist/img/store.jpg">
                    @endif
                    <h3 class="mb-3 text-capitalize">{{$storeOwner->name}}</h3>
                    
                  </div>
                </div>
                
               
              </div>
              <div class="col-lg-8">
               
                <form class="card" method="post" enctype="multipart/form-data" action="editprofile/{{$storeOwner->id}}">
                  @csrf
                  <div class="card-body">
                    <h3 class="card-title font-weight-bold">Profile</h3>
                    <div class="row">
                      
                      <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <label class="form-label"> Name</label>
                          <input type="text" class="form-control" name="name"  placeholder="Name" value="{{$storeOwner->name}}">
                          <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <label class="form-label">Email Address</label>
                          <input type="email" class="form-control" name="email" placeholder="Email" value="{{$storeOwner->email}}">
                          <span style="color:orange"> @error('email') {{ $message }}@enderror </span>
                        </div>
                      </div>
                     
                      <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <label class="form-label">Mobile Number</label>
                          <input type="number" class="form-control" name="mobile" placeholder="Mobile Number" value="{{$storeOwner->mobile}}">
                          <span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
                        </div>
                      </div>
                    
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Gender</label>
                          <select class="form-control custom-select" name="gender">
                            <option value="" selected>-- Select --</option>
                            <option value="male" @if($storeOwner->gender == 'male')selected @endif>Male</option>
                            <option value="female" @if($storeOwner->gender == 'female')selected @endif>Female</option>
                            <option value="other" @if($storeOwner->gender == 'other')selected @endif>Other</option>
                          </select>
                           <span style="color:orange"> @error('gender') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 ">
                        <div class="form-group">
                          <label class="form-label">Profile Picture</label>
                            <input type="file" class="form-control" name="profileImage" id="profileImage"> 
                            <span style="color:orange"> @error('profileImage') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <img id="profileImageshow"  style="width:65%;float:right;" >
                      </div>
                    
                    </div>
                  </div>
                  <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Update Profile</button>
                  </div>
                     <input type="hidden" class="form-control" name="oldprofileImage" value="{{$storeOwner->profileImage}}"> 
                </form>
              </div>
            </div>
          </div>
        </div>
<script>

  function profileImage(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#profileImageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }
    $("#profileImage").change(function() {
  profileImage(this);
  });

  //aadhar card image load
</script>
 @endsection
