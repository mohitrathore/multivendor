@extends('store.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('warning'))
            <script>
            $( document ).ready(function() {
            toastr.warning("{!! session()->get('warning')!!}")
            });
            </script>
            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  
            <style>
        label {
        font-weight: bold;
        }
            </style>
        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-2"> </div>
              <div class="col-8">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title"> Edit Store</h3>
                    
                  </div>
                  <div class="card-body">
                   
                  <form method="post" enctype="multipart/form-data" action="{{route('store.edit')}}">
                    @csrf

          <div class="form-group">
          <label for="inputName">Store Name</label>
          <input   class="form-control" placeholder="Store Name" name="store_name" type="text" value="{{$editstoredata->name}}">
          <span style="color:orange"> @error('store_name') {{ $message }}@enderror </span>
          </div>
          <div class="form-group">
          <label for="inputDescription">Store Description</label>
          <textarea   class="form-control ckeditor" cols="5" placeholder="Store Description" name="description" rows="5">{{$editstoredata->description}}</textarea>
          <span style="color:orange"> @error('description') {{ $message }}@enderror </span>
          </div>
          
          <div class="form-group">
          <label for="inputProjectLeader">Opening Time</label>
          <input   class="form-control" placeholder="Opening Time" name="opening_time" type="time" value="{{$editstoredata->opening_time}}">
          <span style="color:orange"> @error('opening_time') {{ $message }}@enderror </span>
          </div>
          <div class="form-group">
          <label for="inputProjectLeader">Closing Time</label>
          <input   class="form-control  " placeholder="Closing Time" name="closing_time" type="time" value="{{$editstoredata->closing_time}}">
          <span style="color:orange"> @error('closing_time') {{ $message }}@enderror </span>
          </div>

          <div class="form-group">
          <label for="inputClientCompany">Store Address </label>
          <div class="alert alert-primary" role="alert">
            <b>Note : </b> Type new address or Drag Map icon to change the address !
            </div>
          <input type="text" class="form-control" id="searchInput" placeholder="Address" name="address" class="form-control" autocomplete="nope" value="{{$editstoredata->address}}"> 
          <br />
          
          <div id="address-map-container"  >
          <div style="width: 100%; height: 100%"  class="map" id="map" ></div>

          </div>
          <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
          </div>
          <hr>
          <div class="form-group">
          <label for="inputProjectLeader">Cover Photo  <small class="text-info ml-5">   Jpg, Jpeg, Png allowed only*</small></label>
          <br />
          <input type="file" name="cover_photo" id="coverphoto"> 
          <span style="color:orange"> @error('cover_photo') {{ $message }}@enderror </span>

          <img id="coverphotoshow"  style="width:15%;float:right;" src="/store_images/{{$editstoredata->cover_photo}}">
          <input type="hidden" name="oldcover_photo" value="{{$editstoredata->cover_photo}}" />
          </div>
          <br />
          </div>
          <div class="card-footer">
          <div class="col-12">
          <a href="{{route('store.list')}}" class="btn btn-secondary">Cancel</a>
          <input type="submit" value="Update Store" class="btn btn-success float-right">
          </div>
          </div>
       
          <input type="hidden" name="latitude" id="lat" value="{{$editstoredata->latitude}}"/>
          <input type="hidden" name="longitude" id="lng" value="{{$editstoredata->longitude}}"/>
          <input type="hidden" name="city" id="city" value="{{$editstoredata->city}}"/>
          <input type="hidden" name="state" id="state" value="{{$editstoredata->state}}"/>
          <input type="hidden" name="cid" value="{{$editstoredata->id}}"/>

        </form>
                  </div>
                </div>
              </div>
              <div class="col-2"> </div>
            </div>
            </div>
          </div>

          <script>
  
  //Cover photo  image load

  function coverphoto(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#coverphotoshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }
    $("#coverphoto").change(function() {
  coverphoto(this);
  });

  //aadhar card image load

  function aadharcard(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#aadharcardshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }


  $("#aadharcard").change(function() {
  aadharcard(this);
  });

  //Udhyog Gst   image load
  function udhyog_gst(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#udhyog_gstshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }

  $("#udhyog_gst").change(function() {
  udhyog_gst(this);
  });

/* Image load section ends*/
          </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1z2TkkVNIaGuAIFhOw6Q8TEJJzwSr1IM&libraries=places&callback=initialize" async defer></script>
  <script>

  </script>
  <script src="/js/mapInput.js"></script>

 @endsection
