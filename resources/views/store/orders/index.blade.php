@extends('store.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

           <style>
            li 
            {
            color: #9aa0ac;
            }
           </style> 

        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  
                      <div class="col-sm-4"><h3 class="card-title font-weight-bold">Orders</h3> </div>
                      <div class="col-sm-4"> </div>
                      <div class="col-sm-4"></div>
                   
                  </div>
                  
                  <div class="table-responsive ">
                    <table class="table card-table text-nowrap">
                      <thead>
                        <tr>
                          <th style="width:20%;">Customer Name</th>
                          <th style="width:30%;">Delivery Address</th>
                          <th style="width:10%;">Delivery Date</th>
                          <th style="width:10%;">Order Details</th>
                          <th style="width:20%;">Status</th>
                          <th style="width:10%;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      @forelse ($orders as $order)
                      <!-- Order Details Model -->
                      <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog modal-md">
                      <div class="modal-content">
                      <div class="modal-header">
                      <h4 class="modal-title">Orders</h4>       
                      <i class="far fa-times-circle fa-2x" data-dismiss="modal"></i>          
                      </div>
                      <div class="modal-body p-5">
                      <b>Total : </b>   
                       <i class="fas fa-rupee-sign"></i>
                       <span class="text-capitalize">{{$order->amount}}</span>
                       <br>
                        <b>Discount : </b>   
                       <i class="fas fa-rupee-sign"></i>
                       <span class="text-capitalize">{{$order->discount}}</span>
                      <hr> 
                     @foreach($items as $item)
                     @foreach($products as $product)
                     @if($product->id == $item->productid)
                     <h5 class="p-2">

                     <i class="fas fa-shopping-cart"></i> 
                     <span class="text-capitalize">{{$product->name}}</span> 
                     &nbsp;&nbsp; -  &nbsp;&nbsp;
                     <i class="fas fa-balance-scale"></i>
                     <span>{{$item->size}}</span>  
                      &nbsp;&nbsp; = &nbsp;&nbsp;
                      {{$item->quantity}} x <span>{{$item->price}}</span>   
                   </h5>
                     @endif
                     @endforeach
                     @endforeach
                      </div>
                      <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                      </div>
                      </div>
                      </div>
                      <!-- Order Details Model -->
                         <tr>
                           
                          <td>
                          <span class="text-capitalize">{{$order->name}}</span>
                          <br />
                          <small>Created {{date('d-m-Y H:i a', strtotime($order->created_at))}}</small>
                          </td>
                          <td><span style="white-space: nowrap;
                              text-overflow: ellipsis;width: 100%;display: block;overflow: hidden">{{$order->address}}</span></td>
                          <td>{{$order->date_time}}</td>
                          <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Order Details</button></td>
                          
                          <td> 
                            <span class="badge badge-success">Paid</span>
                            @if($order->store_status == 0)
                            <a href="#" > <span class="badge badge-warning">Pending</span> </a>
                            @endif
                            @if($order->store_status == 1)
                            <a href="#"><span class="badge badge-success">Accepted</span></a>
                             @endif
                            @if($order->store_status==2)
                            <span class="badge badge-danger inactivestore" style="cursor:pointer;">Denied</span>
                            @endif
                          </td>
                          <td> 
                            <a href="{{route('store.accept.orders',$order->id)}}" class="btn btn-primary btn-xs" style="font-size:10px;"><i class="fas fa-check-circle"></i></a>
                          <a href="{{route('store.deny.orders',$order->id)}}" class="btn btn-danger btn-xs" style="font-size:10px;"  onclick="return confirm('Are you sure you want to denied this Order?');"><i class="far fa-times-circle"></i></a>
                          </td>
                            <td>
                            
                        </td>
                        </tr>
                        @empty
                        <tr><td colspan="6"><h4 class="text-center"> No Orders Found Yet!</h4></td></tr>

                        @endforelse
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
          <script>
              $('.inactivestore').click(function() {
              toastr.warning('Your store is not approved by ADMIN Yet !')
              });
          </script>
 @endsection
