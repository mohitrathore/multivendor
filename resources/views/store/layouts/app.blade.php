<!doctype html>
<html lang="en" dir="ltr">
  <head>
       <meta name="csrf-token" content="{{ csrf_token() }}">
       <!-- CSRF Token  -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    
    <title>Store Dashboard | Grocery</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    <!-- Dashboard Core -->
     <link href="{{asset('css/bootstrap-select.css')}}" rel="stylesheet" />
    <link href="{{asset('css/dashboard.css')}}" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
    <style>
        i.fa {
        font-size: 15px !important;
        }
    </style>
  </head>
  <body class="">
    <div class="page">
      <div class="page-main">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="./index.html">
                <h4 class="mt-2"> STORE DASHBOARD</h4>
              </a>
              <div class="d-flex order-lg-2 ml-auto">
                <div class="dropdown d-none d-md-flex">
                  <a class="nav-link icon" data-toggle="dropdown">
                    <i class="fa fa-bell"></i>
                    <span class="nav-unread"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/male/41.jpg)"></span>
                      <div>
                        <strong>Demo</strong> Lorem Ipsum Doller Sit Amit.
                        <div class="small text-muted">10 minutes ago</div>
                      </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item text-center text-muted-dark">Mark all as read</a>
                  </div>
                </div>
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar">
                      <img src="{{asset('dist/img/store.jpg')}}" />
                     
                      </span>
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default"> {{ Auth::user()->name }}</span>
                    </span>
                  
                  </a>    
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="{{route('store.profile')}}">
                      <i class="dropdown-icon fa fa-user"></i> Profile
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fa fa-cogs"></i> Settings
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fa fa-question-circle"></i> Need help?
                    </a>
                   
                    <a class="dropdown-item font-weight-bold" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                          <i class="dropdown-icon fa fa-sign-out" ></i>{{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                    <!--Logout  -->
                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>

        <!---- Navigation Start -->

        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-3 ml-auto">
              </div>
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="{{route('store.index')}}" class="nav-link"><i class="fa fa-home mr-2"></i> Dashboard</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{route('store.list')}}" class="nav-link"><i class="fa fa-shopping-cart mr-2"></i></i> Stores</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{route('product.index')}}" class="nav-link"><i class="fa fa-shopping-bag mr-2 "></i></i> Products</a>
                  </li>
                  <li class="nav-item">
                    <a href="{{route('store.orders')}}" class="nav-link"><i class="fa fa-shopping-bag mr-2"></i></i> Orders</a>
                  </li>
                  <li class="nav-item">
                    <a href="./index.html" class="nav-link"><i class="fa fa-shopping-bag mr-2"></i></i> Earning</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        @yield('content');
            
          </div>
          
        </div>
      </div>


      <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-auto ml-lg-auto">
              <div class="row align-items-center">
                <div class="col-auto">
                  
                </div>
                <div class="col-auto">
                 
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
              Copyright © 2021 <a href="https://redxtechnosoft.com" target="_blank">Redx Technosoft</a> All rights reserved.
            </div>
          </div>
        </div>
      </footer>
    </div>
    
<script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>
<script src="{{asset('js/bootstrap-select.js')}}"></script>
  </body>
</html>
<script>

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

</script>