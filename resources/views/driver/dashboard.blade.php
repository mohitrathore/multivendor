@extends('driver.layouts.app')
 
@section('content') 
        
        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-blue mr-3">
                      <i class="fa fa-check"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">20 </a></h4>
                      <small class="text-muted">Total Delieveries</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fa fa-motorcycle"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">10 </a></h4>
                      <small class="text-muted">Total Ongoing Orders</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-red mr-3">
                      <i class="fa fa-times"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">18 </a></h4>
                      <small class="text-muted">Total Canceled Orders</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 col-lg-3">
                <div class="card p-3">
                  <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-yellow mr-3">
                      <i class="fa fa-money"></i>
                    </span>
                    <div>
                      <h4 class="m-0"><a href="javascript:void(0)">                       <i class="fa fa-inr"></i> 50,555 <small>/-</small></a></h4>
                      <small class="text-muted">Total Earning</small>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            </div>
          </div>
            
          </div>
          
        </div>
      </div>
 @endsection
