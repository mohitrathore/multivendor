@extends('agent.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            @if(session()->has('warning'))
            <script>
            $( document ).ready(function() {
            toastr.warning("{!! session()->get('warning')!!}")
            });
            </script>

            @endif
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            $("#agentverification").modal("show");
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

            <!-- On validation error open modal -->
              @if($errors->has('address') || $errors->has('aadharcard')) 
                        <script>
                        $( document ).ready(function() {
                          $("#agentverification").modal("show");
                        });
                        </script>
                        @endif
            <style>
                  label {
                  font-weight: bold;
                  }
                  .card-profile .card-header 
                  { 
                  height: 295px !important;
                  }
            </style>
     <div class="my-3 my-md-5">

      <!----modal starts here--->
          <div id="profileImagemodal" class="modal fade" role='dialog'>
          <div class="modal-dialog modal-lg">
          <div class="modal-content">
          <div class="modal-header">

          <h4 class="modal-title text-capitalize">{{$agent->name}}</h4>

          <i class="fa fa-close fa-2x" data-dismiss="modal" aria-hidden="true"></i>
          </div>
          <div class="modal-body text-center">
          <img src="../profile_images/{{$agent->profileImage}}" style="width:50%;height:50%;">       
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
          </div>
          </div>
          </div>
          <!--Modal ends here--->

                      <!-- Agent Verification Form -->
                       
                      <div class="form-group">
                      <div class="modal fade" id="agentverification" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                      <div class="modal-content">

                      <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Agent Verification Form</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <form method="post" action="{{route('agent.verification')}}" enctype="multipart/form-data">
                        @csrf
                      <div class="modal-body">
                       
                        <div class="form-group">
                          <label class="form-label"> Name</label>
                          <input type="text" class="form-control" name="name"  placeholder="Name" value="{{$agent->name}}" readonly>
                          <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
                        </div>
                     
                        <div class="form-group">
                          <label class="form-label">Email Address</label>
                          <input type="email" class="form-control" name="email" placeholder="Email" value="{{$agent->email}}" readonly>
                          <span style="color:orange"> @error('email') {{ $message }}@enderror </span>
                        </div>
                    
                       <div class="form-group">
                      <label for="inputClientCompany">Address</label>
                      <textarea class="ckeditor form-control " placeholder="Address" name="address"  autocomplete="nope"></textarea>  

                      <span style="color:orange"> @error('address') {{ $message }}@enderror </span>
                      </div>
                      <div class="form-group">
                      <label for="inputProjectLeader">Aadhaar Card </label>
                        <small class="text-info">   Jpg, Jpeg, Png allowed only* </small>
                      <input type="file" name="aadharcard"> 
                      <br>
                      <span style="color:orange"> @error('aadharcard') {{ $message }}@enderror </span>
                      </div>

                      </div>
                      <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" name="submit" class="btn btn-primary">Submit</button>  
                      </div>
                      </form>
                      </div>
                      </div>
                      </div>

                      <!-- Agent Verification Form -->
  
          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                <div class="card card-profile">
                  <div class="card-header" style="background-image: url(../dist/img/agent-store.png);"></div>
                  <div class="card-body">
                    @if($agent->profileImage !== null)
                    <a href="javascript:void(0);" class="" data-toggle="modal" data-target="#profileImagemodal">

                    <img class="card-profile-img tecxt" src="../profile_images/{{$agent->profileImage}}">
                    </a>
                    @else
                    <img class="card-profile-img" src="../dist/img/agent.jpg">
                    @endif
                    <h3 class="mb-3 text-capitalize">{{$agent->name}}</h3>
                     @if($status->status == 1)
                     <h5> Referral Agent Code : 
                        <span for="varient" class="float-right text-success">
                          <i class="mdi mdi-approval "></i>
                           <i id="agentcodecopy">{{$status->agentcode}}</i> 
                           &nbsp;  
                           <button id="cp_btn" class="btn btn-primary btn-sm">Copy</button>
                        </span> 
                      </h5>
                      @endif

                    
                  </div>
                </div>
                
               
              </div>
              <div class="col-lg-8">
            
            @if(session()->has('verification'))
            <div class="alert alert-success">
           {!! session()->get('verification')!!} 
         </div>
            @endif
           
<form class="card" method="post" enctype="multipart/form-data" action="editprofile/{{$agent->id}}">
                  @csrf
                  <div class="card-body">
                    <h3 class="card-title font-weight-bold">Profile
                      @if($status !== null)

                        @if($status->status == 1)
                        <span for="varient" class="float-right text-success"><i class="mdi mdi-approval "></i> Approved as Agent</span>
                        @elseif($status->status == 0)
                        <span for="varient" class="float-right text-danger"><i class="fa fa-ban "></i> Application Rejected</span>
                        <a href=""></a>
                        @elseif($status->status == 2)
                        <span for="varient" class="float-right text-warning"><i class="fa fa-clock-o"></i> Application Pending</span>
                        <a href=""></a>
                        @else
                        @endif

                      @else

                       <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#agentverification">Click Here to Fill Agent Verification Form</button>
                      @endif
                
                  
                    </h3>
                    <div class="row">
                      
                      <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <label class="form-label"> Name</label>
                          <input type="text" class="form-control" name="name"  placeholder="Name" value="{{$agent->name}}">
                          <span style="color:orange"> @error('name') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <label class="form-label">Email Address</label>
                          <input type="email" class="form-control" name="email" placeholder="Email" value="{{$agent->email}}">
                          <span style="color:orange"> @error('email') {{ $message }}@enderror </span>
                        </div>
                      </div>
                     
                      <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                          <label class="form-label">Mobile Number</label>
                          <input type="number" class="form-control" name="mobile" placeholder="Mobile Number" value="{{$agent->mobile}}">
                          <span style="color:orange"> @error('mobile') {{ $message }}@enderror </span>
                        </div>
                      </div>
                    
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Gender</label>
                          <select class="form-control custom-select" name="gender">
                            <option value="" selected>-- Select --</option>
                            <option value="male" @if($agent->gender == 'male')selected @endif>Male</option>
                            <option value="female" @if($agent->gender == 'female')selected @endif>Female</option>
                            <option value="other" @if($agent->gender == 'other')selected @endif>Other</option>
                          </select>
                           <span style="color:orange"> @error('gender') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 ">
                        <div class="form-group">
                          <label class="form-label">Profile Picture</label>
                            <input type="file" class="form-control" name="profileImage" id="profileImage"> 
                            <span style="color:orange"> @error('profileImage') {{ $message }}@enderror </span>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <img id="profileImageshow"  style="width:65%;float:right;" >
                      </div>
                    
                    </div>
                  </div>
                  @if($agent->status == 1)
                  <div class="card-footer text-right">
                    <button type="submit" class="btn btn-primary">Update Profile</button>
                  </div>
                  @endif
                     <input type="hidden" class="form-control" name="oldprofileImage" value="{{$agent->profileImage}}"> 
                </form>
              </div>
            </div>
          </div>
        </div>
<script>
document.getElementById("cp_btn").addEventListener("click", copy_password);

function copy_password() {
    var copyText = document.getElementById("agentcodecopy");
    var textArea = document.createElement("textarea");
    textArea.value = copyText.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
}
  function profileImage(input) {
  if (input.files && input.files[0]) {
  var reader = new FileReader();

  reader.onload = function(e) {
  $('#profileImageshow').attr('src', e.target.result);
  }

  reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
  }
    $("#profileImage").change(function() {
  profileImage(this);
  });

  //aadhar card image load
</script>
 @endsection
