@extends('agent.layouts.app')
 
@section('content') 
        
       @if(session()->has('success'))
            <script>
            $( document ).ready(function() {
            toastr.success("{!! session()->get('success')!!}")
            });
            </script>
            @endif
            
            @if(session()->has('error'))
            <script>
            $( document ).ready(function() {
            toastr.error("{!! session()->get('error')!!}")
            });
            </script>
            @endif  

            

        <div class="my-3 my-md-5">
          <div class="container">
            <div class="row">
              
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                  
                      <div class="col-sm-12"><h3 class="card-title font-weight-bold"> Stores</h3> </div>
                     
                  </div>
                  <div class="table-responsive ">
                    <table class="table card-table  text-nowrap">
                      <thead>
                        <tr>
                          
                          <th>Store Name</th>
                          <th>Commission % </th>
                          <th>Address</th> 
                          <th>Activity</th> 
                        </tr>
                      </thead>
                      <tbody>
                      	 @forelse ($stores as $store)
                        
                         <tr>
                            <td>
                            {{$store->name}} 
                            <br />  
                            <small>Created {{date('d-m-Y H:i a', strtotime($store->created_at))}}</small>
                            </td>
                            <td>{{$store->commission}}</td>
                            <td>{{$store->address}}</td>
                            <td>Activity comes here</td>
                        </tr>
                         @empty
                  <tr><td colspan="4"><h4 class="text-center"> No Referred Store Found</h4></td></tr>
                 
                  @endforelse

                      
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
 @endsection
