<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCode extends Model
{
    protected $table = "couponcode";
    protected $fillable = ['code','type','status','amount','expirydate','minimum_value'];
     protected $hidden = ['code','status'];
   
}
