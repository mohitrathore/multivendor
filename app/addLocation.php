<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class addLocation extends Model
{
     protected $fillable = [
        'name',
        'address_address',
        'address_latitude',
        'address_longitude',
        'city',
        'state',
    ];
}
