<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	protected $table = "products";
	protected $fillable = ['name','description','disclaimer','coverimage','images','type','discount','criteria','productcode','store_id','maincategory','secondcategory','thirdcategory','quantity','stoke','status'];
}
