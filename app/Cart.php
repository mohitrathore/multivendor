<?php
 
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{    
	use Notifiable;
	 protected $table = "cart";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'productid','user_email','status','quantity','total_sum'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'price_varient_id','productid','user_email','status','quantity','total_sum',
    ];
}
