<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
   protected $table = "address";
    protected $fillable = ['name','email','mobile','city','state','zip','country','nick_name','status','address','landmark'];
     protected $hidden = ['name','email','mobile','city','state','zip','country','nick_name','status','address','landmark'];
}
