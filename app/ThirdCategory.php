<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdCategory extends Model
{
   protected $table = "thirdcategory";
    protected $fillable = ['parentid','name','slug','status'];
}
