<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_items extends Model
{
   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = "order_items";
    protected $fillable = [
      'order_id','product_id','productname','productimage','varientid','varientsize','quantity','price','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'order_id','product_id','productname','productimage','varientid','varientsize','quantity','price','status'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
