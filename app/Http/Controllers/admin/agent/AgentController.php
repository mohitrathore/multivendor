<?php

namespace App\Http\Controllers\admin\agent;

use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Image;
class AgentController extends Controller
{
	    public function agent(){
	    	return view('admin.agent.addagent');
	    }
	    protected function addagent(Request $request){

		$request->validate([
		'fullname'=> 'required|max:20',
		'email'=> 'required|email|unique:users',
		'password'=>'required|size:8',
		'gender'=>'required',
		'address'=>'required',
		'aadharcard'=>'required|mimes:jpg,jpeg,png'
		]);

		if ($verification = $request->file('aadharcard')) {
		$verificationPath = 'store_images/verification'; // upload path
		$verificationImage ='agent'.date('YmdHis') . "." . $verification->getClientOriginalExtension();
		$verification->move($verificationPath, $verificationImage);

		}   // Image upload for vehicle RC

		$agent=DB::table('agents')->insert
		([ 
		'name'=>$request->input('fullname'),
		'email'=>$request->input('email'),
		'address'=>$request->input('address'),
		'aadharcard'=>$verificationImage,
		'agentcode'=>'GRAG'.substr(str_shuffle("0123456789"), 0, 6),
		'status'=>1,
		'created_at'=>now()
		]);


		if($agent)
		{
		//Agent crendentials insert with verification
		$agentcredentials=DB::table('users')->insert
		([ 
		'name'=>$request->input('fullname'),
		'email'=>$request->input('email'),
		'password'=>Hash::make($request->input('password')),
		'role'=>'agent',
		'gender'=>$request->input('gender'),
		'status'=>1,
		'created_at'=>now()
		]);


		if($agentcredentials)
		{
		//email send  to driver
		$data = array('name'=>$request->input('fullname'),'email'=>$request->input('email'),'password'=>$request->input('password'));

		Mail::send('send_login_details', $data, function($message) use($request) {
		$message->to($request->input('email'), $request->input('fullname'))->subject
		('Agent Login Credentials');
		$message->from('lionrajput4433@gmail.com','Admin Grocery');
		});

		return redirect()->route('viewagent')->with('success','Agent Added Successfully');
		}
		else
		{
		return redirect()->back()->with('error',' Error in Agent Add');

		}

		}
		
	}
		   public function viewagent(){

			$viewagents=array(
			'agents'=>DB::table('agents')->paginate(15)
			);
			$viewagentusers=array(
			'agentusers'=>DB::table('users')->where('role','agent')->get());
			return view('admin.agent.viewagent',$viewagents,$viewagentusers);
			}

			//View all Agents

		    //status change to active 
            public function active($id,$email){
            /*$active=DB::table('agents')->where('id',$id)
            ->update(['status'=>'1']);*/
            $activeuser=DB::table('users')->where('email',$email)
            ->update(['status'=>'1','email_verified_at'=>now()]);

            if($activeuser){
            return redirect()->back()->with('success','Agent activated successfully');
            }else{
            return redirect()->back()->with('error',' Agent activation error');
            }

            }
            //status change to Inactive 
            public function inactive($id,$email){
           /* $inactive=DB::table('agents')->where('id',$id)
            ->update(['status'=>'0']);*/
            $inactiveuser=DB::table('users')->where('email',$email)
            ->update(['status'=>'0','email_verified_at'=>null]);

            if($inactiveuser){
            return redirect()->back()->with('success','Agent Inactivated successfully');
            }else{
            return redirect()->back()->with('error',' Agent Inactivation error');
            }

            }
            public function editagent($id){

            $editagents=DB::table('users')->where('id',$id)->first();
            $showagents=[
            'editagentdata'=> $editagents,
            'title'=>'Edit'
            ];
            

            return view('admin.agent.editagent',$showagents);
            } 
            //edit agent view
             protected function agentupdate(Request $request){

			$request->validate([
			'fullname'=>'required',
			'mobile'=>'required',
			'gender'=>'required'
			]);
           if ($profileimage = $request->file('profileimage')) 
			{ 
                  $ProfileImage= 'c'.date('YmdHis') . "." .$profileimage->getClientOriginalName();
				$image_resize = Image::make($profileimage->getRealPath());              
				$image_resize->resize(200, 200);
				$image_resize->save('profile_images/' .$ProfileImage);

				if($request->input('oldprofileimagee') !== null)
				{
				unlink('profile_images/'.$request->input('oldprofileImage'));
				}			
			}   // New Profile Image Upload
			else
			{
			$ProfileImage=$request->input('oldprofileImage');
			}
            $update=DB::table('users')->where('id',$request->input('cid'))
            ->update([
            'name'=>$request->input('fullname'),
            'mobile'=>$request->input('mobile'),
            'gender'=>$request->input('gender'),
            'profileImage'=>$ProfileImage,
            'updated_at'=>now()
            ]);

            if($update){
            return redirect()->route('viewagent')->with('success','Agent edit successfully');
            }else{
            return redirect()->back()->with('error',' Error in Agent update');
            }

            }
             protected function agentverification(Request $request){

			$request->validate([
			'verify'=>'required'
			]);

            $update=DB::table('agents')->where('id',$request->input('cid'))
            ->update([
            'status'=>$request->input('verify'),
            'updated_at'=>now()
            ]);

            if($update){
    			if($request->input('verify') == 0)
    			{
    			 return redirect()->route('viewagent')->with('success','Agent Un-Verified successfully');	
    			}
    			else if($request->input('verify') == 1)
    			{
    				return redirect()->route('viewagent')->with('success','Agent Verified successfully');	
    			}
            }
            else{
            return redirect()->back()->with('error',' Error in Agent update');
            }

            }
            // Agent Verification 

            public function deleteagent($id,$email){

            	 $deleteimages=DB::table('agents')->where('email',$email)->first();

				unlink('store_images/verification/'.$deleteimages->aadharcard); //delete cover photo
				$deleteimageusers=DB::table('users')->where('id',$id)->first();
				unlink('profile_images/'.$deleteimageusers->profileImage); 
				//delete profile image
				$deleteagentcredentials=DB::table('users')->where('id',$id)->delete();
				$deleteagentdetails=DB::table('agents')->where('email',$email)->delete();

				if($deleteagentdetails && $deleteagentcredentials){

				return redirect()->route('viewagent')->with('success','Agent deleted successfully');
				}else{
				return redirect()->back()->with('error',' Error in Agent delete');
				}

            }
            //delete Agents
             
}
