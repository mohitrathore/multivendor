<?php

namespace App\Http\Controllers\admin\user;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
		public function view()
		{
			$usershow=array(
			'users'=>DB::table('users')->where('role','user')->paginate(15)
			);
			return view('admin.users.user',$usershow);
		}
}
