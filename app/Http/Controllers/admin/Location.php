<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class Location extends Controller
{
			public function view()
			{
			return view('admin.location.addlocation');
			}
			//show add location view

			public function insertlocation(Request $request)
			{
			//return $request->input(); 
			$request->validate([
			'name'=>'required',
			'address_address'=>'required'
			]);

			$query= DB::table('location')->insert([
			'name'=>$request->input('name'),
			'address_address'=>$request->input('address_address'),
			'address_latitude'=>$request->input('address_latitude'),
			'address_longitude'=>$request->input('address_longitude'),
			'city'=>$request->input('city'),
			'state'=>$request->input('state'),
			'state'=>1,
			'created_at'=>now()

			]);

			if($query){
			return redirect()->route('viewlocation')->with('success','Location Add successfully');
			}else{
			return redirect()->back()->with('error',' Error in Location add');
			}

			}
			//Insert Data in SQL
            
            public function viewlocation(){

            	$viewlocations=array(
                    'locations'=>DB::table('location')->get()
            	);
            	return view('admin.location.viewlocation',$viewlocations);
            }
            //Print all Locations 

            public function editlocation($id){

            	$editlocations=DB::table('location')->where('id',$id)->first();
            	$showlocation=[
            	'editlocationdata'=> $editlocations,
            	'title'=>'Edit'
            	];
           
            	return view('admin.location.editlocation',$showlocation);
            } 
            // Print edit location
             public function locationupdate(Request $request){
              
				$request->validate([
				'name'=>'required'
				]); 

				 $update=DB::table('location')->where('id',$request->input('cid'))
				->update([
                 'name'=>$request->input('name'),
                 'address_address'=>$request->input('newaddress_address'),
                 'address_latitude'=>$request->input('address_latitude'),
                 'address_longitude'=>$request->input('address_longitude'),
                 'city'=>$request->input('city'),
                 'state'=>$request->input('state'),
			     'updated_at'=>now(),
				]);
			
			if($update){
			return redirect()->route('viewlocation')->with('success','Location edit successfully');
			}else{
			return redirect()->back()->with('error',' Error in Location edit');
			}

             }

             public function deletelocation($id){
             	$deletelocation=DB::table('location')->where('id',$id)->delete();
            
	            if($deletelocation){
				return redirect()->route('viewlocation')->with('success','Location deleted successfully');
				}else{
				return redirect()->back()->with('error',' Error in Location edit');
				}

             }
              protected function active($id){
            $active=DB::table('location')->where('id',$id)
            ->update(['status'=>'1']);

            if($active){
            return redirect()->back()->with('success','Location activated successfully');
            }else{
            return redirect()->back()->with('error',' Location activation error');
            }

            }
            //status change to Inactive 
            protected function inactive($id){
            $inactive=DB::table('location')->where('id',$id)
            ->update(['status'=>'0']);

            if($inactive){
            return redirect()->back()->with('success','Location Inactivated successfully');
            }else{
            return redirect()->back()->with('error',' Location Inactivation error');
            }

            } 

}
