<?php 

namespace App\Http\Controllers\admin\category;

use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\SecondCategory;
use App\MainCategory;
use App\ThirdCategory;
use Image;
class CategoryController extends Controller
{

           
 
    public function viewmaincategory()
    {
    $viewmaincategories=array(
    'categories'=>DB::table('maincategory')->paginate(15));
    

    return view('admin.category.viewmaincategory',$viewmaincategories);
    }  // main category show


    public function maincategory()
    {
    return view('admin.category.maincategory');
    }  //main category add page


    protected function addmaincategory(Request $request)
    {
    $request->validate([
    'maincategory.*.name' => 'required|unique:maincategory',
    'maincategory.*.slug'=> 'required|max:20',
    'maincategory.*.image'=> 'required'
    ]);
 
    foreach ($request->maincategory as $key => $value) {

       if ($image = $value['image']) {

            $image       = $value['image'];
            $filename    = 'main'.time() . "." .$image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(148, 148);
            $image_resize->save('categories_image/' .$filename);

            }  // Product Cover image

            $product=new MainCategory();
            $product->name=$value['name'];
            $product->slug=$value['slug'];
            $product->image=$filename;
            $product->status=1;
            $product->save(); 
    }
    return redirect()->route('viewmaincategory')->with('success', 'Parent Category Created Successfully.');

    } 
    //add main category function


    protected function maincategoryupdate(Request $request){

    $request->validate([
    'name'=> 'required|max:20',
    'slug'=> 'required|max:20',
    'image'=> 'mimes:jpeg,png,jpg',

    ]);
        if  ($request->hasfile('image')) {
            $image       =  $request->file('image');
            $filename    = 'main'.time() . "." .$image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(148, 148);
            $image_resize->save('categories_image/' .$filename);
            unlink('categories_image/'.$request->input('oldimage'));
        }
        else
        {
         $filename=$request->input('oldimage');
        }

    $update=DB::table('maincategory')->where('id',$request->input('cid'))
    ->update([
    'name'=>$request->input('name'),
    'slug'=>$request->input('slug'),
    'image'=>$filename,
    'updated_at'=>now()
    ]);

    if($update){
    return redirect()->route('viewmaincategory')->with('success','Main Category edit successfully');
    }else{
    return redirect()->back()->with('error',' Error in Main Category update');
    } 
    //update main category
    }
    protected function assignmaincategory(Request $request){

    $request->validate([
    'maincategory'=> 'required'
    ]);

    $mainoff=DB::table('maincategory')->where('id',$request->input('cid1'))
    ->update([
    'status'=>0,
    'child'=>null,
    'updated_at'=>now()
    ]);
    $updatesecondary=DB::table('secondcategory')->where('parentid',$request->input('cid1'))
    ->update([
    'parentid'=>$request->input('maincategory'),
    'updated_at'=>now()
    ]);
    $update=DB::table('maincategory')->where('id',$request->input('maincategory'))
    ->update([
    'child'=>1,
    'updated_at'=>now()
    ]);

    if($updatesecondary){
    return redirect()->route('viewmaincategory')->with('success','Category assign successfully');
    }else{
    return redirect()->back()->with('error',' Error in assign Main Category OR You cannot assign parent categroy to itself when parent category does not have any childs');
    } 
    //Assign new  main category to all sub and sub childs
    }

    protected function deletemaincategory($id){
    //delete main category

    }

    //main category status change to Inactive 
    public function inactivemaincategory($id){

    $inactivemain=DB::table('maincategory')->where('id',$id)
    ->update(['status'=>'0']);
    $inactivesecond=DB::table('secondcategory')->where('parentid',$id)
    ->update(['status'=>'0']);


    $selectsecond=DB::table('secondcategory')->select('id')->where('parentid',$id)->get();
    //select subcategory for inactivate third category
    foreach($selectsecond as $second=>$values)
    {
    $inactivethird=DB::table('thirdcategory')->where('parentid',$values->id)
    ->update(['status'=>'0']);
    }

 
    if($inactivemain || $inactivesecond || $inactivethird){
    return redirect()->back()->with('success','Main Category Inactivated successfully');
    }else{
    return redirect()->back()->with('error',' Main Category Inactivation error');
    }
    }
    //main category status change to Inactive 
    public function activemaincategory($id){

    $inactivemain=DB::table('maincategory')->where('id',$id)
    ->update(['status'=>'1']);
    $inactivesecond=DB::table('secondcategory')->where('parentid',$id)
    ->update(['status'=>'1']);
    $selectsecond=DB::table('secondcategory')->select('id')->where('parentid',$id)->get();
    //select subcategory for inactivate third category
    foreach($selectsecond as $second=>$values)
    {
    $inactivethird=DB::table('thirdcategory')->where('parentid',$values->id)
    ->update(['status'=>'1']);
    }

    if($inactivemain || $inactivesecond || $inactivethird){
    return redirect()->back()->with('success','Main Category activated successfully');
    }else{
    return redirect()->back()->with('error',' Main Category activation error');
    }

    }
            //Second category show
            protected function updatesecondcategory(Request $request)
            {

                $request->validate([
                'name'=> 'required|max:20',
                'parentid'=> 'required',
                'slug'=> 'required'
                ]);

                $update=DB::table('secondcategory')
                ->where('id',$request->input('cid'))
                ->update([
                'name'=>$request->input('name'),
                'parentid'=>$request->input('parentid'),
                'slug'=>$request->input('slug'),
                'updated_at'=>now()
                ]);

                if($update)
                {
                return redirect()->route("viewsecondcategory")->with('success','Second Category edit successfully');
                }
                else
                {
                return redirect()->back()->with('error',' Error in Second Category update');
                } 
                //update Second category
            }

            public function viewsecondcategory()
            {
                $viewsecondcategories=array(
                'categories'=>DB::table('secondcategory')->paginate(15)
                );
                $maincategories=array(
                'parents'=>DB::table('maincategory')->get()
                );
                return view('admin.category.viewsecondcategory',$viewsecondcategories,$maincategories);
            }  
            //second category list page

            public function secondcategory()
            {
                $parent=array(
                'categorydetail'=>DB::table('maincategory')->where('status','1')->get());

                return view('admin.category.secondcategory',$parent);
            } // second category add page

            protected function addsecondcategory(Request $request)
            {
                $request->validate([
                'secondcategory.*.name' => 'required|unique:secondcategory',
                'secondcategory.*.parentid' => 'required',
                'secondcategory.*.slug' => 'required'
                ]);

                foreach ($request->secondcategory as $key => $value ) { 

                SecondCategory::create($value);
                $childupdatemain=DB::table('maincategory')->where('id',$value['parentid'])
                ->update(['child'=>'1']);

                }
                return redirect()->route("viewsecondcategory")->with('success', 'Second Category Created Successfully.');


            } //add second category

            protected function activesecondcategory($id)
            {
                $activesecond=DB::table('secondcategory')->where('id',$id)
                ->update(['status'=>'1']);
                $activethirdforsecond=DB::table('thirdcategory')->where('parentid',$id)
                ->update(['status'=>'1']);
                

                if($activesecond || $activethirdforsecond)
                {
                return redirect()->back()->with('success','Second Category activated successfully');
                }
                else
                {
                return redirect()->back()->with('error',' Second Category activation error');
                }

            }  //active second category if main category is already activated
                
                protected function inactivesecondcategory($id)
                {
                    $inactivesecond=DB::table('secondcategory')->where('id',$id)
                    ->update(['status'=>'0']);
                    $inactivethird=DB::table('thirdcategory')->where('parentid',$id)
                    ->update(['status'=>'0']);



                    if($inactivesecond || $inactivethird ){
                    return redirect()->back()->with('success','Second Category Inactivated successfully');
                    }else{
                    return redirect()->back()->with('error',' Second Category Inactivation error');
                    }
                }
                //second category status change to Inactive 

                //assign new parent from second category to third category or inactive mehtod for second catetgories
                protected function assignsecondcategory(Request $request){

                $request->validate([
                'secondcategory'=> 'required'
                ]);

                $secondoff=DB::table('secondcategory')->where('id',$request->input('cid1'))
                ->update([
                'status'=>0,
                'child'=>null,
                'updated_at'=>now()
                ]);
                $updatethird=DB::table('thirdcategory')->where('parentid',$request->input('cid1'))
                ->update([
                'parentid'=>$request->input('secondcategory'),
                'updated_at'=>now()
                ]);
                $update=DB::table('secondcategory')->where('id',$request->input('secondcategory'))
                ->update([
                'child'=>1,
                'updated_at'=>now()
                ]);

                if($updatethird){
                return redirect()->route("viewsecondcategory")->with('success','Category assign successfully');
                }else{
                return redirect()->back()->with('error',' Error in assign Main Category OR You cannot assign parent categroy to itself when parent category does not have any childs');
                } 
                //Assign new  main category to all sub and sub childs
                }


            // End Second Category

                    //Third Category

                    public function viewthirdcategory()
                    {
                    $viewthirdcategories=array(
                    'categories'=>DB::table('thirdcategory')->paginate(15));

                    $maincategories=array(
                    'parents'=>DB::table('secondcategory')->get()
                    );
                    return view('admin.category.viewthirdcategory',$viewthirdcategories,$maincategories);
                    } 
                    //View all third category list

                    public function thirdcategory()
                    {
                    $parent=array(
                    'categorydetail'=>DB::table('secondcategory')->where('status','1')->get());
                    return view('admin.category.thirdcategory',$parent);
                    }
                    //Third category add page

                    protected function addthirdcategory(Request $request)
                    {
                    $request->validate([
                    'thirdcategory.*.name' => 'required|unique:thirdcategory',
                    'thirdcategory.*.parentid' => 'required',
                    'thirdcategory.*.slug' => 'required'
                    ]);

                    foreach ($request->thirdcategory as $key => $value ) {

                     ThirdCategory::create($value);
                     $childupdatesecond=DB::table('secondcategory')->where('id',$value['parentid'])
                ->update(['child'=>'1']);

                    }
                    return redirect()->route('viewthirdcategory')->with('success', 'Third Category Created Successfully.');

                    }
                    protected function updatethirdcategory(Request $request){

                    $request->validate([
                    'name'=> 'required|max:20',
                    'parentid'=> 'required',
                    'slug'=> 'required'
                    ]);

                    $update=DB::table('thirdcategory')->where('id',$request->input('cid'))
                    ->update([
                    'name'=>$request->input('name'),
                    'parentid'=>$request->input('parentid'),
                    'slug'=>$request->input('slug'),
                    'updated_at'=>now()
                    ]);

                    if($update){
                    return redirect()->route('viewthirdcategory')->with('success','Third Category edit successfully');
                    }else{
                    return redirect()->back()->with('error',' Error in Third Category update');
                    } 
                    //update Second category
                    }
                    public function inactivethird($id){

                        $inactivethird=DB::table('thirdcategory')->where('id',$id)
                        ->update(['status'=>'0']);
                        if($inactivethird){
                        return redirect()->back()->with('success','Third Category Inactivated successfully');
                        }else{
                        return redirect()->back()->with('error',' Third Category Inactivation error');
                        }
                    }
                    public function activethird($id)
                    {

                        $activethird=DB::table('thirdcategory')->where('id',$id)
                        ->update(['status'=>'1']);
                        if($activethird){
                        return redirect()->back()->with('success','Third Category activated successfully');
                        }else{
                        return redirect()->back()->with('error',' Third Category activation error');
                        }
                    }



}
