<?php

namespace App\Http\Controllers\admin\couponcode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CouponCode;
class CouponController extends Controller
{
    public function index()
    {    
    	 $coupons=CouponCode::paginate(15);
    	return view('admin.couponcode.coupon')->with(compact('coupons'));
    }
    public function addview()
    {
    	return view('admin.couponcode.addcoupon');
    }
     
     protected function add(Request $request)
		{  

			$request->validate([
			'code'=> 'required|size:6',
			'type'=> 'required',
			'expirydate'=> 'required',
			'amount'=> 'required|numeric|min:1|max:100',
			]); 
			//Banner validation
			  // Banner image
			$coupon=new CouponCode();
			$coupon->code=$request->input('code');
			$coupon->type=$request->input('type');
			$coupon->amount=$request->input('amount');
			$coupon->expirydate=$request->input('expirydate');
			$coupon->status=1;
			$coupon->save();    //coupon code add 

			if($coupon->save())
			{
			 	return redirect()->route('couponlist')->with('success','Coupon Code Added Successfully');

			}
			else
			{
				return redirect()->back()->with('error',' Error in Coupon Code Add');
			}


		
		}
		protected function activecoupon($id)
		{
			$activecoupon=CouponCode::where('id',$id)
                ->update(['status' => 1]);

			if($activecoupon)
			{
				return redirect()->back()->with('success','CouponCode activated successfully');
			}
			else
			{
				return redirect()->back()->with('error',' CouponCode activation error');
			}

		}  //active banner

		protected function inactivecoupon($id)
		{
			$inactivecoupon=CouponCode::where('id',$id)
                ->update(['status' =>0]);


			if($inactivecoupon){
				return redirect()->back()->with('success','CouponCode Inactivated successfully');
			}else{
				return redirect()->back()->with('error',' CouponCode Inactivation error');
			}
		}  //Inactive banner

		protected function deletecoupon($id)
		{
			
			    
            $delete=CouponCode::where('id',$id)->delete();

			if($delete){
			 
				return redirect()->back()->with('success','CouponCode delete successfully');
			}else{
				return redirect()->back()->with('error',' CouponCode delete error');
			}
		}  //delete banner
}
