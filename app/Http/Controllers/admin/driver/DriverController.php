<?php

namespace App\Http\Controllers\admin\driver;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class DriverController extends Controller
{
	public function driver()
	{
	return view('admin.driver.adddriver');
	}

	protected function adddriver(Request $request){

		$request->validate([
		'fullname'=> 'required|max:20',
		'email'=> 'required|email|unique:users',
		'password'=>'required|size:8',
		'number'=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10',
		'vehiclenumber'=>'required|regex:/^[A-Za-z][A-Za-z][0-9][0-9][A-Za-z ][A-Za-z ][0-9][0-9][0-9][0-9]$/',
		'gender'=>'required',
		'address'=>'required',
		'driving_license'=>'required|mimes:jpg,jpeg,png',
		'vehicle_rc'=>'required|mimes:jpg,jpeg,png'
		]);


		if ($verification1 = $request->file('driving_license')) {
		$verification1path = 'store_images/verification'; // upload path
		$verification1Image ='d'.date('YmdHis') . "." . $verification1->getClientOriginalExtension();
		$verification1->move($verification1path, $verification1Image);

		}   // Image upload for Driving license

		if ($verification2 = $request->file('vehicle_rc')) {
		$verification2Path = 'store_images/verification'; // upload path
		$verification2Image ='v'.date('YmdHis') . "." . $verification2->getClientOriginalExtension();
		$verification2->move($verification2Path, $verification2Image);

		}   // Image upload for vehicle RC

		$driver=DB::table('drivers')->insert
		([ 
		'name'=>$request->input('fullname'),
		'email'=>$request->input('email'),
		'number'=>$request->input('number'),
		'address'=>$request->input('address'),
		'verification1'=>$verification1Image,
		'verification2'=>$verification2Image,
		'vehicle_number'=>$request->input('vehiclenumber'),
		'status'=>1,
		'created_at'=>now()
		]);


		if($driver)
		{
		//Driver crendentials insert with verification
		$drivercredentials=DB::table('users')->insert
		([ 
		'name'=>$request->input('fullname'),
		'email'=>$request->input('email'),
		'password'=>Hash::make($request->input('password')),
		'role'=>'driver',
		'gender'=>$request->input('gender'),
		'status'=>1,
		'created_at'=>now()
		]);


		if($drivercredentials)
		{
		//email send  to driver
		$data = array('name'=>$request->input('fullname'),'email'=>$request->input('email'),'password'=>$request->input('password'));

		Mail::send('send_login_details', $data, function($message) use($request) {
		$message->to($request->input('email'), $request->input('fullname'))->subject
		('Driver Login Credentials');
		$message->from('lionrajput4433@gmail.com','Admin Grocery');
		});

		return redirect()->route('viewdriver')->with('success','Driver  Added Successfully');
		}
		else
		{
		return redirect()->back()->with('error',' Error in Driver Add');

		}
		}
		
	}
			public function viewdriver(){

			$viewdrivers=array(
			'drivers'=>DB::table('drivers')->paginate(15)
			);
			return view('admin.driver.viewdriver',$viewdrivers);
			}

			//View all Drivers

		    //status change to active 
            public function active($id,$email){
            $active=DB::table('drivers')->where('id',$id)
            ->update(['status'=>'1']);
           
            $activeusers=DB::table('users')->where('id',$id)
            ->update(['status'=>'1']);


            if($active && $activeusers){
            return redirect()->back()->with('success','Driver activated successfully');
            }else{
            return redirect()->back()->with('error',' Driver activation error');
            }

            }
            //status change to Inactive 
            public function inactive($id,$email){
            $inactive=DB::table('drivers')->where('id',$id)
            ->update(['status'=>'0']);
            
            $inactiveusers=DB::table('users')->where('id',$id)
            ->update(['status'=>'0']);

            if($inactive && $inactiveusers){
            return redirect()->back()->with('success','Driver Inactivated successfully');
            }else{
            return redirect()->back()->with('error',' Driver Inactivation error');
            }

            }
            public function editdriver($id){

            $editdrivers=DB::table('drivers')->where('id',$id)->first();
            $showdrivers=[
            'editdriverdata'=> $editdrivers,
            'title'=>'Edit'
            ];
            

            return view('admin.driver.editdriver',$showdrivers);
            } //edit driver view

            protected function driverupdate(Request $request){

			$request->validate([
			'fullname'=> 'required|max:20',
			'number'=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10',
			'vehiclenumber'=>'required|regex:/^[A-Za-z][A-Za-z][0-9][0-9][A-Za-z ][A-Za-z ][0-9][0-9][0-9][0-9]$/',
			'address'=>'required'
			]);

            $update=DB::table('drivers')->where('id',$request->input('cid'))
            ->update([
            'name'=>$request->input('fullname'),
            'number'=>$request->input('number'),
            'vehicle_number'=>$request->input('vehiclenumber'),
            'address'=>$request->input('address'),
            'updated_at'=>now()
            ]);

            if($update){
            return redirect()->route('viewdriver')->with('success','Driver edit successfully');
            }else{
            return redirect()->back()->with('error',' Error in Driver update');
            }

            }
            // update driver details 

            public function deletedriver($id,$email){
            	$deleteimages=DB::table('drivers')->where('id',$id)->first();
            	
                unlink('store_images/verification/'.$deleteimages->verification1); //delete verification1

                unlink('store_images/verification/'.$deleteimages->verification2); //delete verification2

            $deletedrivercredentials=DB::table('users')->where('email',$email)->delete();
            $deletedriverdetails=DB::table('drivers')->where('id',$id)->delete();

            if($deletedriverdetails && $deletedrivercredentials){

            return redirect()->route('viewdriver')->with('success','Driver deleted successfully');
            }else{
            return redirect()->back()->with('error',' Error in Driver delete');
            }

            }
            //delete driver
 }
