<?php

namespace App\Http\Controllers\admin\store;
use Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class StoreController extends Controller
{
            public function view()
            {   

             
            $agentdetails=array(
            'agentdetail'=>DB::table('agents')->where('status','1')->get());

            $storeowners=array(
            'owners'=>DB::table('users')
            ->where('role','store')
            ->where('status','1')
            ->get());
            return view('admin.store.addstore',$storeowners,$agentdetails);
            }
            //show add store page

            public function list()
            {   
             $owners=DB::table('users')->where('role','store')->paginate(15);
             $restrictions=DB::table('maincategory')->where('status',1)->get();
            return view('admin.store.storeownerlist')->with(compact('owners','restrictions'));
            }

            public function viewowner()
            {   
            return view('admin.store.storeowner');
            }
            //show add store owner  page
            //status change to active  store owner
            protected function owneractive($id){
            $active=DB::table('users')->where('id',$id)
            ->update(['status'=>'1']);

            if($active){
            return redirect()->back()->with('success','Store Owner activated successfully');
            }else{
            return redirect()->back()->with('error',' Store Owner activation error');
            }

            }
            //status change to Inactive store owner
            protected function ownerinactive($id){
            $inactive=DB::table('users')->where('id',$id)
            ->update(['status'=>'0']);

            if($inactive){
            return redirect()->back()->with('success','Store Owner Inactivated successfully');
            }else{
            return redirect()->back()->with('error',' Store Owner Inactivation error');
            }

            }

            protected function addowner(Request $request){
           
            $request->validate([ 

            'fullname'=> 'required|max:20',
            'email'=> 'required|email|unique:users',
            'password'=>'required|max:15',
            'gender'=>'required',
            'mobile'=> 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:users',
            
            ]);

            $ownercredentials=DB::table('users')->insert
            ([ 
            'name'=>$request->input('fullname'),
            'email'=>$request->input('email'),
            'mobile'=>$request->input('mobile'),
            'password'=>Hash::make($request->input('password')),
            'role'=>'store',
            'gender'=>$request->input('gender'),
            'status'=>1,
            'created_at'=>now()
            ]);
             
            
            if($ownercredentials)
            {
                  //email send  to store owner 
            $data = array('name'=>$request->input('fullname'),'email'=>$request->input('email'),'password'=>$request->input('password'));

            Mail::send('send_login_details', $data, function($message) use($request) {
            $message->to($request->input('email'), $request->input('fullname'))->subject
            ('Store Owner Login Credentials');
            $message->from('lionrajput4433@gmail.com','Admin Grocery');
            });

            return redirect()->route('storeowner.list')->with('success','Owner Added Successfully');
            }
            else
            {
            return redirect()->back()->with('error',' Error in Owner Add');
            }
            

            }

            //Add store details
            protected function add(Request $request)
            {

            $request->validate([

            'email'=> 'required|email',
            'store_name'=>'required|max:20',
            'address'=> 'required',
            'opening_time'=>'required',
            'closing_time'=> 'required',
            'commission'=>'required|numeric',
            'description'=> 'required',
            'cover_photo'=> 'required|mimes:jpg,jpeg,png',
            'aadharcard'=> 'required|mimes:jpg,jpeg,png',
            'udhyog_gst'=> 'required|mimes:jpg,jpeg,png'

            ]);

            $locations=DB::table('location')->select('city','state')
            ->where('status',1)
            ->where('city',$request->input('city'))
            ->where('state',$request->input('state'))
            ->first();

            if($locations == null)
            {
            return redirect()->back()->with('error','Your Store location is out of our reach area ! Please change the address again');
            
            }
            else
            {
                  if ($cover_photo = $request->file('cover_photo')) {
                  $coverphotoPath = 'store_images/'; // upload path
                  $coverImage ='c'.date('YmdHis') . "." . $cover_photo->getClientOriginalExtension();
                   }   // Image upload for cover photo

                  if ($aadharcard = $request->file('aadharcard')) {
                  $aadharcardPath = 'store_images/verification'; // upload path
                  $aadharcardImage ='aa'.date('YmdHis') . "." . $aadharcard->getClientOriginalExtension();
                     }   // Image upload for aadhar card

                  if ($udhyog_gst = $request->file('udhyog_gst')) {
                  $udhyog_gst_Path = 'store_images/verification'; // upload path
                  $udhyog_gstImage ='u_gst'.date('YmdHis') . "." . $udhyog_gst->getClientOriginalExtension();
                   }   // Image upload for udhyog & Gst
 
            $query=DB::table('stores')->insert
            ([
            'email'=>$request->input('email'),
            'name'=>$request->input('store_name'),
            'address'=>$request->input('address'),
            'opening_time'=>$request->input('opening_time'),
            'closing_time'=>$request->input('closing_time'),
            'commission'=>$request->input('commission'),
            'description'=>$request->input('description'),
            'latitude'=>$request->input('latitude'),
            'longitude'=>$request->input('longitude'),
            'city'=>$request->input('city'),
            'state'=>$request->input('state'),
            'agent'=>$request->input('agent'),
            'cover_photo'=>$coverImage,
            'aadharcard'=>$aadharcardImage,
            'udhyog_gst'=>$udhyog_gstImage,
            'status'=>1,
            'created_at'=>now()
            ]);

            if($query)
            {
               $udhyog_gst->move($udhyog_gst_Path, $udhyog_gstImage); // Udhyog GST photo
               $aadharcard->move($aadharcardPath, $aadharcardImage); // Aadhar Card photo
               $cover_photo->move($coverphotoPath, $coverImage); // Cover photo
             return redirect()->route('viewstore')->with('success','Store Added Successfully');
            }
            else
            {
            return redirect()->back()->with('error',' Error in Store Add');
            }
         }
            }

            public function viewstore(){

            $viewstores=array(
            'stores'=>DB::table('stores')->paginate(15)
            );
            return view('admin.store.viewstore',$viewstores);
            }

            //View all Stores

            //edit store

            //status change to active 
            protected function active($id){
            $active=DB::table('stores')->where('id',$id)
            ->update(['status'=>'1']);

            if($active){
            return redirect()->back()->with('success','Store activated successfully');
            }else{
            return redirect()->back()->with('error',' Store activation error');
            }

            }
            //status change to Inactive 
            protected function inactive($id){
            $inactive=DB::table('stores')->where('id',$id)
            ->update(['status'=>'0']);

            if($inactive){
            return redirect()->back()->with('success','Store Inactivated successfully');
            }else{
            return redirect()->back()->with('error',' Store Inactivation error');
            }

            } 
            public function editstore($id){

            $editstores=DB::table('stores')->where('id',$id)->first();
            $showstores=[
            'editstoredata'=> $editstores,
            'title'=>'Edit'
            ];


            return view('admin.store.editstore',$showstores);
            } 

            protected function storeupdate(Request $request){

            $request->validate([

            'store_name'=>'required|max:20',
            'address'=> 'required',
            'opening_time'=>'required',
            'closing_time'=> 'required',
            'commission'=>'required|numeric',
            'description'=> 'required',
            'cover_photo'=> 'mimes:jpg,jpeg,png',

            ]);

            $locations=DB::table('location')->select('city','state')
            ->where('status',1)
            ->where('city',$request->input('city'))
            ->where('state',$request->input('state'))
            ->first();

            if($locations == null)
            {
            return redirect()->back()->with('error','Your Store location is out of our reach area ! Please change the address again');
            
            }
            else
            { 
            
            if ($cover_photo = $request->file('cover_photo')) {
                  $coverphotoPath = 'store_images/'; // upload path
                  $coverImage ='c'.date('YmdHis') . "." . $cover_photo->getClientOriginalExtension();
                   $cover_photo->move($coverphotoPath, $coverImage); // Cover photo
                   unlink('store_images/'.$request->input('oldcover_photo')); //delete cover photo
                   }
                   else{
                        $coverImage=$request->input('oldcover_photo');
                   }   // Image Edit for cover photo
             
            $update=DB::table('stores')->where('id',$request->input('cid'))
            ->update([
            'name'=>$request->input('store_name'),
            'address'=>$request->input('address'),
            'opening_time'=>$request->input('opening_time'),
            'closing_time'=>$request->input('closing_time'),
            'commission'=>$request->input('commission'),
            'description'=>$request->input('description'),
            'latitude'=>$request->input('latitude'),
            'longitude'=>$request->input('longitude'),
            'cover_photo'=>$coverImage,
            'city'=>$request->input('city'),
            'state'=>$request->input('state'),
            'agent'=>$request->input('agent'),
            'updated_at'=>now()
            ]);

            if($update)
            {
           
            return redirect()->route('viewstore')->with('success','Store edit successfully');
            }else{
            return redirect()->back()->with('error',' Error in store update');
            }
      }

            }
            // update store details 

            public function deletestore($id){

                  $deleteimages=DB::table('stores')->where('id',$id)->first();


                  unlink('store_images/'.$deleteimages->cover_photo); //delete cover photo
                  unlink('store_images/verification/'.$deleteimages->aadharcard); //delete aadharcard
                  unlink('store_images/verification/'.$deleteimages->udhyog_gst); //delete udhyog gst

                  $deletestore=DB::table('stores')->where('id',$id)->delete();

            if($deletestore){

            return redirect()->route('viewstore')->with('success','Store deleted successfully');
            }else{
            return redirect()->back()->with('error',' Error in store delete');
            }

            } 
            //delete store

            public function addrestrictions(Request $request)
            {

            $request->validate([
            'restrictions'=>'required',
            ]);
             $restrictions=DB::table('restrictions')->insert([ 
            'email'=>$request->input('email'),
            'restrict_cat'=>json_encode($request->input('restrictions')),
            'status'=>1,
            'created_at'=>now()
            ]);

            if($restrictions)
            {
             
            return redirect()->route('storeowner.list')->with('success','Owner Category Restricted added Successfully');
            }
            else
            {
            return redirect()->back()->with('error',' Error in Owner Category Restricted');
            }
            }
  }
              