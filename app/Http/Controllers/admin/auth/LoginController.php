<?php

namespace App\Http\Controllers\admin\auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
  /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "route('admin.dashboard')";

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
           
            $this->middleware('guest')->except('logout');
    }
    
    public function loginshow()
    {
      return view('admin.auth.login');
    }
    
    public function login(Request $request)
    {
      // Validate the form data
      $request->validate([
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      
      // Attempt to log the user in
      if (Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $request->input('remember'))) {
         $user = Auth()->guard('admin')->user();
        // if successful, then redirect to their intended location
       return redirect()->intended(route('admin.dashboard'));
      } 
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    
    public function logout()
    {
        Auth::guard('admin')->logout();
       
        return redirect('/');
    }
}
