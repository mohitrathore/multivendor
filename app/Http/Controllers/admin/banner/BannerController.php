<?php

namespace App\Http\Controllers\admin\banner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Banner;
use Image;

class BannerController extends Controller
{   
		public function index()
		{
            $banners=DB::table('banners')->paginate(15);
			return view('admin.banner.viewbanner')->with(compact('banners'));	
		}
		public function viewaddbanner()
		{

			return view('admin.banner.addbanner');	
		} 

		protected function add(Request $request)
		{
			$request->validate([
			'name'=> 'required|max:20',
			'image'=> 'required|mimes:jpg,jpeg,png',
			'type'=> 'required',
			]); 
			//Banner validation
			if ($banner = $request->file('image')) {
			$bannernImage =date('YmdHis') . "." . $banner->getClientOriginalExtension();
			$image_resize = Image::make($banner->getRealPath());              
            $image_resize->resize(1159, 398);
            $image_resize->save('dist/img/ad_banner/' .$bannernImage);

           

			}  // Banner image
			$banner=new Banner();
			$banner->name=$request->input('name');
			$banner->image=$bannernImage;
			$banner->type=$request->input('type');
			$banner->status=1;
			$banner->save();    //banner add 

			if($banner->save())
			{
			 	return redirect()->route('banners')->with('success','Banner Added Successfully');

			}
			else
			{
				return redirect()->back()->with('error',' Error in Banner Add');
			}


		
		}
		
		public function vieweditbanner($id)
		{      
			$editbanner=DB::table('banners')->where('id',$id)->first();

			return view('admin.banner.editbanner')->with(compact('editbanner'));	
		} 

		protected function update(Request $request)
		{
			$request->validate([
			'name'=> 'required|max:20',
			'image'=> 'mimes:jpg,jpeg,png',
			'type'=> 'required',
			]); 
			//Banner validation
			if ($banner = $request->file('image')) {
			$bannerPath = 'dist/img/ad_banner'; // upload path
			$bannernImage =date('YmdHis') . "." . $banner->getClientOriginalExtension();
			$banner->move($bannerPath, $bannernImage);
            unlink('dist/img/ad_banner/'.$request->input('oldimage'));
			}  // Banner image
			else
			{
             $bannernImage=$request->input('oldimage');
			}

			$banner=Banner::find($request->input('cid'));
			$banner->name=$request->input('name');
			$banner->image=$bannernImage;
			$banner->type=$request->input('type');
			$banner->status=1;
			$banner->save();    //banner add 

			if($banner->save())
			{
			 	return redirect()->route('banners')->with('success','Banner Added Successfully');

			}
			else
			{
				return redirect()->back()->with('error',' Error in Banner Add');
			}


		
		}
		protected function activebanner($id)
		{
			$activebanner=DB::table('banners')->where('id',$id)
			->update(['status'=>'1']);

			if($activebanner)
			{
				return redirect()->back()->with('success','Banner activated successfully');
			}
			else
			{
				return redirect()->back()->with('error',' Banner activation error');
			}

		}  //active banner

		protected function inactivebanner($id)
		{
			$inactivebanner=DB::table('banners')->where('id',$id)
			->update(['status'=>'0']);

			if($inactivebanner){
				return redirect()->back()->with('success','Banner Inactivated successfully');
			}else{
				return redirect()->back()->with('error',' Banner Inactivation error');
			}
		}  //Inactive banner

		protected function deletebanner($id)
		{
			
			 $deleteimage=DB::table('banners')->where('id',$id)->first();
             unlink('dist/img/ad_banner/'.$deleteimage->image); //delete image
			    
            $delete=DB::table('banners')->where('id',$id)->delete();

			if($delete){
			 
				return redirect()->back()->with('success','Banner delete successfully');
			}else{
				return redirect()->back()->with('error',' Banner delete error');
			}
		}  //delete banner
}
