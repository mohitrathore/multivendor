<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class AgentController extends Controller
{   
	
    public function index()
    {
        $agent=DB::table('users')->where('email',Auth::user()->email)->first();
        return view('agent.dashboard')->with(compact('agent'));
    }

    public function verification(Request $request)
    {
       $request->validate([
            'address'=>'required',
            'aadharcard'=> "required|mimes:jpg,jpeg,png",
            ]);

       if ($verification = $request->file('aadharcard')) {
		$verificationPath = 'store_images/verification'; // upload path
		$verificationImage ='agent'.date('YmdHis') . "." . $verification->getClientOriginalExtension();
		$verification->move($verificationPath, $verificationImage);

		}   // Image upload for vehicle RC
		$agent=DB::table('agents')->insert
		([ 
		'name'=>$request->input('name'),
		'email'=>$request->input('email'),
		'address'=>$request->input('address'),
		'aadharcard'=>$verificationImage,
		'agentcode'=>'GRAG'.substr(str_shuffle("0123456789"), 0, 6),
		'status'=>2,
		'created_at'=>now()
		]);


		if($agent)
		{
			return redirect()->back()->with('verification','Agent Verification Details Send to Our Verification Department ! Please check regulary into your profile section for update');
		}
		else
		{
			return redirect()->back()->with('error',' Error in Agent Verification Submit form');
		}

    }
}
