<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Image;
class ReferredController extends Controller
{

	public function index()
	{  

	$agentcode=DB::table('agents')->select('agentcode')->where('email',Auth::user()->email)->first();

	$stores=DB::table('stores')->where('agent',$agentcode->agentcode)->get();

	return view('agent.reference.referred')->with(compact('stores','agentcode'));
	}
}
