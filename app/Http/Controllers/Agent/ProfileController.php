<?php
 
namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Image;

class ProfileController extends Controller
{
    public function index()
    {   
        $status=DB::table('agents')->where('email',Auth::user()->email)->first();
    	$agent=DB::table('users')->where('email',Auth::user()->email)->first();
    	return view('agent.profile.profile')->with(compact('agent','status'));
    }

    protected function edit(Request $request,$id)
    {
        $request->validate([

            'name'=>'required|max:200',
            'email'=> "required|email|unique:users,email,".$id,
            'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10',
            'gender'=> 'required',
            'profileImage'=> 'mimes:jpg,jpeg,png'
            ]);

			if ($profileimage = $request->file('profileImage')) 
			{ 
                  $ProfileImage= 'c'.date('YmdHis') . "." .$profileimage->getClientOriginalName();
				$image_resize = Image::make($profileimage->getRealPath());              
				$image_resize->resize(200, 200);
				$image_resize->save('profile_images/' .$ProfileImage);

				if($request->input('oldprofileImage') !== null)
				{
				unlink('profile_images/'.$request->input('oldprofileImage'));
				}			
			}   // New Profile Image Upload
			else
			{
			$ProfileImage=$request->input('oldprofileImage');
			}



			$update=DB::table('users')->where('email',Auth::user()->email)
            ->update([
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'profileImage'=>$ProfileImage,
            'mobile'=>$request->input('mobile'),
            'gender'=>$request->input('gender'),
            'updated_at'=>now()
            ]);

            if($update){

            return redirect()->route('agent.profile')->with('success','Profile Updated Successfully');
            }else{
            return redirect()->back()->with('error',' Profile Updation Error');
            }
    }
    
}
