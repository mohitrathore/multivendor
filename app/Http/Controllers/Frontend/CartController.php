<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Products;
use App\Weight_Price;
use App\CouponCode;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\Session;
class CartController extends Controller
{   

    public function cart()
    { 
        
        $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

        $CartCount=count($carts);

        $produtid= $carts->pluck('productid')->toArray();

        $varientid= $carts->pluck('price_varient_id')->toArray();

        $products= Products::whereIn('id',$produtid)->get();

        return view('frontend.cartview')->with(compact('CartCount','products','carts'));
    }

public function cartupdate(Request $request)
    {        
        if($request->ajax())
        {


        $products= Products::select('quantity','name')->where('id',$request->pid)->where('status',1)->first();
        $productprice= Weight_Price::select('price','saleprice')->where('id',$request->vid)->first();
        if( $request->task == 1)
        {
            if($request->quant > $products->quantity)
            {
                return response()->json([0,$products->name],200);
                //quantity not allowed

            }
            else
            {   
                $carts = Cart::where('productid',$request->pid)
                ->where('price_varient_id',$request->vid)->first();


                $quantity=$carts->quantity + 1;
                $price=$carts->total_sum + $productprice->saleprice;
                $savingamount=$productprice->price - $productprice->saleprice;
                $savings=$carts->total_saving + $savingamount;
               
                $cartquant=Cart::where('productid',$request->pid)
                ->where('price_varient_id',$request->vid)
                ->update(['quantity' => $quantity,'total_sum'=>$price,'total_saving'=>$savings]);

                $cartnew = Cart::where('user_email',Auth::user()->email)->get();
                $total=$cartnew->sum('total_sum');
                $totalsaving=$cartnew->sum('total_saving');


                return response()->json([1,$quantity,$total,$totalsaving],200);

            }
        }
        else if($request->task == 0)
        {
            if($request->quant == 0 || $request->quant < 0)
            {
            $cartquant=Cart::where('productid',$request->pid)
            ->where('price_varient_id',$request->vid)
            ->delete();
            if($cartquant)
            {
            return response()->json([0,$products->name],200);
            }

            }
            else
            {  
            $carts = Cart::where('productid',$request->pid)
            ->where('price_varient_id',$request->vid)->first();


            $quantity=$carts->quantity - 1;
            $price=$carts->total_sum - $productprice->saleprice;
            $savingamount=$productprice->price - $productprice->saleprice;
            $totalsave=$carts->total_saving - $savingamount;
            $cartquant=Cart::where('productid',$request->pid)
            ->where('price_varient_id',$request->vid)
            ->update(['quantity' => $quantity,'total_sum'=>$price,'total_saving'=>$totalsave]);

           $cartnew = Cart::where('user_email',Auth::user()->email)->get();
           $total=$cartnew->sum('total_sum');
           $totalsave=$cartnew->sum('total_saving');

            return response()->json([1,$quantity,$total,$totalsave],200);

            }
        }
    }
}

    public function cartadd(Request $request)
    { 

        if($request->ajax())
        { 
        //Cart Count
         
        $cartcheck = Cart::where('user_email',Auth::user()->email)
         ->where('productid',$request->pid)
         ->where('price_varient_id',$request->vid)
         ->get();

        $productstoke = Products::select('stoke')->where('id',$request->pid)->first();
        $productprice = Weight_Price::where('id',$request->vid)->where('productid',$request->pid)->first();
        
        $CartCount=count($cartcheck);

        if($CartCount > 0 || $productstoke->stoke == 0)
        {
          return response()->json([2],200);
        }
        else
        {   
            $total_saving=$productprice->price - $productprice->saleprice;
            
            $cart=new Cart;
            $cart->productid=$request->pid;
            $cart->user_email=Auth::user()->email;
            $cart->price_varient_id=$request->vid;
            $cart->price=$productprice->price;
            $cart->size=$productprice->weight.''.$productprice->unit;
            $cart->saleprice=$productprice->saleprice;
            $cart->total_sum=$productprice->saleprice;
            $cart->total_saving=$total_saving;
            $cart->quantity=1;
            $cart->created_at=now();
            $cart->save();

        if( $cart->save() )
            { 
                    $cartcount= Cart::where('user_email',Auth::user()->email)
                    ->get();
                    $cartcountshow=count($cartcount);
                   return response()->json([1,$cartcountshow],200);
            }
            else
            {
                   return response()->json([0],200);

            }  
        }
        


    }
   }

   public function cartdelete($pid,$vid)
   {    $products= Products::select('quantity','name')->where('id',$pid)->where('status',1)->first();
         $cartdelete=Cart::where('productid',$pid)
            ->where('price_varient_id',$vid)
            ->delete();
          ;

            if($cartdelete)
            {
            return redirect()->back()->with('success',$products->name.' removed from your cart');
            }else{
            return redirect()->back()->with('error',' Profile Updation Error');
            }
   }

   public function couponcheck(Request $request)
   {     

        if($request->ajax())
        { 
            $couponcode = CouponCode::where('code',$request->code)->where('status',1)->first();
           
            if($couponcode !== null)
            {
                return response()->json([1,$couponcode],200);
            }
            else
            {
               return response()->json([0,'Coupon code Invalid'],200);
            }
           
           
        }
   }
   public function gotocheckoutfromcart(Request $request)
   {   
     
            $api = new Api('rzp_test_RY9YnlOGgxjtXk', '1EQIPRQQtTXgZ8ml10mxjOYK');
            $order  = $api->order->create(array('receipt' => Auth::user()->name, 'amount' => $request->checkout * 100 , 'currency' => 'INR')); // Creates order
            $orderId = $order['id'];

            Session::put('checkout',$request->checkout);
            Session::put('checkout_disc',$request->checkout_disc);
            Session::put('order_id',$orderId);

       return response()->json([0,'checkout page'],200);
   }
}
