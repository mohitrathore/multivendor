<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\User;
use App\Address;
class AddressController extends Controller
{
    
     public function index()
    { 
        
        $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

        $CartCount=count($carts);

        $users=User::where('email',Auth::user()->email)
               ->first();

         $addressall=Address::where('email',Auth::user()->email)->get();

        return view('frontend.address.address')->with(compact('CartCount','users','addressall'));
    }

    public function view()
    { 
        
        $carts = Cart::where('user_email',Auth::user()->email)
               ->get();
        $CartCount=count($carts);

        $users=User::where('email',Auth::user()->email)
               ->first();

        return view('frontend.address.add_address')->with(compact('CartCount','users'));
    }
    protected function add(Request $request)
    {


        $request->validate([
            'address'=>'required|max:200',
            'name'=>'required|max:200',
            'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10|unique:address',
            'city'=> 'required',
            'state'=> 'required',
            'country'=> 'required',
            'zipcode'=> 'required',
            'landmark'=> 'required',
            'nickname'=> 'required'
            ]);

			$address= new Address();
			$address->name=$request->input('name');
			$address->email=Auth::user()->email;
			$address->mobile=$request->input('mobile');
			$address->city=$request->input('city');
			$address->state=$request->input('state');
			$address->country=$request->input('country');
			$address->zip=$request->input('zipcode');
			$address->landmark=$request->input('landmark');
			$address->nick_name=$request->input('nickname');
			$address->address=$request->input('address');
			$address->save();

            if($address->save()){

            return redirect()->back()->with('success','Address Added Successfully');
            }else{
            return redirect()->back()->with('error',' Address Added Error');
            }
    } 
    public function editview($id)
    {  

        $carts = Cart::where('user_email',Auth::user()->email)
               ->get();
        $CartCount=count($carts);

        $users=User::where('email',Auth::user()->email)
               ->first();

        $address=Address::where('id',$id)->first();

        if($address->email !== Auth::user()->email){
          return redirect()->back()->with('error','Sorry, You are not Unauthorize to edit this address');
        }
        
        return view('frontend.address.edit_address')->with(compact('CartCount','users','address'));
    }
    public function edit(Request $request)
    {  
               $request->validate([

            'name'=>'required|max:200',
            'mobile'=>"required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10",
            'city'=> 'required',
            'state'=> 'required',
            'country'=> 'required',
            'zipcode'=> 'required',
            'landmark'=> 'required',
            'nickname'=> 'required',
            'address'=> 'required'
            ]);

            $update=Address::where('id',$request->input('cid'))
            ->update([
            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'mobile'=>$request->input('mobile'),
            'city'=>$request->input('city'),
            'state'=>$request->input('state'),
            'zip'=>$request->input('zipcode'),
            'landmark'=>$request->input('landmark'),
            'nick_name'=>$request->input('nickname'),
            'updated_at'=>now()
            ]);

            if($update){

            return redirect()->route('address.view')->with('success','Address Updated Successfully');
            }else{
            return redirect()->back()->with('error',' Address Updation Error');
            }
    }

    public function delete($id)
    { 
        $delete=Address::where('id',$id)->delete();

            if($delete)
            {
            return redirect()->back()->with('success','Address Deleted Successfully');
            }
            else
            {
            return redirect()->back()->with('error',' Address Delete Error');
            }
        
    }
}
