<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Products;
use App\Address;
use App\Orders;
use App\Order_items;
use Razorpay\Api\Api;
use Illuminate\Support\Facades\Session;
class CheckoutController extends Controller
{
    public function index()
    {  
    	 $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

        $CartCount=count($carts);
        
        if($CartCount == 0 || Session::get('checkout') == "" || Session::get('order_id') == "")
        {
           return redirect()->route('shop.cart')->with('error','Sorry, You cannot access this page OR Your Cart is empty !');
        }
        else
        {
          $produtid= $carts->pluck('productid')->toArray();

        $varientid= $carts->pluck('price_varient_id')->toArray();

        $products= Products::whereIn('id',$produtid)->get();

        $addresses= Address::where('email',Auth::user()->email)->get();

        $addressstatuscheck=Address::select('status','address')->where('email',Auth::user()->email)->where('status',1)->first();
        
    return view('frontend.checkout')->with(compact('CartCount','products','carts','addresses','addressstatuscheck'));
        }
        
}
   

      public function deliverhere($id)
      {     
          $addressall = Address::where('email',Auth::user()->email)->update(['status'=>0]);
          $address = Address::where('id',$id)->update(['status'=>1]);

          if($address){
          return redirect()->back()->with('success','Address Selected');
          }
          }

        /*Payment Integration Start*/
        protected function razorpay(Request $request)
          {
          $data = $request->all();

          $fetch_address= Address::where('email',Auth::user()->email)->where('status',1)->first();
          //fetch address
          $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

          $productid= $carts->pluck('productid')->toArray();
          $products= Products::select('store_id')->whereIn('id',$productid)->get();
          $storeid= $products->pluck('store_id')->toArray();
          $varientid= $carts->pluck('price_varient_id')->toArray();

               foreach($carts as $cart)
               {
                $OrderItems= new Order_items;
                $OrderItems->order_id=$data['razorpay_order_id'];
                $OrderItems->productid=$cart->productid;
                $OrderItems->varientid=$cart->price_varient_id;
                $OrderItems->quantity=$cart->quantity;
                $OrderItems->size=$cart->size;
                $OrderItems->price=$cart->saleprice;
                $OrderItems->save();
               }

         //fetch all products id from cart
          $Orders= new Orders;
          $Orders->name=Auth::user()->name;
          $Orders->email=Auth::user()->email;
          $Orders->mobile=Auth::user()->mobile;
          $Orders->address=$fetch_address->address;
          $Orders->landmark=$fetch_address->landmark;
          $Orders->order_id = $data['razorpay_order_id'];
          $Orders->amount = Session::get('checkout');
          $Orders->date_time = Session::get('date_time');
          $Orders->discount = Session::get('checkout_disc');
          $Orders->store_id=json_encode($storeid);
          $api = new Api('rzp_test_RY9YnlOGgxjtXk', '1EQIPRQQtTXgZ8ml10mxjOYK');
          try{
          $attributes = array(
          'razorpay_signature'  => $data['razorpay_signature'],
          'razorpay_payment_id' => $data['razorpay_payment_id'],
          'razorpay_order_id'   => $data['razorpay_order_id']
          );
          $order = $api->utility->verifyPaymentSignature($attributes);
          $success = true;
        
          }catch(SignatureVerificationError $e){

          $succes = false;
         

          }

          if($success){
           
           $Orders->payment_status = 1;
           $Orders->save();
           
           //save order
           Session::forget(['checkout', 'checkout_disc','order_id']);
           //destroy all sessions related to cart and checkout
            $carts = Cart::where('user_email',Auth::user()->email)
               ->get();
                foreach($carts as $cart)
              {
                $pid=$cart->productid;
                $quantity=$cart->quantity;

                $productsmatch=Products::where('id',$pid)
                 ->first();
                 $productQuantity=$productsmatch->quantity;
                 $newQuantity=$productQuantity - $quantity;

                 if($newQuantity == 0)
                 {
                  $updateStoke_Quantity=Products::where('id',$productsmatch->id)
                 ->update(['stoke' => 0,'quantity'=>$newQuantity]);
                 }
                 else
                 {
                  $updateQuantity=Products::where('id',$productsmatch->id)
                 ->update(['quantity'=>$newQuantity]);
                 }
                
              }
              //Quantity and Stock Update
              $cartdelete = Cart::where('user_email',Auth::user()->email)
               ->delete();
           //delete cart items after success


          return redirect()->route('order.success');
          }
          else
          {

          return redirect()->route('error');
          }
      }

        public function error()
        {
        return view('error');
        }


    public function success()
    {   
      $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

        $CartCount=count($carts);
        return view('frontend.success')->with(compact('CartCount'));
    }

    protected function date_time(Request $request)
    {   
      $request->validate([
    'datetime'=> 'required',
    'time'=> 'required'
    ]);
    $date=$request->datetime;
    
    $time=$request->time[0];
    $date_time= $date.'    '.'('.$time.')';
    
    Session::put('date_time',$date_time);
     return redirect()->back();
    }

}
