<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Cart;

class IndexController extends Controller
{
    
        public function index()
        {
     
         $latitude = Cookie::get('LocationLat');
         $longitude = Cookie::get('LocationLong');
         $km =10;  //Show stores under this distance
     
        $stores = DB::table('stores')
        ->selectRaw("id, name, address, latitude, longitude,
        ( 6371* acos( cos( radians(?) ) *
        cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(?)
        ) + sin( radians(?) ) *
        sin( radians( latitude ) ) )
        ) AS distance", [$latitude, $longitude, $latitude])
        ->where('status', '=', 1)
        ->having("distance", "<", $km)
        ->orderBy("id",'asc')
        ->offset(0)
        ->limit(20)
        ->get();
        //Get Stores Under 10 Km
    
         $storId = $stores->pluck('id')->toArray();
        
         $products=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)->get();

         $product_varientId=$products->pluck('id')->toArray();
         
         //Product id for Varients
         $productId=$products->pluck('maincategory')->toArray();
         // Show Product According to Location
         $maincategories=DB::table('maincategory')
        ->where('status',1)
        ->whereIn('id',$productId)
        ->get();
        //MainCategory
        $bannerstop=DB::table('banners')
        ->where('status',1)
        ->where('type','top')
        ->get();
        //BannerTop
        $bannersmid=DB::table('banners')
        ->where('status',1)
        ->where('type','mid')
        ->orderBy('updated_at','desc')
        ->take(2)
        ->get();
        //BannerMedium
        $varients=DB::table('price_according_weight')
        ->where('status',1)
        ->whereIn('productid',$product_varientId)
        ->get();
        //Varients

        //Cart Count
       
       if(Auth::user() !== null)
       {
        $cart = Cart::where('user_email',Auth::user()->email)
               ->get();
         $CartCount=count($cart);
       }
       
        return view('frontend.index')->with(compact('maincategories','bannerstop','products','bannersmid','varients','CartCount'));
    }

     public function locationupdate(Request $request)
     {
      
      $getaddress=$request->location;
      $getcity=$request->city;
      $getstate=$request->state;
     $getlat=$request->latitude;
     $getlng=$request->longitude;
      
      $locationupdate=DB::table('location')->where('city',$getcity)->where('state',$getstate)->get();

      if(count($locationupdate) == 0 ){
        return redirect()->back()->with('locationerror','We do not serve at your location')->withCookie(Cookie::forget('LocationCity'))->withCookie(Cookie::forget('LocationAddress'))->withCookie(Cookie::forget('LocationLat'))->withCookie(Cookie::forget('LocationLong'));

      }
      else
      {
            //$time = time()+86400; //One day
            // $time = time()+3600; //One Hour
            $time = time()+60; //One Minute
  
        return redirect()->back()->withCookie(Cookie::make('LocationCity',$getcity,$time))->withCookie(Cookie::make('LocationAddress',$getaddress,$time))->withCookie(Cookie::make('LocationLat',$getlat,$time))->withCookie(Cookie::make('LocationLong',$getlng,$time));
        dd($time);
      }


     }
     

}
