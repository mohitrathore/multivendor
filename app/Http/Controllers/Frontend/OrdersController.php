<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Orders;
use App\User;
use App\Cart;
use App\Products;
use App\Weight_Price;
use App\Order_items;
use Illuminate\Support\Facades\Auth;
class OrdersController extends Controller
{
    public function index()
    {  
    	 $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

        $CartCount=count($carts);
        $users=User::where('email',Auth::user()->email)->first();
        $orders=Orders::where('email',Auth::user()->email)->paginate(9);
        
        return view('frontend.orders.index')->with(compact('CartCount','orders','users'));
       
        
     }
     public function orderdetail($id)
    {  
    	$carts = Cart::where('user_email',Auth::user()->email)
               ->get();
          
        $CartCount=count($carts);

         $orders=Orders::where('email',Auth::user()->email)->where('id',$id)->first();
         
          $ordertid=$orders->order_id;
          $orderitems=Order_items::where('order_id',$ordertid)->get();

			$productid= $orderitems->pluck('productid')->toArray();
			$varientid= $orderitems->pluck('varientid')->toArray();
            
            $products= Products::whereIn('id',$productid)->get();
         
        
        return view('frontend.orders.order_detail')->with(compact('CartCount','products','orders','orderitems'));
     }
}
