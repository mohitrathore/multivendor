<?php
 
namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\User;
use Image;
class ProfileController extends Controller
{
     public function index()
    { 
        
        $carts = Cart::where('user_email',Auth::user()->email)
               ->get();

        $CartCount=count($carts);

        $users=User::where('email',Auth::user()->email)
               ->first();

        return view('frontend.profile.profile')->with(compact('CartCount','users'));
    }
    protected function edit(Request $request)
    {
        $request->validate([

            'name'=>'required|max:200',
            'mobile'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:10',
            'gender'=> 'required',
            'profileImage'=> 'mimes:jpg,jpeg,png'
            ]);

			if ($profileimage = $request->file('profileImage')) 
			{ 
                  $ProfileImage= 'u'.date('YmdHis') . "." .$profileimage->getClientOriginalName();
				$image_resize = Image::make($profileimage->getRealPath());              
				$image_resize->resize(100, 100);
				$image_resize->save('profile_images/' .$ProfileImage);

				if($request->input('oldprofileImage') !== null)
				{
				unlink('profile_images/'.$request->input('oldprofileImage'));
				}			
			}   // New Profile Image Upload
			else
			{
			$ProfileImage=$request->input('oldprofileImage');
			}



			$update=User::where('email',Auth::user()->email)
            ->update([
            'name'=>$request->input('name'),
            'profileImage'=>$ProfileImage,
            'mobile'=>$request->input('mobile'),
            'gender'=>$request->input('gender'),
            'updated_at'=>now()
            ]);

            if($update){

            return redirect()->route('profile.view')->with('success','Profile Updated Successfully');
            }else{
            return redirect()->back()->with('error',' Profile Updation Error');
            }
    }
}
