<?php

namespace App\Http\Controllers\Frontend;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Cookie;
use App\Cart;
class ShopController extends Controller
{
    public function main($main,$mainid){
    	$latitude = Cookie::get('LocationLat');
         $longitude = Cookie::get('LocationLong');
         $km =10;
     
        $stores = DB::table('stores')
        ->selectRaw("id, name, address, latitude, longitude,
        ( 6371* acos( cos( radians(?) ) *
        cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(?)
        ) + sin( radians(?) ) *
        sin( radians( latitude ) ) )
        ) AS distance", [$latitude, $longitude, $latitude])
        ->where('status', '=', 1)
        ->having("distance", "<", $km)
        ->orderBy("id",'asc')
        ->offset(0)
        ->limit(20)
        ->get();

         $storId = $stores->pluck('id')->toArray();
         /* Show Product Main Category according to category id based on location and category */
         $products=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)->where('maincategory',$mainid)->paginate(9);

         $productid=$products->pluck('id')->toArray();
          /* Show Product Main Category according to category id based on location */
         
         /* Show all category  according to  locations  */
         $products_maincategory=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)->get();
         
         $productmaincategoryid=$products_maincategory->pluck('maincategory')->toArray();
         

         $maincategory=DB::table('maincategory')->where('status',1)
         ->whereIn('id',$productmaincategoryid)->get();

         $secondcategory=DB::table('secondcategory')->where('status',1)
         ->whereIn('parentid',$productmaincategoryid)->get();

         $productsecondcategoryid=$secondcategory->pluck('id')->toArray();

         $thirdcategory=DB::table('thirdcategory')->where('status',1)
         ->whereIn('parentid',$productsecondcategoryid)->get();

         //second category
           /* Show all category  according to  locations  */
         $varients=DB::table('price_according_weight')->whereIn('productid',$productid)->where('status',1)->get();
          
        //Cart Count
        $cart = Cart::where('user_email',Auth::user()->email)
               ->get();
        $CartCount=count($cart);
        
      return view('frontend.shop.shop_main')->with(compact('products','varients','main','mainid','maincategory','secondcategory','thirdcategory','CartCount'));

    }

    public function second($main,$mainid,$second,$secondid){
    	$latitude = Cookie::get('LocationLat');
         $longitude = Cookie::get('LocationLong');
         $km =10;
     
        $stores = DB::table('stores')
        ->selectRaw("id, name, address, latitude, longitude,
        ( 6371* acos( cos( radians(?) ) *
        cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(?)
        ) + sin( radians(?) ) *
        sin( radians( latitude ) ) )
        ) AS distance", [$latitude, $longitude, $latitude])
        ->where('status', '=', 1)
        ->having("distance", "<", $km)
        ->orderBy("id",'asc')
        ->offset(0)
        ->limit(20)
        ->get();

         $storId = $stores->pluck('id')->toArray();
         /* Show Product according to location and category */
         $products=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)
         ->where('maincategory',$mainid)
         ->where('secondcategory',$secondid)
         ->paginate(9);
         
         $productid=$products->pluck('id')->toArray();
          /* Show Product Main Category according to category id based on location */
         
         /* Show all category  according to  locations  */
         $products_all=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)->get();
         
         $productmaincategoryid=$products_all->pluck('maincategory')->toArray();
         $productsecondcategoryid=$products_all->pluck('secondcategory')->toArray();
        //fetch both category id who match in product table 
         $maincategory=DB::table('maincategory')->where('status',1)
         ->whereIn('id',$productmaincategoryid)->get();
         
         //main category
         $secondcategory=DB::table('secondcategory')->where('status',1)
         ->whereIn('parentid',$productmaincategoryid)->get();
         //second category
         
         $productsecondcategoryid=$secondcategory->pluck('id')->toArray();

         $thirdcategory=DB::table('thirdcategory')->where('status',1)
         ->whereIn('parentid',$productsecondcategoryid)->get();
         //third category
       
         $varients=DB::table('price_according_weight')->whereIn('productid',$productid)->where('status',1)->get();
        
        //Cart Count
        $cart = Cart::where('user_email',Auth::user()->email)
               ->get();
        $CartCount=count($cart);

      return view('frontend.shop.shop_second')->with(compact('products','varients','main','mainid','maincategory','secondcategory','second','secondid','thirdcategory','CartCount'));

    }
    public function third($main,$mainid,$second,$secondid,$third,$thirdid){
    	$latitude = Cookie::get('LocationLat');
         $longitude = Cookie::get('LocationLong');
         $km =10;
     
        $stores = DB::table('stores')
        ->selectRaw("id, name, address, latitude, longitude,
        ( 6371* acos( cos( radians(?) ) *
        cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(?)
        ) + sin( radians(?) ) *
        sin( radians( latitude ) ) )
        ) AS distance", [$latitude, $longitude, $latitude])
        ->where('status', '=', 1)
        ->having("distance", "<", $km)
        ->orderBy("id",'asc')
        ->offset(0)
        ->limit(20)
        ->get();

         $storId = $stores->pluck('id')->toArray();
         /* Show Product according to location and category */
         $products=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)
         ->where('maincategory',$mainid)
         ->where('secondcategory',$secondid)
         ->where('thirdcategory',$thirdid)
         ->paginate(9);
         
         $productid=$products->pluck('id')->toArray();
          /* Show Product Main Category according to category id based on location */
         
         /* Show all category  according to  locations  */
         $products_all=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)->get();
         
         $productmaincategoryid=$products_all->pluck('maincategory')->toArray();
         $productsecondcategoryid=$products_all->pluck('secondcategory')->toArray();
        //fetch both category id who match in product table 
         $maincategory=DB::table('maincategory')->where('status',1)
         ->whereIn('id',$productmaincategoryid)->get();
         
         //main category
         $secondcategory=DB::table('secondcategory')->where('status',1)
         ->whereIn('parentid',$productmaincategoryid)->get();
         //second category
         
         $productsecondcategoryid=$secondcategory->pluck('id')->toArray();

         $thirdcategory=DB::table('thirdcategory')->where('status',1)
         ->whereIn('parentid',$productsecondcategoryid)->get();
         //third category
       
         $varients=DB::table('price_according_weight')->whereIn('productid',$productid)->where('status',1)->get();
         //Cart Count
        $cart = Cart::where('user_email',Auth::user()->email)
               ->get();
        $CartCount=count($cart);

      return view('frontend.shop.shop_third')->with(compact('products','varients','main','mainid','maincategory','secondcategory','second','secondid','thirdcategory','third','thirdid','CartCount'));

    }

    public function singleproduct($product,$productid)
    {  
    	$latitude = Cookie::get('LocationLat');
         $longitude = Cookie::get('LocationLong');
         $km =10;
     
        $stores = DB::table('stores')
        ->selectRaw("id, name, address, latitude, longitude,
        ( 6371* acos( cos( radians(?) ) *
        cos( radians( latitude ) )
        * cos( radians( longitude ) - radians(?)
        ) + sin( radians(?) ) *
        sin( radians( latitude ) ) )
        ) AS distance", [$latitude, $longitude, $latitude])
        ->where('status', '=', 1)
        ->having("distance", "<", $km)
        ->orderBy("id",'asc')
        ->offset(0)
        ->limit(20)
        ->get();

         $storId = $stores->pluck('id')->toArray();
         
         /* Show Product according to location and category */
         $products=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)
         ->where('id',$productid)
         ->first();
          $productbest_offer=DB::table('products')->where('status',1)
         ->whereIn('store_id',$storId)
         ->where('criteria','best-offer')
         ->get();
         //product vartients
         $varients=DB::table('price_according_weight')->where('productid',$productid)->where('status',1)->get();
          //Cart Count
        $cart = Cart::where('user_email',Auth::user()->email)
               ->get();
        $CartCount=count($cart);
         
       return view('frontend.product_single')->with(compact('products','varients','productbest_offer','CartCount'));
    }
}
