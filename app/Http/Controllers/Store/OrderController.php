<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Orders;
use App\Order_items;
use App\Products;
class OrderController extends Controller
{
    
    public function index()
    {   
        $store=DB::table('stores')->where('email',Auth::user()->email)->first();
        $storeid=$store->id;
        $orders=Orders::whereJsonContains('store_id',$storeid)->where('payment_status',1)->get();

        $orderid= $orders->pluck('order_id')->toArray();
        
       
        $items=Order_items::whereIn('order_id',$orderid)->get(); 
        $pid= $items->pluck('productid')->toArray();
        $products=Products::whereIn('id',$pid)->get();

    	return view('store.orders.index')->with(compact('orders','items','products'));
    }
     protected function accept($id)
		{
			$accept=Orders::where('id',$id)->update(['store_status'=>1]);

			if($accept)
			{
				return redirect()->back()->with('success','Order accepted successfully');
			}
			else
			{
				return redirect()->back()->with('error',' Order accept error');
			}

		}  //active product

		protected function deny($id)
		{
			$deny=Orders::where('id',$id)->update(['store_status'=>2]);

			if($deny){
				return redirect()->back()->with('success','Order Denied successfully');
			}else{
				return redirect()->back()->with('error','Order Denied error');
			}
		}
}
