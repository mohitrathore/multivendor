<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Image;
use App\Products;
use App\Weight_Price;
class ProductController extends Controller
{       
	   public function index()
		{     
		 
		$viewproducts=DB::table('products')->where('owner_email',Auth::user()->email)->paginate(15);
         
		$main=DB::table('maincategory')->select('id','name')->where('status',1)->get();

		$second=DB::table('secondcategory')->select('id','name')->where('status',1)->get();

		$third=DB::table('thirdcategory')->select('id','name')->where('status',1)->get();


		return view('store.products.viewproduct')->with(compact('viewproducts','main','second','third'));
	
		}
		public function view()
		{     

		$restrictions=DB::table('restrictions')->select('restrict_cat')->where('email',Auth::user()->email)->first();
		
		$maincategories=DB::table('maincategory')->where('status',1)->get();
		$stores=DB::table('stores')->where('status',1)->where('email',Auth::user()->email)->get();
		return view('store.products.addproduct')->with(compact('maincategories','stores','restrictions'))->with('no',1);
		}
		public function selectsecondcategory(Request $request)
		{

		if($request->ajax())
		{
		$viewsecondcategories=array(
		'secondcategories'=>DB::table('secondcategory')
		->where('parentid',$request->maincategory)
		->where('status',1)
		->get()
		);
		return view('store.products.productselectsecond',$viewsecondcategories);
		}

		} //secondcateogry show from ajax 

		public function selectthirdcategory(Request $request)
		{

		if($request->ajax())
		{
		$viewthirdategories=array(
		'thirdcategories'=>DB::table('thirdcategory')
		->where('parentid',$request->secondcategory)
		->where('status',1)
		->get());
		return view('store.products.productselectthird',$viewthirdategories);
		}

		} //thirdcateogry show from ajax 

		protected function add(Request $request)
		{
             
			$request->validate([
			'store'=> 'required',
			'name'=> 'required|max:225',
			'description'=> 'required',
			'disclaimer'=> 'required',
			'coverimage'=>'required|mimes:jpg,jpeg,png',
			'stoke'=>'required',
			'type'=>'required',
			'criteria'=>'required',
			'discount'=>'required',
			'maincategory'=>'required',
			'quantity'=>'required',
			'weightprice.*.weight' => 'required',
			'weightprice.*.unit' => 'required',
			'weightprice.*.price' => 'required|numeric|min:0|not_in:0|not_in:1',
			'images' =>'required',
			'images.*' => 'mimes:jpeg,png,jpg'
			]); 
			//product validation

              if($request->hasfile('images'))
				{

				foreach($request->file('images') as $image)
				{
				$name= time() . "." .$image->getClientOriginalName();
				$image_resize = Image::make($image->getRealPath());              
				$image_resize->resize(917, 1000);
				$image_resize->save('product_images/' .$name);
				$data[] = $name;  
				}
				} //product multiple images

			if ($verification = $request->file('coverimage')) {
			    $verificationImage= time() . "." .$verification->getClientOriginalName();
				$image_resize = Image::make($verification->getRealPath());              
				$image_resize->resize(181, 181);
				$image_resize->save('product_images/' .$verificationImage);

			}  // Product Cover image
 
				$product=new Products();
				$product->name=$request->input('name');
				$product->description=$request->input('description');
				$product->disclaimer=$request->input('disclaimer');
				$product->productcode=$request->input('productcode');
				$product->coverimage=$verificationImage;
				$product->images=json_encode($data);
				$product->stoke=$request->input('stoke');
				$product->type=$request->input('type');
				$product->criteria=$request->input('criteria');
				$product->store_id=$request->input('store');
				$product->owner_email=Auth::user()->email;
				$product->discount=$request->input('discount');
				$product->maincategory=$request->input('maincategory');
				$product->secondcategory=$request->input('secondcategory');
				$product->thirdcategory=$request->input('thirdcategory');
				$product->quantity=$request->input('quantity');
				$product->status=1;
				$product->save();    //Product add 

			if( $product->save() )
			{ 
					foreach ($request->weightprice as $key => $value)
					{    
						$saleprice=$value['price'] - $value['price']*$request->input('discount')/100;

						$weight_price= new Weight_Price();
						$weight_price->productid=$product->id;
						$weight_price->weight=$value['weight'];
						$weight_price->unit=$value['unit'];
						$weight_price->price=$value['price'];
						$weight_price->saleprice=$saleprice;
						$weight_price->status=1;
						$weight_price->save(); //Product Weight, Units, Price added 

					} 
					

			        return redirect()->route('product.index')->with('success','Product Added Successfully');

			}
			else
			{
			        return redirect()->back()->with('error',' Error in Product Add');

			}

		}  // Add product with weight and price varients

		public function vieweditproduct($id)
		{
			$productdetails=DB::table('products')->where('id',$id)->first();
			$restrictions=DB::table('restrictions')->select('restrict_cat')->where('email',Auth::user()->email)->first();

			$maincategories=DB::table('maincategory')->where('status',1)->get();
			$stores=DB::table('stores')->where('status',1)->where('email',Auth::user()->email)->get();
			$maincategories=DB::table('maincategory')->where('status',1)->get();
			$secondcategories=DB::table('secondcategory')->where('parentid',$productdetails->maincategory)->get();
			$thirdcategories=DB::table('thirdcategory')->where('parentid',$productdetails->secondcategory)->get();

			$weightprices=DB::table('price_according_weight')->where('productid',$id)->get();

			return view('store.products.editproduct')->with(compact('productdetails','stores','weightprices','maincategories','secondcategories','thirdcategories','restrictions'))->with('no',1);
		}
		protected function updateproduct(Request $request)
		{ 

			$request->validate([
			'name'=> 'required|max:255',
			'description'=> 'required',
			'disclaimer'=> 'required',
			'coverimagenew'=>'mimes:jpg,jpeg,png',
			'stoke'=>'required',
			'type'=>'required',
			'criteria'=>'required',
			'store'=>'required',
			'discount'=>'required',
			'maincategory'=>'required',
			'quantity'=>'required',
			'weightprice.*.weight' => 'required',
			'weightprice.*.unit' => 'required',
			'weightprice.*.price' => 'required|numeric|min:0|not_in:0|not_in:1',
			'images.*' => 'mimes:jpeg,png,jpg'
			]); 
			//product validation

				if($request->hasfile('images'))
				{
					foreach($request->file('images') as $image)
					{

						$name= time() . "." .$image->getClientOriginalName();
						$image_resize = Image::make($image->getRealPath());              
						$image_resize->resize(917, 1000);
						$image_resize->save('product_images/' .$name);
						$data[] = $name;  

					}

					foreach(json_decode($request->input('realimages'),true) as $image =>$key)
					{

						unlink('product_images/'.$key);

					} 	//delete all multiple images

					$multipleImages=json_encode($data); // All multiple images name
				}
				else
				{ 
                   $multipleImages=json_decode($request->input('realimages'),true); //old multiple images name;
				}  

			if ($verification = $request->file('coverimagenew')) 
			{

				$verificationImage =date('YmdHis') . "." . $verification->getClientOriginalExtension();
				$image_resize = Image::make($verification->getRealPath());              
				$image_resize->resize(181,181);
				$image_resize->save('product_images/' .$verificationImage);
				unlink('product_images/'.$request->input('coverimage'));
			} // New Product Cover image
			else
			{
				$verificationImage=$request->input('coverimage');
			} // Old Product Cover image
             
             
			$product=Products::find($request->input('cid'));
			$product->name=$request->input('name');
			$product->description=$request->input('description');
			$product->disclaimer=$request->input('disclaimer');
			$product->coverimage=$verificationImage;
			$product->images=$multipleImages;
			$product->stoke=$request->input('stoke');
			$product->type=$request->input('type');
			$product->criteria=$request->input('criteria');
			$product->store_id=$request->input('store');
			$product->discount=$request->input('discount');
			$product->maincategory=$request->input('maincategory');
			$product->secondcategory=$request->input('secondcategory');
			$product->thirdcategory=$request->input('thirdcategory');
			$product->quantity=$request->input('quantity');
			$product->save();    //Product update details 

			if( $product->save() )
			{


                    $weightpricecount=count($request->weightprice);
                    
                    for($i=0;$i < $weightpricecount; $i++)
                    {
						$weight=$request->weightprice[$i]['weight'];
						$unit=$request->weightprice[$i]['unit'];
						$price=$request->weightprice[$i]['price'];
						$idwp=$request->weightprice[$i]['id'];
						$saleprice=$price - $price*$request->input('discount')/100;
                        
						$wp=Weight_Price::updateOrCreate([
						'id'   => $idwp,
						],[
						'weight'    => $weight,
						'unit'      => $unit,
						'price'     => $price,
						'saleprice' => $saleprice,
						'productid' => $request->input('cid'),
						'status'    => 1,
						'updated_at'=> now()
						]);

                    }
                  

			        return redirect()->back()->with('success','Product Edited Successfully');

			}
			else
			{
			        return redirect()->back()->with('error',' Error in Product Edit');

			}

		}  // Update product with weight and price varients
		protected function activeproduct($id)
		{
			$activeproduct=DB::table('products')->where('id',$id)
			->update(['status'=>'1']);

			if($activeproduct)
			{
				return redirect()->back()->with('success','Product activated successfully');
			}
			else
			{
				return redirect()->back()->with('error',' Product activation error');
			}

		}  //active product

		protected function inactiveproduct($id)
		{
			$inactiveproduct=DB::table('products')->where('id',$id)
			->update(['status'=>'0']);

			if($inactiveproduct){
				return redirect()->back()->with('success','Product Inactivated successfully');
			}else{
				return redirect()->back()->with('error',' Product Inactivation error');
			}
		}  //Inactive product

		protected function deleteproduct($id)			    
		{

				$deleteimages=DB::table('products')->where('id',$id)->first();

				foreach(json_decode($deleteimages->images) as $image)
				{

					unlink('product_images/'.$image);
				} //delete all multiple images

			    unlink('product_images/'.$deleteimages->coverimage); //delete coverimage
				$delete=DB::table('products')->where('id',$id)->delete();
				$deletevarients=DB::table('price_according_weight')->where('productid',$id)->delete();

				if($delete){
				return redirect()->back()->with('success','Product delete successfully');
				}else{
				return redirect()->back()->with('error',' Product delete error');
				}
		}  //delete product

}
