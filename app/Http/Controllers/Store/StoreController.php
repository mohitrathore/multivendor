<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class StoreController extends Controller
{
    public function index()
    {    
    	$storeOwner=DB::table('users')->where('email',Auth::user()->email)->first();
        return view('store.dashboard')->with(compact('storeOwner'));
    }
}
