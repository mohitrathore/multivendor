<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Image;
class ListController extends Controller
{
	public function index()
	{  
	$stores=DB::table('stores')
	->where('email',Auth::user()->email)
	->get();
	return view('store.stores')->with(compact('stores'));
	}
	public function view()
	{
            $agents=DB::table('agents')->where('status',1)->get();

      return view('store.addstore')->with(compact('agents'));
	} 

      protected function add(Request $request)
      {
               $request->validate([

            'store_name'=>'required|max:20',
            'address'=> 'required',
            'opening_time'=>'required',
            'closing_time'=> 'required',
            'description'=> 'required',
            'cover_photo'=> 'required|mimes:jpg,jpeg,png',
            'aadharcard'=> 'required|mimes:jpg,jpeg,png',
            'udhyog_gst'=> 'required|mimes:jpg,jpeg,png',
            'agentcode'=> 'required'

            ]);
           
            $locations=DB::table('location')->select('city','state')
            ->where('status',1)
            ->where('city',$request->input('city'))
            ->where('state',$request->input('state'))
            ->first();

            if($locations == null)
            {
            return redirect()->back()->with('error','We do not serve at your location yet ! Coming Soon');

            }
            else
            {

                  
                if ($cover_photo = $request->file('cover_photo')) {
                  $coverphotoPath = 'store_images/'; // upload path
                  $coverImage ='c'.date('YmdHis') . "." . $cover_photo->getClientOriginalExtension();
                  }   // Image upload for cover photo

                  if ($aadharcard = $request->file('aadharcard')) {
                  $aadharcardPath = 'store_images/verification'; // upload path
                  $aadharcardImage ='aa'.date('YmdHis') . "." . $aadharcard->getClientOriginalExtension();
                  }   // Image upload for aadhar card

                  if ($udhyog_gst = $request->file('udhyog_gst')) {
                  $udhyog_gst_Path = 'store_images/verification'; // upload path
                  $udhyog_gstImage ='u_gst'.date('YmdHis') . "." . $udhyog_gst->getClientOriginalExtension();
                  }   // Image upload for udhyog & Gst

                  $query=DB::table('stores')->insert
                  ([
                  'email'=>Auth::user()->email,
                  'name'=>$request->input('store_name'),
                  'address'=>$request->input('address'),
                  'opening_time'=>$request->input('opening_time'),
                  'closing_time'=>$request->input('closing_time'),
                  'description'=>$request->input('description'),
                  'latitude'=>$request->input('latitude'),
                  'longitude'=>$request->input('longitude'),
                  'city'=>$request->input('city'),
                  'state'=>$request->input('state'),
                  'agent'=>$request->input('agentcode'),
                  'cover_photo'=>$coverImage,
                  'aadharcard'=>$aadharcardImage,
                  'udhyog_gst'=>$udhyog_gstImage,
                  'status'=>2,
                  'created_at'=>now()
                  ]);

                  if($query)
                  {
                  $udhyog_gst->move($udhyog_gst_Path, $udhyog_gstImage); // Udhyog GST photo
                  $aadharcard->move($aadharcardPath, $aadharcardImage); // Aadhar Card photo
                  $cover_photo->move($coverphotoPath, $coverImage); // Cover photo
                  return redirect()->route('store.list')->with('success','Store Added Successfully');
                  }
                  else
                  {
                  return redirect()->back()->with('error',' Error in Store Add');

                  }
                       
            }
              

                  
			}  //Store add

		public function edit($id){

            $editstores=DB::table('stores')->where('id',$id)->first();
            $showstores=[
            'editstoredata'=> $editstores,
            'title'=>'Edit'
            ];


            return view('store.editstore',$showstores);
            }  //store edit page

            protected function update(Request $request){

            $request->validate([

            'store_name'=>'required|max:20',
            'address'=> 'required',
            'opening_time'=>'required',
            'closing_time'=> 'required',
            'description'=> 'required'

            ]);   
            $locations=DB::table('location')->select('city','state')
            ->where('status',1)
            ->where('city',$request->input('city'))
            ->where('state',$request->input('state'))
            ->first();

            if($locations == null)
            {
            return redirect()->back()->with('error','Your Store location is out of our reach area ! Please change the address again');
            
            }
            else
            {

                  if ($cover_photo = $request->file('cover_photo'))
                  {
                        $coverphotoPath = 'store_images/'; // upload path
                        $coverImage ='c'.date('YmdHis') . "." . $cover_photo->getClientOriginalExtension();
                          $cover_photo->move($coverphotoPath, $coverImage); // Cover photos
                         unlink('store_images/'.$request->input('oldcover_photo'));
                  }   // New Cover Photo upload for cover photo
                  else
                  {
                  $coverImage=$request->input('oldcover_photo');
                  }


            $update=DB::table('stores')->where('id',$request->input('cid'))
            ->update([
            'name'=>$request->input('store_name'),
            'address'=>$request->input('address'),
            'cover_photo'=>$coverImage,
            'opening_time'=>$request->input('opening_time'),
            'closing_time'=>$request->input('closing_time'),
            'description'=>$request->input('description'),
            'latitude'=>$request->input('latitude'),
            'longitude'=>$request->input('longitude'),
            'city'=>$request->input('city'),
            'state'=>$request->input('state'),
            'agent'=>$request->input('agent'),
            'updated_at'=>now()
            ]);

            if($update){

            return redirect()->route('store.list')->with('success','Store edit successfully');
            }else{
            return redirect()->back()->with('error',' Error in store update');
            }

      }

            }
            // update store details 
 


    		//status change to active 
            protected function active($id){
            $active=DB::table('stores')->where('id',$id)
            ->update(['status'=>'1']);

            if($active){
            return redirect()->back()->with('success','Store activated successfully');
            }else{
            return redirect()->back()->with('error',' Store activation error');
            }

            }
            //status change to Inactive 
            protected function inactive($id){
            $inactive=DB::table('stores')->where('id',$id)
            ->update(['status'=>'0']);

            if($inactive){
            return redirect()->back()->with('success','Store Inactivated successfully');
            }else{
            return redirect()->back()->with('error',' Store Inactivation error');
            }

            }
            public function deletestore($id){

                  $deleteimages=DB::table('stores')->where('id',$id)->first();


                  unlink('store_images/'.$deleteimages->cover_photo); //delete cover photo
                  unlink('store_images/verification/'.$deleteimages->aadharcard); //delete aadharcard
                  unlink('store_images/verification/'.$deleteimages->udhyog_gst); //delete udhyog gst

                  $deletestore=DB::table('stores')->where('id',$id)->delete();

            if($deletestore){

            return redirect()->back()->with('success','Store deleted successfully');
            }else{
            return redirect()->back()->with('error',' Error in store delete');
            }

            }
            //delete store
}
