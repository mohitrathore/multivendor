<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo()
    {
        switch(Auth::user()->role){
        case 'store':
        $this->redirectTo = '/store';
        return $this->redirectTo;
        break;
        case 'agent':
        $this->redirectTo = '/agent';
        return $this->redirectTo;
        break;
        case 'driver':
        $this->redirectTo = '/driver';
        return $this->redirectTo;
        break;

        default:
        $this->redirectTo = url()->previous();
        return $this->redirectTo;
        }
        // return $next($request);
    } 
//protected $redirectTo ='/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    protected function credentials(Request $request) {
      return array_merge($request->only($this->username(), 'password','otp'));
}

    public function sendOtp(Request $request)
    {

        $otp = rand(1000,9999);
        $user = User::where('email','=',$request->email)->update(['otp' => $otp]);
        $user_role= User::where('email','=',$request->email)->first();
        

        if($user)
        {
            $authKey = "313372656478736d733830331613632715";

            // mobiles numbers 
            $mobileNumber = $user_role->mobile;

            //Sender ID,While using route4 sender id should be 6 characters long.
            $senderId = "TESTSM";

            //Your message to send, Add URL encoding here.
            $message = "Dear Customer, use OTP code".' '.$otp.' '." to verify your account for Login
            Grocery, Aapke apne manpasand products";
 
            //Define route 
            $route = "4";
            //Prepare you post parameters
            $postData = array(
            'authentic-key' => $authKey,
            'number' => $mobileNumber,
            'message' => $message,
            'senderid' => $senderId,
            'route' => $route
            );

            //print_r($postData);
            //Put your sms provider api
            $url="http://sms.redxsms.com/http-tokenkeyapi.php?";

            // init the resource
            $ch = curl_init();

            curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
            ));


            //Ignore SSL certificate verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


            //get response
            $output = curl_exec($ch);



            //Print error if any
            if(curl_errno($ch))
            {

            echo 'error:' . curl_error($ch);
            }

            curl_close($ch);
            }
        
        return response()->json([$user,$user_role],200);
    }
    
}
