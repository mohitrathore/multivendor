<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       /*if ($guard == "admin" && Auth::guard($guard)->check()) {
            return redirect('/admin');
        }
       
        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }*/
         switch($guard){
            case 'admin':
             if(Auth::guard($guard)->check())
             {
                return redirect()->route('admin.dashboard');
             }
             else
             {
                return redirect()->route('admin.login'); 
             }
            break;

            default:
           if(Auth::guard($guard)->check())
             {
                return redirect('/');
             }
            break;
        } 
      return $next($request);
    }
}
