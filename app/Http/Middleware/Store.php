<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Illuminate\Http\Response;
class Store
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (!Auth::check()) {
            return redirect()->route('login')->with('error','You cannot see Aurthorise Pages without Login !');
        }


       if (Auth::user()->role == 'store') {
             return $next($request);
        } 
        else{
            return new Response(view('unauthorized')->with('role', 'Store Owner'));
        }
    }
}
