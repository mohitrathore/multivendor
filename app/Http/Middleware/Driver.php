<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use Illuminate\Http\Response;
class Driver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (!Auth::check()) {
            return redirect()->route('login')->with('error','You cannot see Aurthorize Pages without Login !');
        }

       if (Auth::user()->role == 'driver') {
             return $next($request);  
        } 

        else{
            return new Response(view('unauthorized')->with('role', 'Drivers'));
        }
    }
}
