<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weight_Price extends Model
{
     protected $table = "price_according_weight";
    protected $fillable = ['productid','weight','price','unit','status','saleprice'];
}
