<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecondCategory extends Model
{
   protected $table = "secondcategory";
    protected $fillable = ['parentid','name','slug','status'];
}
