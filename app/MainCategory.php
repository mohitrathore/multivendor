<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
	 protected $table = "maincategory";
    protected $fillable = ['name','slug','image','status'];
}
