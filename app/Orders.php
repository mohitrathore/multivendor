<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Orders extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','email','mobile','amount','discount','store_id','store_status','order_id','driver_id','driver_email','driver_status','payment_status','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'amount','discount','store_id','store_status','order_id','driver_id','driver_email','driver_status','payment_status','status'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
